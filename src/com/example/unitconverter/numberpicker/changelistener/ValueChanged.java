package com.example.unitconverter.numberpicker.changelistener;

import java.util.Observable;

import android.widget.NumberPicker;

public class ValueChanged extends Observable implements NumberPicker.OnValueChangeListener {
	@Override
	public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
		setChanged();
		notifyObservers();
	}
}