package com.example.unitconverter.edittext.listeners;

import java.util.Observable;

import android.text.Editable;
import android.text.TextWatcher;

public class EditTextChange extends Observable implements TextWatcher {
	@Override
	public void afterTextChanged(Editable editField) {
		setChanged();
		notifyObservers();
	}

	@Override
	public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub

	}
}