package com.example.unitconverter.edittext.listeners;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;

public class EditTextOnClick implements OnClickListener {
	@Override
	public void onClick(View v) {
		((EditText) v).setText("");
	}
}