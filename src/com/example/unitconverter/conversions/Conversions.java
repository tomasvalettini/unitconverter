package com.example.unitconverter.conversions;

import java.math.BigDecimal;

public class Conversions {
	/*
	 * CONSTANTS
	 */
	private static final double kelvinFactor = 273.15;
	private static final double fahrenheitFactor = 32;
	private static final double metersToInchesFactor = 39.3700787;
	private static final double metersToFeetFactor = 3.2808399;
	private static final double metersToYardsFactor = 1.0936133;
	private static final double metersToMilesFactor = 0.000621371;
	private static final double metersToNauticalMilesFactor = 0.00053996;
	private static final double inchesToMetersFactor = 0.0254;
	private static final double feetToMetersFactor = 0.3048;
	private static final double yardsToMetersFactor = 0.9144;
	private static final double milesToMetersFactor = 1609.344;
	private static final double nauticalMilesToMetersFactor = 1852;

	private static final double gramsToOuncesFactor = 0.035274;
	private static final double gramsToPoundsFactor = 0.00220462;
	private static final double gramsToStonesFactor = 0.000157473;
	private static final double gramsToShorttonsFactor = 0.0000011023;
	private static final double gramsToLongtonsFactor = 0.00000098421;

	private static final int inchFeetFactor = 12;
	private static final int inchYardFactor = 36;
	private static final int inchMileFactor = 63360;

	private static final double litersToCubicInchesFactor = 61.0237;
	private static final double litersToCubicFeetFactor = 0.0353147;
	private static final double litersToUSTspFactor = 202.884;
	private static final double litersToUSTbspFactor = 67.628;
	private static final double litersToUSOuncesFactor = 33.814;
	private static final double litersToUSCupsFactor = 4.22675;
	private static final double litersToUSPintsFactor = 2.11338;
	private static final double litersToUSQuartsFactor = 1.05669;
	private static final double litersToUSGallonsFactor = 0.264172;
	private static final double litersToImperialTspFactor = 168.936;
	private static final double litersToImperialTbspFactor = 56.3121;
	private static final double litersToImperialOuncesFactor = 35.1951;
	private static final double litersToImperialPintsFactor = 1.75975;
	private static final double litersToImperialQuartsFactor = 0.879877;
	private static final double litersToImperialGallons = 0.219969;

	private static final double cubicInchesToLitersFactor = 0.0163871;
	private static final double cubicFeetToLitersFactor = 28.3168;
	private static final double usTspToLitersFactor = 0.00492892;
	private static final double usTbspToLitersFactor = 0.0147868;
	private static final double usOuncesToLitersFactor = 0.0295735;
	private static final double usCupsToLitersFactor = 0.236588;
	private static final double usPintsToLitersFactor = 0.473176;
	private static final double usQuartsToLitersFactor = 0.946353;
	private static final double usGallonsToLitersFactor = 3.78541;
	private static final double imperialTspToLitersFactor = 0.00591939;
	private static final double imperialTbspToLitersFactor = 0.0177782;
	private static final double imperialOuncesToLitersFactor = 0.0284131;
	private static final double imperialPintsToLitersFactor = 0.568261;
	private static final double imperialQuartsToLitersFactor = 1.13652;
	private static final double imperialGallonsToLitersFactor = 4.54609;

	private static final double squareMetersToHectaresFactor = 0.0001;
	private static final double squareMetersToSquareKilometersFactor = 0.000001;
	private static final double squareMetersToSquareInchesFactor = 1550;
	private static final double squareMetersToSquareFeetFactor = 10.7639;
	private static final double squareMetersToSquareYardsFactor = 1.19599;
	private static final double squareMetersToAcresFactor = 0.000247105;
	private static final double squareMetersToSquareMilesFactor = 0.000000381;
	private static final double hectaresToSquareMetersFactor = 10000;
	private static final double squareKilometersToSquareMetersFactor = 1000000;
	private static final double squareInchesToSquareMetersFactor = 0.00064516;
	private static final double squareFeetToSquareMetersFactor = 0.092903;
	private static final double squareYardsToSquareMetersFactor = 0.836127;
	private static final double acresToSquareMetersFactor = 4046.86;
	private static final double squareMilesToSquareMetersFactor = 2560000;

	private static final double litersPer100KmToKmPerLitersFactor = 100;
	private static final double litersPer100KmToMilesPerUSGallonsFactor = 235.215;
	private static final double litersPer100KmToMilesPerImperialGallonsFactor = 282.481;
	private static final double milesPerUSGallonsToKmPerLitersFactor = 0.425144;
	private static final double milesPerUSGallonsToMilesPerImperialGallons = 1.20095;
	private static final double milesPerImperialGallonsToKmPerLitersFactor = 0.354006;
	private static final double milesPerImperialGallonsToMilesPerUSGallons = 0.832674;

	private static final double timeFactor = 60;
	private static final double secondHourFactor = timeFactor * timeFactor;
	private static final double dayhourFactor = 24;
	private static final double daysInWeek = 7;
	private static final double daysInMonths = 30.4368;
	private static final double daysInYears = 365.242;
	private static final double daysInDecades = daysInYears * 10;
	private static final double daysInCenturies = daysInYears * 100;
	private static final double secondsToNanosecondsFactor = 1000000000;
	private static final double secondsToMicrosecondsFactor = 1000000;
	private static final double secondsToMillisecondsFactor = 1000;
	private static final double secondsToMinutesFactor = 1 / timeFactor;
	private static final double secondsToHoursFactor = 1 / timeFactor / timeFactor;
	private static final double secondsToDaysFactor = secondsToHoursFactor / dayhourFactor;
	private static final double daysToWeekFactor = 1 / daysInWeek;
	private static final double daysToMonthsFactor = 1 / daysInMonths;
	private static final double daysToYearsFactor = 1 / daysInYears;
	private static final double daysToDecadesFactor = 1 / daysInDecades;
	private static final double daysToCenturiesFactor = 1 / daysInCenturies;

	private static final int bitsInBytes = 8;
	private static final int bitsInKibibits = 1024;
	private static final int kiloFactor = 1000;

	/*
	 * Temperature conversions!!!
	 */
	public static double celsiusToFahrenheit(double celsius) {
		return (celsius * 9 / 5) + fahrenheitFactor;
	}

	public static double celsiusToKelvin(double celsius) {
		return celsius + kelvinFactor;
	}

	public static double fahrenheitToCelsius(double fahrenheit) {
		return (fahrenheit - fahrenheitFactor) * 5 / 9;
	}

	public static double fahrenheitToKelvin(double fahrenheit) {
		return fahrenheitToCelsius(fahrenheit) + kelvinFactor;
	}

	public static double kelvinToCelsius(double kelvin) {
		return kelvin - kelvinFactor;
	}

	public static double kelvinToFehrenheit(double kelvin) {
		return celsiusToFahrenheit(kelvinToCelsius(kelvin));
	}

	/*
	 * Length conversions!!!
	 */
	public static double millimetersToCentimeters(double mm) {
		return mm / 10;
	}

	public static double millimetersToMeters(double mm) {
		return mm / 1000;
	}

	public static double millimetersToKilometers(double mm) {
		return mm / 1000000;
	}

	public static double millimetersToInches(double mm) {
		return metersToInches(millimetersToMeters(mm));
	}

	public static double millimetersToFeet(double mm) {
		return metersToFeet(millimetersToMeters(mm));
	}

	public static double millimetersToYards(double mm) {
		return metersToYards(millimetersToMeters(mm));
	}

	public static double millimetersToMiles(double mm) {
		return metersToMiles(millimetersToMeters(mm));
	}

	public static double millimetersToNauticalMiles(double mm) {
		return metersToNauticalMiles(millimetersToMeters(mm));
	}

	public static double centimetersToMillimeters(double cm) {
		return cm * 10;
	}

	public static double centimetersToMeters(double cm) {
		return cm / 100;
	}

	public static double centimetersToKilometers(double cm) {
		return cm / 100000;
	}

	public static double centimetersToInches(double cm) {
		return metersToInches(centimetersToMeters(cm));
	}

	public static double centimetersToFeet(double cm) {
		return metersToFeet(centimetersToMeters(cm));
	}

	public static double centimetersToYards(double cm) {
		return metersToYards(centimetersToMeters(cm));
	}

	public static double centimetersToMiles(double cm) {
		return metersToMiles(centimetersToMeters(cm));
	}

	public static double centimetersToNauticalMiles(double cm) {
		return metersToNauticalMiles(centimetersToMeters(cm));
	}

	public static double metersToMillimeters(double m) {
		return m * 1000;
	}

	public static double metersToCentimeters(double m) {
		return m * 100;
	}

	public static double metersToKilometers(double m) {
		return m / 1000;
	}

	public static double metersToInches(double m) {
		return m * metersToInchesFactor;
	}

	public static double metersToFeet(double m) {
		return m * metersToFeetFactor;
	}

	public static double metersToYards(double m) {
		return m * metersToYardsFactor;
	}

	public static double metersToMiles(double m) {
		return m * metersToMilesFactor;
	}

	public static double metersToNauticalMiles(double m) {
		return m * metersToNauticalMilesFactor;
	}

	public static double kilometersToMillimeters(double km) {
		return km * 1000000;
	}

	public static double kilometersToCentimeters(double km) {
		return km * 100000;
	}

	public static double kilometersToMeters(double km) {
		return km * 1000;
	}

	public static double kilometersToInches(double km) {
		return metersToInches(kilometersToMeters(km));
	}

	public static double kilometersToFeet(double km) {
		return metersToFeet(kilometersToMeters(km));
	}

	public static double kilometersToYards(double km) {
		return metersToYards(kilometersToMeters(km));
	}

	public static double kilometersToMiles(double km) {
		return metersToMiles(kilometersToMeters(km));
	}

	public static double kilometersToNauticalMiles(double km) {
		return metersToNauticalMiles(kilometersToMeters(km));
	}

	public static double inchesToMillimeters(double in) {
		return metersToMillimeters(inchesToMeters(in));
	}

	public static double inchesToCentimeters(double in) {
		return metersToCentimeters(inchesToMeters(in));
	}

	public static double inchesToMeters(double in) {
		return in * inchesToMetersFactor;
	}

	public static double inchesToKilometers(double in) {
		return metersToKilometers(inchesToMeters(in));
	}

	public static double inchesToFeet(double in) {
		return in / inchFeetFactor;
	}

	public static double inchesToYards(double in) {
		return in / inchYardFactor;
	}

	public static double inchesToMiles(double in) {
		return in / inchMileFactor;
	}

	public static double inchesToNauticalMiles(double in) {
		return metersToNauticalMiles(inchesToMeters(in));
	}

	public static double feetToMillimeters(double ft) {
		return metersToMillimeters(feetToMeters(ft));
	}

	public static double feetToCentimeters(double ft) {
		return metersToCentimeters(feetToMeters(ft));
	}

	public static double feetToMeters(double ft) {
		return ft * feetToMetersFactor;
	}

	public static double feetToKilometers(double ft) {
		return metersToKilometers(feetToMeters(ft));
	}

	public static double feetToInches(double ft) {
		return ft * inchFeetFactor;
	}

	public static double feetToYards(double ft) {
		return inchesToYards(feetToInches(ft));
	}

	public static double feetToMiles(double ft) {
		return inchesToMiles(feetToInches(ft));
	}

	public static double feetToNauticalMiles(double ft) {
		return inchesToNauticalMiles(feetToInches(ft));
	}

	public static double yardsToMillimeters(double yd) {
		return metersToMillimeters(yardsToMeters(yd));
	}

	public static double yardsToCentimeters(double yd) {
		return metersToCentimeters(yardsToMeters(yd));
	}

	public static double yardsToMeters(double yd) {
		return yd * yardsToMetersFactor;
	}

	public static double yardsToKilometers(double yd) {
		return metersToKilometers(yardsToMeters(yd));
	}

	public static double yardsToInches(double yd) {
		return yd * inchYardFactor;
	}

	public static double yardsToFeet(double yd) {
		return inchesToFeet(yardsToInches(yd));
	}

	public static double yardsToMiles(double yd) {
		return inchesToMiles(yardsToInches(yd));
	}

	public static double yardsToNauticalMiles(double yd) {
		return inchesToNauticalMiles(yardsToInches(yd));
	}

	public static double milesToMillimeters(double mi) {
		return metersToMillimeters(milesToMeters(mi));
	}

	public static double milesToCentimeters(double mi) {
		return metersToCentimeters(milesToMeters(mi));
	}

	public static double milesToMeters(double mi) {
		return mi * milesToMetersFactor;
	}

	public static double milesToKilometers(double mi) {
		return metersToKilometers(milesToMeters(mi));
	}

	public static double milesToInches(double mi) {
		return metersToInches(milesToMeters(mi));
	}

	public static double milesToFeet(double mi) {
		return inchesToFeet(milesToInches(mi));
	}

	public static double milesToYards(double mi) {
		return inchesToYards(milesToInches(mi));
	}

	public static double milesToNauticalMiles(double mi) {
		return inchesToNauticalMiles(milesToInches(mi));
	}

	public static double nauticalMilesToMillimeters(double nm) {
		return metersToMillimeters(nauticalMilesToMeters(nm));
	}

	public static double nauticalMilesToCentimeters(double nm) {
		return metersToCentimeters(nauticalMilesToMeters(nm));
	}

	public static double nauticalMilesToMeters(double nm) {
		return nm * nauticalMilesToMetersFactor;
	}

	public static double nauticalMilesToKilometers(double nm) {
		return metersToKilometers(nauticalMilesToMeters(nm));
	}

	public static double nauticalMilesToInches(double nm) {
		return metersToInches(nauticalMilesToMeters(nm));
	}

	public static double nauticalMilesToFeet(double nm) {
		return metersToFeet(nauticalMilesToMeters(nm));
	}

	public static double nauticalMilesToYards(double nm) {
		return metersToYards(nauticalMilesToMeters(nm));
	}

	public static double nauticalMilesToMiles(double nm) {
		return metersToMiles(nauticalMilesToMeters(nm));
	}

	/*
	 * Mass conversions!!!
	 */
	public static double microgramsToMilligrams(double mcg) {
		return mcg / 1000;
	}

	public static double microgramsToGrams(double mcg) {
		return mcg / 1000000;
	}

	public static double microgramsToKilograms(double mcg) {
		return mcg / 1000000000;
	}

	public static double microgramsToMetrictons(double mcg) {
		return gramsToMetrictons(microgramsToGrams(mcg));
	}

	public static double microgramsToOunces(double mcg) {
		return gramsToOunces(microgramsToGrams(mcg));
	}

	public static double microgramsToPounds(double mcg) {
		return gramsToPounds(microgramsToGrams(mcg));
	}

	public static double microgramsToStones(double mcg) {
		return gramsToStones(microgramsToGrams(mcg));
	}

	public static double microgramsToShorttons(double mcg) {
		return gramsToShorttons(microgramsToGrams(mcg));
	}

	public static double microgramsToLongtons(double mcg) {
		return gramsToLongtons(microgramsToGrams(mcg));
	}

	public static double milligramsToMicrograms(double mg) {
		return mg * 1000;
	}

	public static double milligramsToGrams(double mg) {
		return mg / 1000;
	}

	public static double milligramsToKilograms(double mg) {
		return mg / 1000000;
	}

	public static double milligramsToMetrictons(double mg) {
		return gramsToMetrictons(milligramsToGrams(mg));
	}

	public static double milligramsToOunces(double mg) {
		return gramsToOunces(milligramsToGrams(mg));
	}

	public static double milligramsToPounds(double mg) {
		return gramsToPounds(milligramsToGrams(mg));
	}

	public static double milligramsToStones(double mg) {
		return gramsToStones(milligramsToGrams(mg));
	}

	public static double milligramsToShorttons(double mg) {
		return gramsToShorttons(milligramsToGrams(mg));
	}

	public static double milligramsToLongtons(double mg) {
		return gramsToLongtons(milligramsToGrams(mg));
	}

	public static double gramsToMicrograms(double g) {
		return g * 1000000;
	}

	public static double gramsToMilligrams(double g) {
		return g * 1000;
	}

	public static double gramsToKilograms(double g) {
		return g / 1000;
	}

	public static double gramsToMetrictons(double g) {
		return g / 1000000;
	}

	public static double gramsToOunces(double g) {
		return g * gramsToOuncesFactor;
	}

	public static double gramsToPounds(double g) {
		return g * gramsToPoundsFactor;
	}

	public static double gramsToStones(double g) {
		return g * gramsToStonesFactor;
	}

	public static double gramsToShorttons(double g) {
		return g * gramsToShorttonsFactor;
	}

	public static double gramsToLongtons(double g) {
		return g * gramsToLongtonsFactor;
	}

	public static double kilogramsToMicrograms(double kg) {
		return kg * 1000000000;
	}

	public static double kilogramsToMilligrams(double kg) {
		return kg * 1000000;
	}

	public static double kilogramsToGrams(double kg) {
		return kg * 1000;
	}

	public static double kilogramsToMetrictons(double kg) {
		return kg / 1000;
	}

	public static double kilogramsToOunces(double kg) {
		return gramsToOunces(kilogramsToGrams(kg));
	}

	public static double kilogramsToPounds(double kg) {
		return gramsToPounds(kilogramsToGrams(kg));
	}

	public static double kilogramsToStones(double kg) {
		return gramsToStones(kilogramsToGrams(kg));
	}

	public static double kilogramsToShorttons(double kg) {
		return gramsToShorttons(kilogramsToGrams(kg));
	}

	public static double kilogramsToLongtons(double kg) {
		return gramsToLongtons(kilogramsToGrams(kg));
	}

	public static double metrictonsToMicrograms(double mt) {
		return gramsToMicrograms(metrictonsToGrams(mt));
	}

	public static double metrictonsToMilligrams(double mt) {
		return gramsToMilligrams(metrictonsToGrams(mt));
	}

	public static double metrictonsToGrams(double mt) {
		return mt * 1000000;
	}

	public static double metrictonsToKilograms(double mt) {
		return gramsToKilograms(metrictonsToGrams(mt));
	}

	public static double metrictonsToOunces(double mt) {
		return gramsToOunces(metrictonsToGrams(mt));
	}

	public static double metrictonsToPounds(double mt) {
		return gramsToPounds(metrictonsToGrams(mt));
	}

	public static double metrictonsToStones(double mt) {
		return gramsToStones(metrictonsToGrams(mt));
	}

	public static double metrictonsToShorttons(double mt) {
		return gramsToShorttons(metrictonsToGrams(mt));
	}

	public static double metrictonsToLongtons(double mt) {
		return gramsToLongtons(metrictonsToGrams(mt));
	}

	public static double ouncesToMicrograms(double oz) {
		return gramsToMicrograms(ouncesToGrams(oz));
	}

	public static double ouncesToMilligrams(double oz) {
		return gramsToMilligrams(ouncesToGrams(oz));
	}

	public static double ouncesToGrams(double oz) {
		return oz * 28.3495;
	}

	public static double ouncesToKilograms(double oz) {
		return gramsToKilograms(ouncesToGrams(oz));
	}

	public static double ouncesToMetrictons(double oz) {
		return gramsToMetrictons(ouncesToGrams(oz));
	}

	public static double ouncesToPounds(double oz) {
		return gramsToPounds(ouncesToGrams(oz));
	}

	public static double ouncesToStones(double oz) {
		return gramsToStones(ouncesToGrams(oz));
	}

	public static double ouncesToShorttons(double oz) {
		return gramsToShorttons(ouncesToGrams(oz));
	}

	public static double ouncesToLongtons(double oz) {
		return gramsToLongtons(ouncesToGrams(oz));
	}

	public static double poundsToMicrograms(double lbs) {
		return gramsToMicrograms(poundsToGrams(lbs));
	}

	public static double poundsToMilligrams(double lbs) {
		return gramsToMilligrams(poundsToGrams(lbs));
	}

	public static double poundsToGrams(double lbs) {
		return lbs * 453.592;
	}

	public static double poundsToKilograms(double lbs) {
		return gramsToKilograms(poundsToGrams(lbs));
	}

	public static double poundsToMetrictons(double lbs) {
		return gramsToMetrictons(poundsToGrams(lbs));
	}

	public static double poundsToOunces(double lbs) {
		return gramsToOunces(poundsToGrams(lbs));
	}

	public static double poundsToStones(double lbs) {
		return gramsToStones(poundsToGrams(lbs));
	}

	public static double poundsToShorttons(double lbs) {
		return gramsToShorttons(poundsToGrams(lbs));
	}

	public static double poundsToLongtons(double lbs) {
		return gramsToLongtons(poundsToGrams(lbs));
	}

	public static double stonesToMicrograms(double stn) {
		return gramsToMicrograms(stonesToGrams(stn));
	}

	public static double stonesToMilligrams(double stn) {
		return gramsToMilligrams(stonesToGrams(stn));
	}

	public static double stonesToGrams(double stn) {
		return stn * 6350.29;
	}

	public static double stonesToKilograms(double stn) {
		return gramsToKilograms(stonesToGrams(stn));
	}

	public static double stonesToMetrictons(double stn) {
		return gramsToMetrictons(stonesToGrams(stn));
	}

	public static double stonesToOunces(double stn) {
		return gramsToOunces(stonesToGrams(stn));
	}

	public static double stonesToPounds(double stn) {
		return gramsToPounds(stonesToGrams(stn));
	}

	public static double stonesToShorttons(double stn) {
		return gramsToShorttons(stonesToGrams(stn));
	}

	public static double stonesToLongtons(double stn) {
		return gramsToLongtons(stonesToGrams(stn));
	}

	public static double shorttonsToMicrograms(double st) {
		return gramsToMicrograms(shorttonsToGrams(st));
	}

	public static double shorttonsToMilligrams(double st) {
		return gramsToMilligrams(shorttonsToGrams(st));
	}

	public static double shorttonsToGrams(double st) {
		return st * 907185;
	}

	public static double shorttonsToKilograms(double st) {
		return gramsToKilograms(shorttonsToGrams(st));
	}

	public static double shorttonsToMetrictons(double st) {
		return gramsToMetrictons(shorttonsToGrams(st));
	}

	public static double shorttonsToOunces(double st) {
		return gramsToOunces(shorttonsToGrams(st));
	}

	public static double shorttonsToPounds(double st) {
		return gramsToPounds(shorttonsToGrams(st));
	}

	public static double shorttonsToStones(double st) {
		return gramsToStones(stonesToGrams(st));
	}

	public static double shorttonsToLongtons(double st) {
		return gramsToLongtons(shorttonsToGrams(st));
	}

	public static double longtonsToMicrograms(double lt) {
		return gramsToMicrograms(longtonsToGrams(lt));
	}

	public static double longtonsToMilligrams(double lt) {
		return gramsToMilligrams(longtonsToGrams(lt));
	}

	public static double longtonsToGrams(double lt) {
		return lt * 1016000;
	}

	public static double longtonsToKilograms(double lt) {
		return gramsToKilograms(longtonsToGrams(lt));
	}

	public static double longtonsToMetrictons(double lt) {
		return gramsToMetrictons(longtonsToGrams(lt));
	}

	public static double longtonsToOunces(double lt) {
		return gramsToOunces(longtonsToGrams(lt));
	}

	public static double longtonsToPounds(double lt) {
		return gramsToPounds(longtonsToGrams(lt));
	}

	public static double longtonsToStones(double lt) {
		return gramsToStones(stonesToGrams(lt));
	}

	public static double longtonsToShorttons(double lt) {
		return gramsToShorttons(longtonsToGrams(lt));
	}

	/*
	 * Speed conversions!!!
	 */
	public static double msToKmh(double ms) {
		return metersToKilometers(ms) * hoursToSeconds(1);
	}

	public static double msToFs(double ms) {
		return metersToFeet(ms);
	}

	public static double msToMih(double ms) {
		return metersToMiles(ms) * hoursToSeconds(1);
	}

	public static double msToKnots(double ms) {
		return metersToNauticalMiles(ms) * hoursToSeconds(1);
	}

	public static double kmhToMs(double kmh) {
		return kilometersToMeters(kmh) * secondsToHours(1);
	}

	public static double kmhToFs(double kmh) {
		return kmh * 0.911344;
	}

	public static double kmhToMih(double kmh) {
		return kilometersToMiles(kmh);
	}

	public static double kmhToKnots(double kmh) {
		return kilometersToNauticalMiles(kmh);
	}

	public static double fsToMs(double fs) {
		return feetToMeters(fs);
	}

	public static double fsToKmh(double fs) {
		return feetToKilometers(fs) * hoursToSeconds(1);
	}

	public static double fsToMih(double fs) {
		return feetToMiles(fs) * hoursToSeconds(1);
	}

	public static double fsToKnots(double fs) {
		return feetToNauticalMiles(fs) * hoursToSeconds(1);
	}

	public static double mihToMs(double mih) {
		return milesToMeters(mih) * secondsToHours(1);
	}

	public static double mihToKmh(double mih) {
		return milesToKilometers(mih);
	}

	public static double mihToFs(double mih) {
		return milesToFeet(mih) * secondsToHours(1);
	}

	public static double mihToKnots(double mih) {
		return milesToNauticalMiles(mih);
	}

	public static double knotsToMs(double kn) {
		return nauticalMilesToMeters(kn) * secondsToHours(1);
	}

	public static double knotsToKmh(double kn) {
		return nauticalMilesToKilometers(kn);
	}

	public static double knotsToFs(double kn) {
		return nauticalMilesToFeet(kn) * secondsToHours(1);
	}

	public static double knotsToMih(double kn) {
		return nauticalMilesToMiles(kn);
	}

	/*
	 * Volume conversions!!!
	 */
	public static double millilitersToLiters(double ml) {
		return ml / 1000;
	}

	public static double millilitersToCubicMeters(double ml) {
		return litersToCubicMeters(millilitersToLiters(ml));
	}

	public static double millilitersToCubicInches(double ml) {
		return litersToCubicInches(millilitersToLiters(ml));
	}

	public static double millilitersToCubicFeet(double ml) {
		return litersToCubicFeet(millilitersToLiters(ml));
	}

	public static double millilitersToUSTsp(double ml) {
		return litersToUSTsp(millilitersToLiters(ml));
	}

	public static double millilitersToUSTbsp(double ml) {
		return litersToUSTbsp(millilitersToLiters(ml));
	}

	public static double millilitersToUSOunces(double ml) {
		return litersToUSOunces(millilitersToLiters(ml));
	}

	public static double millilitersToUSCups(double ml) {
		return litersToUSCups(millilitersToLiters(ml));
	}

	public static double millilitersToUSPints(double ml) {
		return litersToUSPints(millilitersToLiters(ml));
	}

	public static double millilitersToUSQuarts(double ml) {
		return litersToUSQuarts(millilitersToLiters(ml));
	}

	public static double millilitersToUSGallons(double ml) {
		return litersToUSGallons(millilitersToLiters(ml));
	}

	public static double millilitersToImperialTsp(double ml) {
		return litersToImperialTsp(millilitersToLiters(ml));
	}

	public static double millilitersToImperialTbsp(double ml) {
		return litersToImperialTbsp(millilitersToLiters(ml));
	}

	public static double millilitersToImperialOunces(double ml) {
		return litersToImperialOunces(millilitersToLiters(ml));
	}

	public static double millilitersToImperialPints(double ml) {
		return litersToImperialPints(millilitersToLiters(ml));
	}

	public static double millilitersToImperialQuarts(double ml) {
		return litersToImperialQuarts(millilitersToLiters(ml));
	}

	public static double millilitersToImperialGallons(double ml) {
		return litersToImperialGallons(millilitersToLiters(ml));
	}

	public static double litersToMilliliters(double l) {
		return l * 1000;
	}

	public static double litersToCubicMeters(double l) {
		return l / 1000;
	}

	public static double litersToCubicInches(double l) {
		return l * litersToCubicInchesFactor;
	}

	public static double litersToCubicFeet(double l) {
		return l * litersToCubicFeetFactor;
	}

	public static double litersToUSTsp(double l) {
		return l * litersToUSTspFactor;
	}

	public static double litersToUSTbsp(double l) {
		return l * litersToUSTbspFactor;
	}

	public static double litersToUSOunces(double l) {
		return l * litersToUSOuncesFactor;
	}

	public static double litersToUSCups(double l) {
		return l * litersToUSCupsFactor;
	}

	public static double litersToUSPints(double l) {
		return l * litersToUSPintsFactor;
	}

	public static double litersToUSQuarts(double l) {
		return l * litersToUSQuartsFactor;
	}

	public static double litersToUSGallons(double l) {
		return l * litersToUSGallonsFactor;
	}

	public static double litersToImperialTsp(double l) {
		return l * litersToImperialTspFactor;
	}

	public static double litersToImperialTbsp(double l) {
		return l * litersToImperialTbspFactor;
	}

	public static double litersToImperialOunces(double l) {
		return l * litersToImperialOuncesFactor;
	}

	public static double litersToImperialPints(double l) {
		return l * litersToImperialPintsFactor;
	}

	public static double litersToImperialQuarts(double l) {
		return l * litersToImperialQuartsFactor;
	}

	public static double litersToImperialGallons(double l) {
		return l * litersToImperialGallons;
	}

	public static double cubicMetersToMilliliters(double cub) {
		return litersToMilliliters(cubicMetersToLiters(cub));
	}

	public static double cubicMetersToLiters(double cub) {
		return cub * 1000;
	}

	public static double cubicMetersToCubicInches(double cub) {
		return litersToCubicInches(cubicMetersToLiters(cub));
	}

	public static double cubicMetersToCubicFeet(double cub) {
		return litersToCubicFeet(cubicMetersToLiters(cub));
	}

	public static double cubicMetersToUSTsp(double cub) {
		return litersToUSTsp(cubicMetersToLiters(cub));
	}

	public static double cubicMetersToUSTbsp(double cub) {
		return litersToUSTbsp(cubicMetersToLiters(cub));
	}

	public static double cubicMetersToUSOunces(double cub) {
		return litersToUSOunces(cubicMetersToLiters(cub));
	}

	public static double cubicMetersToUSCups(double cub) {
		return litersToUSCups(cubicMetersToLiters(cub));
	}

	public static double cubicMetersToUSPints(double cub) {
		return litersToUSPints(cubicMetersToLiters(cub));
	}

	public static double cubicMetersToUSQuarts(double cub) {
		return litersToUSQuarts(cubicMetersToLiters(cub));
	}

	public static double cubicMetersToUSGallons(double cub) {
		return litersToUSGallons(cubicMetersToLiters(cub));
	}

	public static double cubicMetersToImperialTsp(double cub) {
		return litersToImperialTsp(cubicMetersToLiters(cub));
	}

	public static double cubicMetersToImperialTbsp(double cub) {
		return litersToImperialTbsp(cubicMetersToLiters(cub));
	}

	public static double cubicMetersToImperialOunces(double cub) {
		return litersToImperialOunces(cubicMetersToLiters(cub));
	}

	public static double cubicMetersToImperialPints(double cub) {
		return litersToImperialPints(cubicMetersToLiters(cub));
	}

	public static double cubicMetersToImperialQuarts(double cub) {
		return litersToImperialQuarts(cubicMetersToLiters(cub));
	}

	public static double cubicMetersToImperialGallons(double cub) {
		return litersToImperialGallons(cubicMetersToLiters(cub));
	}

	public static double cubicInchesToMilliliters(double ci) {
		return litersToMilliliters(cubicInchesToLiters(ci));
	}

	public static double cubicInchesToLiters(double ci) {
		return ci * cubicInchesToLitersFactor;
	}

	public static double cubicInchesToCubicMeters(double ci) {
		return litersToCubicMeters(cubicInchesToLiters(ci));
	}

	public static double cubicInchesToCubicFeet(double ci) {
		return litersToCubicFeet(cubicInchesToLiters(ci));
	}

	public static double cubicInchesToUSTsp(double ci) {
		return litersToUSTsp(cubicInchesToLiters(ci));
	}

	public static double cubicInchesToUSTbsp(double ci) {
		return litersToUSTbsp(cubicInchesToLiters(ci));
	}

	public static double cubicInchesToUSOunces(double ci) {
		return litersToUSOunces(cubicInchesToLiters(ci));
	}

	public static double cubicInchesToUSCups(double ci) {
		return litersToUSCups(cubicInchesToLiters(ci));
	}

	public static double cubicInchesToUSPints(double ci) {
		return litersToUSPints(cubicInchesToLiters(ci));
	}

	public static double cubicInchesToUSQuarts(double ci) {
		return litersToUSQuarts(cubicInchesToLiters(ci));
	}

	public static double cubicInchesToUSGallons(double ci) {
		return litersToUSGallons(cubicInchesToLiters(ci));
	}

	public static double cubicInchesToImperialTsp(double ci) {
		return litersToImperialTsp(cubicInchesToLiters(ci));
	}

	public static double cubicInchesToImperialTbsp(double ci) {
		return litersToImperialTbsp(cubicInchesToLiters(ci));
	}

	public static double cubicInchesToImperialOunces(double ci) {
		return litersToImperialOunces(cubicInchesToLiters(ci));
	}

	public static double cubicInchesToImperialPints(double ci) {
		return litersToImperialPints(cubicInchesToLiters(ci));
	}

	public static double cubicInchesToImperialQuarts(double ci) {
		return litersToImperialQuarts(cubicInchesToLiters(ci));
	}

	public static double cubicInchesToImperialGallons(double ci) {
		return litersToImperialGallons(cubicInchesToLiters(ci));
	}

	public static double cubicFeetToMilliliters(double ci) {
		return litersToMilliliters(cubicFeetToLiters(ci));
	}

	public static double cubicFeetToLiters(double ci) {
		return ci * cubicFeetToLitersFactor;
	}

	public static double cubicFeetToCubicMeters(double ci) {
		return litersToCubicMeters(cubicFeetToLiters(ci));
	}

	public static double cubicFeetToCubicInches(double ci) {
		return litersToCubicFeet(cubicFeetToLiters(ci));
	}

	public static double cubicFeetToUSTsp(double ci) {
		return litersToUSTsp(cubicFeetToLiters(ci));
	}

	public static double cubicFeetToUSTbsp(double ci) {
		return litersToUSTbsp(cubicFeetToLiters(ci));
	}

	public static double cubicFeetToUSOunces(double ci) {
		return litersToUSOunces(cubicFeetToLiters(ci));
	}

	public static double cubicFeetToUSCups(double ci) {
		return litersToUSCups(cubicFeetToLiters(ci));
	}

	public static double cubicFeetToUSPints(double ci) {
		return litersToUSPints(cubicFeetToLiters(ci));
	}

	public static double cubicFeetToUSQuarts(double ci) {
		return litersToUSQuarts(cubicFeetToLiters(ci));
	}

	public static double cubicFeetToUSGallons(double ci) {
		return litersToUSGallons(cubicFeetToLiters(ci));
	}

	public static double cubicFeetToImperialTsp(double ci) {
		return litersToImperialTsp(cubicFeetToLiters(ci));
	}

	public static double cubicFeetToImperialTbsp(double ci) {
		return litersToImperialTbsp(cubicFeetToLiters(ci));
	}

	public static double cubicFeetToImperialOunces(double ci) {
		return litersToImperialOunces(cubicFeetToLiters(ci));
	}

	public static double cubicFeetToImperialPints(double ci) {
		return litersToImperialPints(cubicFeetToLiters(ci));
	}

	public static double cubicFeetToImperialQuarts(double ci) {
		return litersToImperialQuarts(cubicFeetToLiters(ci));
	}

	public static double cubicFeetToImperialGallons(double ci) {
		return litersToImperialGallons(cubicFeetToLiters(ci));
	}

	public static double usTspToMilliliters(double ustsp) {
		return litersToMilliliters(usTspToLiters(ustsp));
	}

	public static double usTspToLiters(double ustsp) {
		return ustsp * usTspToLitersFactor;
	}

	public static double usTspToCubicMeters(double ustsp) {
		return litersToCubicMeters(usTspToLiters(ustsp));
	}

	public static double usTspToCubicInches(double ustsp) {
		return litersToCubicInches(usTspToLiters(ustsp));
	}

	public static double usTspToCubicFeet(double ustsp) {
		return litersToCubicFeet(usTspToLiters(ustsp));
	}

	public static double usTspToUSTbsp(double ustsp) {
		return litersToUSTbsp(usTspToLiters(ustsp));
	}

	public static double usTspToUSOunces(double ustsp) {
		return litersToUSOunces(usTspToLiters(ustsp));
	}

	public static double usTspToUSCups(double ustsp) {
		return litersToUSCups(usTspToLiters(ustsp));
	}

	public static double usTspToUSPints(double ustsp) {
		return litersToUSPints(usTspToLiters(ustsp));
	}

	public static double usTspToUSQuarts(double ustsp) {
		return litersToUSQuarts(usTspToLiters(ustsp));
	}

	public static double usTspToUSGallons(double ustsp) {
		return litersToUSGallons(usTspToLiters(ustsp));
	}

	public static double usTspToImperialTsp(double ustsp) {
		return litersToImperialTsp(usTspToLiters(ustsp));
	}

	public static double usTspToImperialTbsp(double ustsp) {
		return litersToImperialTbsp(usTspToLiters(ustsp));
	}

	public static double usTspToImperialOunces(double ustsp) {
		return litersToImperialOunces(usTspToLiters(ustsp));
	}

	public static double usTspToImperialPints(double ustsp) {
		return litersToImperialPints(usTspToLiters(ustsp));
	}

	public static double usTspToImperialQuarts(double ustsp) {
		return litersToImperialQuarts(usTspToLiters(ustsp));
	}

	public static double usTspToImperialGallons(double ustsp) {
		return litersToImperialGallons(usTspToLiters(ustsp));
	}

	public static double usTbspToMilliliters(double ustbsp) {
		return litersToMilliliters(usTbspToLiters(ustbsp));
	}

	public static double usTbspToLiters(double ustbsp) {
		return ustbsp * usTbspToLitersFactor;
	}

	public static double usTbspToCubicMeters(double ustbsp) {
		return litersToCubicMeters(usTbspToLiters(ustbsp));
	}

	public static double usTbspToCubicInches(double ustbsp) {
		return litersToCubicInches(usTbspToLiters(ustbsp));
	}

	public static double usTbspToCubicFeet(double ustbsp) {
		return litersToCubicFeet(usTbspToLiters(ustbsp));
	}

	public static double usTbspToUSTsp(double ustbsp) {
		return litersToUSTsp(usTbspToLiters(ustbsp));
	}

	public static double usTbspToUSOunces(double ustbsp) {
		return litersToUSOunces(usTbspToLiters(ustbsp));
	}

	public static double usTbspToUSCups(double ustbsp) {
		return litersToUSCups(usTbspToLiters(ustbsp));
	}

	public static double usTbspToUSPints(double ustbsp) {
		return litersToUSPints(usTbspToLiters(ustbsp));
	}

	public static double usTbspToUSQuarts(double ustbsp) {
		return litersToUSQuarts(usTbspToLiters(ustbsp));
	}

	public static double usTbspToUSGallons(double ustbsp) {
		return litersToUSGallons(usTbspToLiters(ustbsp));
	}

	public static double usTbspToImperialTsp(double ustbsp) {
		return litersToImperialTsp(usTbspToLiters(ustbsp));
	}

	public static double usTbspToImperialTbsp(double ustbsp) {
		return litersToImperialTbsp(usTbspToLiters(ustbsp));
	}

	public static double usTbspToImperialOunces(double ustbsp) {
		return litersToImperialOunces(usTbspToLiters(ustbsp));
	}

	public static double usTbspToImperialPints(double ustbsp) {
		return litersToImperialPints(usTbspToLiters(ustbsp));
	}

	public static double usTbspToImperialQuarts(double ustbsp) {
		return litersToImperialQuarts(usTbspToLiters(ustbsp));
	}

	public static double usTbspToImperialGallons(double ustbsp) {
		return litersToImperialGallons(usTbspToLiters(ustbsp));
	}

	public static double usOuncesToMilliliters(double usoz) {
		return litersToMilliliters(usOuncesToLiters(usoz));
	}

	public static double usOuncesToLiters(double usoz) {
		return usoz * usOuncesToLitersFactor;
	}

	public static double usOuncesToCubicMeters(double usoz) {
		return litersToCubicMeters(usOuncesToLiters(usoz));
	}

	public static double usOuncesToCubicInches(double usoz) {
		return litersToCubicInches(usOuncesToLiters(usoz));
	}

	public static double usOuncesToCubicFeet(double usoz) {
		return litersToCubicFeet(usOuncesToLiters(usoz));
	}

	public static double usOuncesToUSTsp(double usoz) {
		return litersToUSTsp(usOuncesToLiters(usoz));
	}

	public static double usOuncesToUSTbsp(double usoz) {
		return litersToUSTbsp(usOuncesToLiters(usoz));
	}

	public static double usOuncesToUSCups(double usoz) {
		return litersToUSCups(usOuncesToLiters(usoz));
	}

	public static double usOuncesToUSPints(double usoz) {
		return litersToUSPints(usOuncesToLiters(usoz));
	}

	public static double usOuncesToUSQuarts(double usoz) {
		return litersToUSQuarts(usOuncesToLiters(usoz));
	}

	public static double usOuncesToUSGallons(double usoz) {
		return litersToUSGallons(usOuncesToLiters(usoz));
	}

	public static double usOuncesToImperialTsp(double usoz) {
		return litersToImperialTsp(usOuncesToLiters(usoz));
	}

	public static double usOuncesToImperialTbsp(double usoz) {
		return litersToImperialTbsp(usOuncesToLiters(usoz));
	}

	public static double usOuncesToImperialOunces(double usoz) {
		return litersToImperialOunces(usOuncesToLiters(usoz));
	}

	public static double usOuncesToImperialPints(double usoz) {
		return litersToImperialPints(usOuncesToLiters(usoz));
	}

	public static double usOuncesToImperialQuarts(double usoz) {
		return litersToImperialQuarts(usOuncesToLiters(usoz));
	}

	public static double usOuncesToImperialGallons(double usoz) {
		return litersToImperialGallons(usOuncesToLiters(usoz));
	}

	public static double usCupsToMilliliters(double usc) {
		return litersToMilliliters(usCupsToLiters(usc));
	}

	public static double usCupsToLiters(double usc) {
		return usc * usCupsToLitersFactor;
	}

	public static double usCupsToCubicMeters(double usc) {
		return litersToCubicMeters(usCupsToLiters(usc));
	}

	public static double usCupsToCubicInches(double usc) {
		return litersToCubicInches(usCupsToLiters(usc));
	}

	public static double usCupsToCubicFeet(double usc) {
		return litersToCubicFeet(usCupsToLiters(usc));
	}

	public static double usCupsToUSTsp(double usc) {
		return litersToUSTsp(usCupsToLiters(usc));
	}

	public static double usCupsToUSTbsp(double usc) {
		return litersToUSTbsp(usCupsToLiters(usc));
	}

	public static double usCupsToUSOunces(double usc) {
		return litersToUSOunces(usCupsToLiters(usc));
	}

	public static double usCupsToUSPints(double usc) {
		return litersToUSPints(usCupsToLiters(usc));
	}

	public static double usCupsToUSQuarts(double usc) {
		return litersToUSQuarts(usCupsToLiters(usc));
	}

	public static double usCupsToUSGallons(double usc) {
		return litersToUSGallons(usCupsToLiters(usc));
	}

	public static double usCupsToImperialTsp(double usc) {
		return litersToImperialTsp(usCupsToLiters(usc));
	}

	public static double usCupsToImperialTbsp(double usc) {
		return litersToImperialTbsp(usCupsToLiters(usc));
	}

	public static double usCupsToImperialOunces(double usc) {
		return litersToImperialOunces(usCupsToLiters(usc));
	}

	public static double usCupsToImperialPints(double usc) {
		return litersToImperialPints(usCupsToLiters(usc));
	}

	public static double usCupsToImperialQuarts(double usc) {
		return litersToImperialQuarts(usCupsToLiters(usc));
	}

	public static double usCupsToImperialGallons(double usc) {
		return litersToImperialGallons(usCupsToLiters(usc));
	}

	public static double usPintsToMilliliters(double usp) {
		return litersToMilliliters(usPintsToLiters(usp));
	}

	public static double usPintsToLiters(double usp) {
		return usp * usPintsToLitersFactor;
	}

	public static double usPintsToCubicMeters(double usp) {
		return litersToCubicMeters(usPintsToLiters(usp));
	}

	public static double usPintsToCubicInches(double usp) {
		return litersToCubicInches(usPintsToLiters(usp));
	}

	public static double usPintsToCubicFeet(double usp) {
		return litersToCubicFeet(usPintsToLiters(usp));
	}

	public static double usPintsToUSTsp(double usp) {
		return litersToUSTsp(usPintsToLiters(usp));
	}

	public static double usPintsToUSTbsp(double usp) {
		return litersToUSTbsp(usPintsToLiters(usp));
	}

	public static double usPintsToUSOunces(double usp) {
		return litersToUSOunces(usPintsToLiters(usp));
	}

	public static double usPintsToUSCups(double usp) {
		return litersToUSCups(usPintsToLiters(usp));
	}

	public static double usPintsToUSQuarts(double usp) {
		return litersToUSQuarts(usPintsToLiters(usp));
	}

	public static double usPintsToUSGallons(double usp) {
		return litersToUSGallons(usPintsToLiters(usp));
	}

	public static double usPintsToImperialTsp(double usp) {
		return litersToImperialTsp(usPintsToLiters(usp));
	}

	public static double usPintsToImperialTbsp(double usp) {
		return litersToImperialTbsp(usPintsToLiters(usp));
	}

	public static double usPintsToImperialOunces(double usp) {
		return litersToImperialOunces(usPintsToLiters(usp));
	}

	public static double usPintsToImperialPints(double usp) {
		return litersToImperialPints(usPintsToLiters(usp));
	}

	public static double usPintsToImperialQuarts(double usp) {
		return litersToImperialQuarts(usPintsToLiters(usp));
	}

	public static double usPintsToImperialGallons(double usp) {
		return litersToImperialGallons(usPintsToLiters(usp));
	}

	public static double usQuartsToMilliliters(double usq) {
		return litersToMilliliters(usQuartsToLiters(usq));
	}

	public static double usQuartsToLiters(double usq) {
		return usq * usQuartsToLitersFactor;
	}

	public static double usQuartsToCubicMeters(double usq) {
		return litersToCubicMeters(usQuartsToLiters(usq));
	}

	public static double usQuartsToCubicInches(double usq) {
		return litersToCubicInches(usQuartsToLiters(usq));
	}

	public static double usQuartsToCubicFeet(double usq) {
		return litersToCubicFeet(usQuartsToLiters(usq));
	}

	public static double usQuartsToUSTsp(double usq) {
		return litersToUSTsp(usQuartsToLiters(usq));
	}

	public static double usQuartsToUSTbsp(double usq) {
		return litersToUSTbsp(usQuartsToLiters(usq));
	}

	public static double usQuartsToUSOunces(double usq) {
		return litersToUSOunces(usQuartsToLiters(usq));
	}

	public static double usQuartsToUSCups(double usq) {
		return litersToUSCups(usQuartsToLiters(usq));
	}

	public static double usQuartsToUSPints(double usq) {
		return litersToUSPints(usQuartsToLiters(usq));
	}

	public static double usQuartsToUSGallons(double usq) {
		return litersToUSGallons(usQuartsToLiters(usq));
	}

	public static double usQuartsToImperialTsp(double usq) {
		return litersToImperialTsp(usQuartsToLiters(usq));
	}

	public static double usQuartsToImperialTbsp(double usq) {
		return litersToImperialTbsp(usQuartsToLiters(usq));
	}

	public static double usQuartsToImperialOunces(double usq) {
		return litersToImperialOunces(usQuartsToLiters(usq));
	}

	public static double usQuartsToImperialPints(double usq) {
		return litersToImperialPints(usQuartsToLiters(usq));
	}

	public static double usQuartsToImperialQuarts(double usq) {
		return litersToImperialQuarts(usQuartsToLiters(usq));
	}

	public static double usQuartsToImperialGallons(double usq) {
		return litersToImperialGallons(usQuartsToLiters(usq));
	}

	public static double usGallonsToMilliliters(double usgal) {
		return litersToMilliliters(usGallonsToLiters(usgal));
	}

	public static double usGallonsToLiters(double usgal) {
		return usgal * usGallonsToLitersFactor;
	}

	public static double usGallonsToCubicMeters(double usgal) {
		return litersToCubicMeters(usGallonsToLiters(usgal));
	}

	public static double usGallonsToCubicInches(double usgal) {
		return litersToCubicInches(usGallonsToLiters(usgal));
	}

	public static double usGallonsToCubicFeet(double usgal) {
		return litersToCubicFeet(usGallonsToLiters(usgal));
	}

	public static double usGallonsToUSTsp(double usgal) {
		return litersToUSTsp(usGallonsToLiters(usgal));
	}

	public static double usGallonsToUSTbsp(double usgal) {
		return litersToUSTbsp(usGallonsToLiters(usgal));
	}

	public static double usGallonsToUSOunces(double usgal) {
		return litersToUSOunces(usGallonsToLiters(usgal));
	}

	public static double usGallonsToUSCups(double usgal) {
		return litersToUSCups(usGallonsToLiters(usgal));
	}

	public static double usGallonsToUSPints(double usgal) {
		return litersToUSPints(usGallonsToLiters(usgal));
	}

	public static double usGallonsToUSQuarts(double usgal) {
		return litersToUSQuarts(usGallonsToLiters(usgal));
	}

	public static double usGallonsToImperialTsp(double usgal) {
		return litersToImperialTsp(usGallonsToLiters(usgal));
	}

	public static double usGallonsToImperialTbsp(double usgal) {
		return litersToImperialTbsp(usGallonsToLiters(usgal));
	}

	public static double usGallonsToImperialOunces(double usgal) {
		return litersToImperialOunces(usGallonsToLiters(usgal));
	}

	public static double usGallonsToImperialPints(double usgal) {
		return litersToImperialPints(usGallonsToLiters(usgal));
	}

	public static double usGallonsToImperialQuarts(double usgal) {
		return litersToImperialQuarts(usGallonsToLiters(usgal));
	}

	public static double usGallonsToImperialGallons(double usgal) {
		return litersToImperialGallons(usGallonsToLiters(usgal));
	}

	public static double imperialTspToMilliliters(double imptsp) {
		return litersToMilliliters(imperialTspToLiters(imptsp));
	}

	public static double imperialTspToLiters(double imptsp) {
		return imptsp * imperialTspToLitersFactor;
	}

	public static double imperialTspToCubicMeters(double imptsp) {
		return litersToCubicMeters(imperialTspToLiters(imptsp));
	}

	public static double imperialTspToCubicInches(double imptsp) {
		return litersToCubicInches(imperialTspToLiters(imptsp));
	}

	public static double imperialTspToCubicFeet(double imptsp) {
		return litersToCubicFeet(imperialTspToLiters(imptsp));
	}

	public static double imperialTspToUSTsp(double imptsp) {
		return litersToUSTsp(imperialTspToLiters(imptsp));
	}

	public static double imperialTspToUSTbsp(double imptsp) {
		return litersToUSTbsp(imperialTspToLiters(imptsp));
	}

	public static double imperialTspToUSOunces(double imptsp) {
		return litersToUSOunces(imperialTspToLiters(imptsp));
	}

	public static double imperialTspToUSCups(double imptsp) {
		return litersToUSCups(imperialTspToLiters(imptsp));
	}

	public static double imperialTspToUSPints(double imptsp) {
		return litersToUSPints(imperialTspToLiters(imptsp));
	}

	public static double imperialTspToUSQuarts(double imptsp) {
		return litersToUSQuarts(imperialTspToLiters(imptsp));
	}

	public static double imperialTspToUSGallons(double imptsp) {
		return litersToUSGallons(imperialTspToLiters(imptsp));
	}

	public static double imperialTspToImperialTbsp(double imptsp) {
		return litersToImperialTbsp(imperialTspToLiters(imptsp));
	}

	public static double imperialTspToImperialOunces(double imptsp) {
		return litersToImperialOunces(imperialTspToLiters(imptsp));
	}

	public static double imperialTspToImperialPints(double imptsp) {
		return litersToImperialPints(imperialTspToLiters(imptsp));
	}

	public static double imperialTspToImperialQuarts(double imptsp) {
		return litersToImperialQuarts(imperialTspToLiters(imptsp));
	}

	public static double imperialTspToImperialGallons(double imptsp) {
		return litersToImperialGallons(imperialTspToLiters(imptsp));
	}

	public static double imperialTbspToMilliliters(double imptbsp) {
		return litersToMilliliters(imperialTbspToLiters(imptbsp));
	}

	public static double imperialTbspToLiters(double imptbsp) {
		return imptbsp * imperialTbspToLitersFactor;
	}

	public static double imperialTbspToCubicMeters(double imptbsp) {
		return litersToCubicMeters(imperialTbspToLiters(imptbsp));
	}

	public static double imperialTbspToCubicInches(double imptbsp) {
		return litersToCubicInches(imperialTbspToLiters(imptbsp));
	}

	public static double imperialTbspToCubicFeet(double imptbsp) {
		return litersToCubicFeet(imperialTbspToLiters(imptbsp));
	}

	public static double imperialTbspToUSTsp(double imptbsp) {
		return litersToUSTsp(imperialTbspToLiters(imptbsp));
	}

	public static double imperialTbspToUSTbsp(double imptbsp) {
		return litersToUSTbsp(imperialTbspToLiters(imptbsp));
	}

	public static double imperialTbspToUSOunces(double imptbsp) {
		return litersToUSOunces(imperialTbspToLiters(imptbsp));
	}

	public static double imperialTbspToUSCups(double imptbsp) {
		return litersToUSCups(imperialTbspToLiters(imptbsp));
	}

	public static double imperialTbspToUSPints(double imptbsp) {
		return litersToUSPints(imperialTbspToLiters(imptbsp));
	}

	public static double imperialTbspToUSQuarts(double imptbsp) {
		return litersToUSQuarts(imperialTbspToLiters(imptbsp));
	}

	public static double imperialTbspToUSGallons(double imptbsp) {
		return litersToUSGallons(imperialTbspToLiters(imptbsp));
	}

	public static double imperialTbspToImperialTsp(double imptbsp) {
		return litersToImperialTsp(imperialTbspToLiters(imptbsp));
	}

	public static double imperialTbspToImperialOunces(double imptbsp) {
		return litersToImperialOunces(imperialTbspToLiters(imptbsp));
	}

	public static double imperialTbspToImperialPints(double imptbsp) {
		return litersToImperialPints(imperialTbspToLiters(imptbsp));
	}

	public static double imperialTbspToImperialQuarts(double imptbsp) {
		return litersToImperialQuarts(imperialTbspToLiters(imptbsp));
	}

	public static double imperialTbspToImperialGallons(double imptbsp) {
		return litersToImperialGallons(imperialTbspToLiters(imptbsp));
	}

	public static double imperialOuncesToMilliliters(double imptbsp) {
		return litersToMilliliters(imperialOuncesToLiters(imptbsp));
	}

	public static double imperialOuncesToLiters(double imptbsp) {
		return imptbsp * imperialOuncesToLitersFactor;
	}

	public static double imperialOuncesToCubicMeters(double imptbsp) {
		return litersToCubicMeters(imperialOuncesToLiters(imptbsp));
	}

	public static double imperialOuncesToCubicInches(double imptbsp) {
		return litersToCubicInches(imperialOuncesToLiters(imptbsp));
	}

	public static double imperialOuncesToCubicFeet(double imptbsp) {
		return litersToCubicFeet(imperialOuncesToLiters(imptbsp));
	}

	public static double imperialOuncesToUSTsp(double imptbsp) {
		return litersToUSTsp(imperialOuncesToLiters(imptbsp));
	}

	public static double imperialOuncesToUSTbsp(double imptbsp) {
		return litersToUSTbsp(imperialOuncesToLiters(imptbsp));
	}

	public static double imperialOuncesToUSOunces(double imptbsp) {
		return litersToUSOunces(imperialOuncesToLiters(imptbsp));
	}

	public static double imperialOuncesToUSCups(double imptbsp) {
		return litersToUSCups(imperialOuncesToLiters(imptbsp));
	}

	public static double imperialOuncesToUSPints(double imptbsp) {
		return litersToUSPints(imperialOuncesToLiters(imptbsp));
	}

	public static double imperialOuncesToUSQuarts(double imptbsp) {
		return litersToUSQuarts(imperialOuncesToLiters(imptbsp));
	}

	public static double imperialOuncesToUSGallons(double imptbsp) {
		return litersToUSGallons(imperialOuncesToLiters(imptbsp));
	}

	public static double imperialOuncesToImperialTsp(double imptbsp) {
		return litersToImperialTsp(imperialOuncesToLiters(imptbsp));
	}

	public static double imperialOuncesToImperialTbsp(double imptbsp) {
		return litersToImperialTbsp(imperialOuncesToLiters(imptbsp));
	}

	public static double imperialOuncesToImperialPints(double imptbsp) {
		return litersToImperialPints(imperialOuncesToLiters(imptbsp));
	}

	public static double imperialOuncesToImperialQuarts(double imptbsp) {
		return litersToImperialQuarts(imperialOuncesToLiters(imptbsp));
	}

	public static double imperialOuncesToImperialGallons(double imptbsp) {
		return litersToImperialGallons(imperialOuncesToLiters(imptbsp));
	}

	public static double imperialPintsToMilliliters(double imppt) {
		return litersToMilliliters(imperialPintsToLiters(imppt));
	}

	public static double imperialPintsToLiters(double imppt) {
		return imppt * imperialPintsToLitersFactor;
	}

	public static double imperialPintsToCubicMeters(double imppt) {
		return litersToCubicMeters(imperialPintsToLiters(imppt));
	}

	public static double imperialPintsToCubicInches(double imppt) {
		return litersToCubicInches(imperialPintsToLiters(imppt));
	}

	public static double imperialPintsToCubicFeet(double imppt) {
		return litersToCubicFeet(imperialPintsToLiters(imppt));
	}

	public static double imperialPintsToUSTsp(double imppt) {
		return litersToUSTsp(imperialPintsToLiters(imppt));
	}

	public static double imperialPintsToUSTbsp(double imppt) {
		return litersToUSTbsp(imperialPintsToLiters(imppt));
	}

	public static double imperialPintsToUSOunces(double imppt) {
		return litersToUSOunces(imperialPintsToLiters(imppt));
	}

	public static double imperialPintsToUSCups(double imppt) {
		return litersToUSCups(imperialPintsToLiters(imppt));
	}

	public static double imperialPintsToUSPints(double imppt) {
		return litersToUSPints(imperialPintsToLiters(imppt));
	}

	public static double imperialPintsToUSQuarts(double imppt) {
		return litersToUSQuarts(imperialPintsToLiters(imppt));
	}

	public static double imperialPintsToUSGallons(double imppt) {
		return litersToUSGallons(imperialPintsToLiters(imppt));
	}

	public static double imperialPintsToImperialTsp(double imppt) {
		return litersToImperialTsp(imperialPintsToLiters(imppt));
	}

	public static double imperialPintsToImperialTbsp(double imppt) {
		return litersToImperialTbsp(imperialPintsToLiters(imppt));
	}

	public static double imperialPintsToImperialOunces(double imppt) {
		return litersToImperialOunces(imperialPintsToLiters(imppt));
	}

	public static double imperialPintsToImperialQuarts(double imppt) {
		return litersToImperialQuarts(imperialPintsToLiters(imppt));
	}

	public static double imperialPintsToImperialGallons(double imppt) {
		return litersToImperialGallons(imperialPintsToLiters(imppt));
	}

	public static double imperialQuartsToMilliliters(double impq) {
		return litersToMilliliters(imperialQuartsToLiters(impq));
	}

	public static double imperialQuartsToLiters(double impq) {
		return impq * imperialQuartsToLitersFactor;
	}

	public static double imperialQuartsToCubicMeters(double impq) {
		return litersToCubicMeters(imperialQuartsToLiters(impq));
	}

	public static double imperialQuartsToCubicInches(double impq) {
		return litersToCubicInches(imperialQuartsToLiters(impq));
	}

	public static double imperialQuartsToCubicFeet(double impq) {
		return litersToCubicFeet(imperialQuartsToLiters(impq));
	}

	public static double imperialQuartsToUSTsp(double impq) {
		return litersToUSTsp(imperialQuartsToLiters(impq));
	}

	public static double imperialQuartsToUSTbsp(double impq) {
		return litersToUSTbsp(imperialQuartsToLiters(impq));
	}

	public static double imperialQuartsToUSOunces(double impq) {
		return litersToUSOunces(imperialQuartsToLiters(impq));
	}

	public static double imperialQuartsToUSCups(double impq) {
		return litersToUSCups(imperialQuartsToLiters(impq));
	}

	public static double imperialQuartsToUSPints(double impq) {
		return litersToUSPints(imperialQuartsToLiters(impq));
	}

	public static double imperialQuartsToUSQuarts(double impq) {
		return litersToUSQuarts(imperialQuartsToLiters(impq));
	}

	public static double imperialQuartsToUSGallons(double impq) {
		return litersToUSGallons(imperialQuartsToLiters(impq));
	}

	public static double imperialQuartsToImperialTsp(double impq) {
		return litersToImperialTsp(imperialQuartsToLiters(impq));
	}

	public static double imperialQuartsToImperialTbsp(double impq) {
		return litersToImperialTbsp(imperialQuartsToLiters(impq));
	}

	public static double imperialQuartsToImperialOunces(double impq) {
		return litersToImperialOunces(imperialQuartsToLiters(impq));
	}

	public static double imperialQuartsToImperialPints(double impq) {
		return litersToImperialPints(imperialQuartsToLiters(impq));
	}

	public static double imperialQuartsToImperialGallons(double impq) {
		return litersToImperialGallons(imperialQuartsToLiters(impq));
	}

	public static double imperialGallonsToMilliliters(double impgal) {
		return litersToMilliliters(imperialGallonsToLiters(impgal));
	}

	public static double imperialGallonsToLiters(double impgal) {
		return impgal * imperialGallonsToLitersFactor;
	}

	public static double imperialGallonsToCubicMeters(double impgal) {
		return litersToCubicMeters(imperialGallonsToLiters(impgal));
	}

	public static double imperialGallonsToCubicInches(double impgal) {
		return litersToCubicInches(imperialGallonsToLiters(impgal));
	}

	public static double imperialGallonsToCubicFeet(double impgal) {
		return litersToCubicFeet(imperialGallonsToLiters(impgal));
	}

	public static double imperialGallonsToUSTsp(double impgal) {
		return litersToUSTsp(imperialGallonsToLiters(impgal));
	}

	public static double imperialGallonsToUSTbsp(double impgal) {
		return litersToUSTbsp(imperialGallonsToLiters(impgal));
	}

	public static double imperialGallonsToUSOunces(double impgal) {
		return litersToUSOunces(imperialGallonsToLiters(impgal));
	}

	public static double imperialGallonsToUSCups(double impgal) {
		return litersToUSCups(imperialGallonsToLiters(impgal));
	}

	public static double imperialGallonsToUSPints(double impgal) {
		return litersToUSPints(imperialGallonsToLiters(impgal));
	}

	public static double imperialGallonsToUSQuarts(double impgal) {
		return litersToUSQuarts(imperialGallonsToLiters(impgal));
	}

	public static double imperialGallonsToUSGallons(double impgal) {
		return litersToUSGallons(imperialGallonsToLiters(impgal));
	}

	public static double imperialGallonsToImperialTsp(double impgal) {
		return litersToImperialTsp(imperialGallonsToLiters(impgal));
	}

	public static double imperialGallonsToImperialTbsp(double impgal) {
		return litersToImperialTbsp(imperialGallonsToLiters(impgal));
	}

	public static double imperialGallonsToImperialOunces(double impgal) {
		return litersToImperialOunces(imperialGallonsToLiters(impgal));
	}

	public static double imperialGallonsToImperialPints(double impgal) {
		return litersToImperialPints(imperialGallonsToLiters(impgal));
	}

	public static double imperialGallonsToImperialQuarts(double impgal) {
		return litersToImperialQuarts(imperialGallonsToLiters(impgal));
	}

	/*
	 * Area conversions!!!
	 */
	public static double squareMetersToHectares(double sqm) {
		return sqm * squareMetersToHectaresFactor;
	}

	public static double squareMetersToSquareKilometers(double sqm) {
		return sqm * squareMetersToSquareKilometersFactor;
	}

	public static double squareMetersToSquareInches(double sqm) {
		return sqm * squareMetersToSquareInchesFactor;
	}

	public static double squareMetersToSquareFeet(double sqm) {
		return sqm * squareMetersToSquareFeetFactor;
	}

	public static double squareMetersToSquareYards(double sqm) {
		return sqm * squareMetersToSquareYardsFactor;
	}

	public static double squareMetersToAcres(double sqm) {
		return sqm * squareMetersToAcresFactor;
	}

	public static double squareMetersToSquareMiles(double sqm) {
		return sqm * squareMetersToSquareMilesFactor;
	}

	public static double hectaresToSquareMeters(double ht) {
		return ht * hectaresToSquareMetersFactor;
	}

	public static double hectaresToSquareKilometers(double ht) {
		return squareMetersToSquareKilometers(hectaresToSquareMeters(ht));
	}

	public static double hectaresToSquareInches(double ht) {
		return squareMetersToSquareInches(hectaresToSquareMeters(ht));
	}

	public static double hectaresToSquareFeet(double ht) {
		return squareMetersToSquareFeet(hectaresToSquareMeters(ht));
	}

	public static double hectaresToSquareYards(double ht) {
		return squareMetersToSquareYards(hectaresToSquareMeters(ht));
	}

	public static double hectaresToAcres(double ht) {
		return squareMetersToAcres(hectaresToSquareMeters(ht));
	}

	public static double hectaresToSquareMiles(double ht) {
		return squareMetersToSquareMiles(hectaresToSquareMeters(ht));
	}

	public static double squareKilometersToSquareMeters(double skm) {
		return skm * squareKilometersToSquareMetersFactor;
	}

	public static double squareKilometersToHectares(double skm) {
		return squareMetersToHectares(squareKilometersToSquareMeters(skm));
	}

	public static double squareKilometersToSquareInches(double skm) {
		return squareMetersToSquareInches(squareKilometersToSquareMeters(skm));
	}

	public static double squareKilometersToSquareFeet(double skm) {
		return squareMetersToSquareFeet(squareKilometersToSquareMeters(skm));
	}

	public static double squareKilometersToSquareYards(double skm) {
		return squareMetersToSquareYards(squareKilometersToSquareMeters(skm));
	}

	public static double squareKilometersToAcres(double skm) {
		return squareMetersToAcres(squareKilometersToSquareMeters(skm));
	}

	public static double squareKilometersToSquareMiles(double skm) {
		return squareMetersToSquareMiles(squareKilometersToSquareMeters(skm));
	}

	public static double squareInchesToSquareMeters(double sin) {
		return sin * squareInchesToSquareMetersFactor;
	}

	public static double squareInchesToHectares(double sin) {
		return squareMetersToHectares(squareInchesToSquareMeters(sin));
	}

	public static double squareInchesToSquareKilometers(double sin) {
		return squareMetersToSquareKilometers(squareInchesToSquareMeters(sin));
	}

	public static double squareInchesToSquareFeet(double sin) {
		return squareMetersToSquareFeet(squareInchesToSquareMeters(sin));
	}

	public static double squareInchesToSquareYards(double sin) {
		return squareMetersToSquareYards(squareInchesToSquareMeters(sin));
	}

	public static double squareInchesToAcres(double sin) {
		return squareMetersToAcres(squareInchesToSquareMeters(sin));
	}

	public static double squareInchesToSquareMiles(double sin) {
		return squareMetersToSquareMiles(squareInchesToSquareMeters(sin));
	}

	public static double squareFeetToSquareMeters(double sft) {
		return sft * squareFeetToSquareMetersFactor;
	}

	public static double squareFeetToHectares(double sft) {
		return squareMetersToHectares(squareFeetToSquareMeters(sft));
	}

	public static double squareFeetToSquareKilometers(double sft) {
		return squareMetersToSquareKilometers(squareFeetToSquareMeters(sft));
	}

	public static double squareFeetToSquareInches(double sft) {
		return squareMetersToSquareInches(squareFeetToSquareMeters(sft));
	}

	public static double squareFeetToSquareYards(double sft) {
		return squareMetersToSquareYards(squareFeetToSquareMeters(sft));
	}

	public static double squareFeetToAcres(double sft) {
		return squareMetersToAcres(squareFeetToSquareMeters(sft));
	}

	public static double squareFeetToSquareMiles(double sft) {
		return squareMetersToSquareMiles(squareFeetToSquareMeters(sft));
	}

	public static double squareYardsToSquareMeters(double sft) {
		return sft * squareYardsToSquareMetersFactor;
	}

	public static double squareYardsToHectares(double sft) {
		return squareMetersToHectares(squareYardsToSquareMeters(sft));
	}

	public static double squareYardsToSquareKilometers(double sft) {
		return squareMetersToSquareKilometers(squareYardsToSquareMeters(sft));
	}

	public static double squareYardsToSquareInches(double sft) {
		return squareMetersToSquareInches(squareYardsToSquareMeters(sft));
	}

	public static double squareYardsToSquareFeet(double sft) {
		return squareMetersToSquareFeet(squareYardsToSquareMeters(sft));
	}

	public static double squareYardsToAcres(double sft) {
		return squareMetersToAcres(squareYardsToSquareMeters(sft));
	}

	public static double squareYardsToSquareMiles(double sft) {
		return squareMetersToSquareMiles(squareYardsToSquareMeters(sft));
	}

	public static double acresToSquareMeters(double sft) {
		return sft * acresToSquareMetersFactor;
	}

	public static double acresToHectares(double sft) {
		return squareMetersToHectares(acresToSquareMeters(sft));
	}

	public static double acresToSquareKilometers(double sft) {
		return squareMetersToSquareKilometers(acresToSquareMeters(sft));
	}

	public static double acresToSquareInches(double sft) {
		return squareMetersToSquareInches(acresToSquareMeters(sft));
	}

	public static double acresToSquareFeet(double sft) {
		return squareMetersToSquareFeet(acresToSquareMeters(sft));
	}

	public static double acresToSquareYards(double sft) {
		return squareMetersToSquareYards(acresToSquareMeters(sft));
	}

	public static double acresToSquareMiles(double sft) {
		return squareMetersToSquareMiles(acresToSquareMeters(sft));
	}

	public static double squareMilesToSquareMeters(double sft) {
		return sft * squareMilesToSquareMetersFactor;
	}

	public static double squareMilesToHectares(double sft) {
		return squareMetersToHectares(squareMilesToSquareMeters(sft));
	}

	public static double squareMilesToSquareKilometers(double sft) {
		return squareMetersToSquareKilometers(squareMilesToSquareMeters(sft));
	}

	public static double squareMilesToSquareInches(double sft) {
		return squareMetersToSquareInches(squareMilesToSquareMeters(sft));
	}

	public static double squareMilesToSquareFeet(double sft) {
		return squareMetersToSquareFeet(squareMilesToSquareMeters(sft));
	}

	public static double squareMilesToSquareYards(double sft) {
		return squareMetersToSquareYards(squareMilesToSquareMeters(sft));
	}

	public static double squareMilesToAcres(double sft) {
		return squareMetersToAcres(squareMilesToSquareMeters(sft));
	}

	public static double litersPer100KmToKmPerLiters(double l100km) {
		return l100km * litersPer100KmToKmPerLitersFactor;
	}

	public static double litersPer100KmToMilesPerUSGallons(double l100km) {
		return l100km * litersPer100KmToMilesPerUSGallonsFactor;
	}

	public static double litersPer100KmToMilesPerImperialGallons(double l100km) {
		return l100km * litersPer100KmToMilesPerImperialGallonsFactor;
	}

	public static double kmPerLitersToLitersPer100Km(double kmpl) {
		return kmpl * litersPer100KmToKmPerLitersFactor;
	}

	public static double kmPerLitersToMilesPerUSGallons(double kmpl) {
		return kmpl * (litersPer100KmToMilesPerUSGallonsFactor / 100);
	}

	public static double kmPerLitersToMilesPerImperialGallons(double kmpl) {
		return kmpl * (litersPer100KmToMilesPerImperialGallonsFactor / 100);
	}

	public static double milesPerUSGallonsToLitersPer100Km(double kmpl) {
		return kmpl * litersPer100KmToMilesPerUSGallonsFactor;
	}

	public static double milesPerUSGallonsToKmPerLiters(double kmpl) {
		return kmpl * milesPerUSGallonsToKmPerLitersFactor;
	}

	public static double milesPerUSGallonsToMilesPerImperialGallons(double kmpl) {
		return kmpl * milesPerUSGallonsToMilesPerImperialGallons;
	}

	public static double milesPerImperialGallonsToLitersPer100Km(double mipl) {
		return mipl * litersPer100KmToMilesPerImperialGallonsFactor;
	}

	public static double milesPerImperialGallonsToKmPerLiters(double mipl) {
		return mipl * milesPerImperialGallonsToKmPerLitersFactor;
	}

	public static double milesPerImperialGallonsToMilesPerUSGallons(double mipl) {
		return mipl * milesPerImperialGallonsToMilesPerUSGallons;
	}

	/*
	 * Time conversions!!!
	 */
	public static double nanosecondsToMicroseconds(double ns) {
		return ns / secondsToMillisecondsFactor;
	}

	public static double nanosecondsToMilliseconds(double ns) {
		return ns / secondsToMicrosecondsFactor;
	}

	public static double nanosecondsToSeconds(double ns) {
		return ns / secondsToNanosecondsFactor;
	}

	public static double nanosecondsToMinutes(double ns) {
		return secondsToMinutes(nanosecondsToSeconds(ns));
	}

	public static double nanosecondsToHours(double ns) {
		return secondsToHours(nanosecondsToSeconds(ns));
	}

	public static double nanosecondsToDays(double ns) {
		return secondsToDays(nanosecondsToSeconds(ns));
	}

	public static double nanosecondsToWeeks(double ns) {
		return daysToWeeks(secondsToDays(nanosecondsToSeconds(ns)));
	}

	public static double nanosecondsToMonths(double ns) {
		return daysToMonths(secondsToDays(nanosecondsToSeconds(ns)));
	}

	public static double nanosecondsToYears(double ns) {
		return daysToYears(secondsToDays(nanosecondsToSeconds(ns)));
	}

	public static double nanosecondsToDecades(double ns) {
		return daysToDecades(secondsToDays(nanosecondsToSeconds(ns)));
	}

	public static double nanosecondsToCenturies(double ns) {
		return daysToCenturies(secondsToDays(nanosecondsToSeconds(ns)));
	}

	public static double microsecondsToNanoseconds(double mis) {
		return secondsToNanoseconds(microsecondsToSeconds(mis));
	}

	public static double microsecondsToMilliseconds(double mis) {
		return secondsToMilliseconds(microsecondsToSeconds(mis));
	}

	public static double microsecondsToSeconds(double mis) {
		return mis * (1 / secondsToMicrosecondsFactor);
	}

	public static double microsecondsToMinutes(double mis) {
		return secondsToMinutes(microsecondsToSeconds(mis));
	}

	public static double microsecondsToHours(double mis) {
		return secondsToHours(microsecondsToSeconds(mis));
	}

	public static double microsecondsToDays(double mis) {
		return secondsToDays(microsecondsToSeconds(mis));
	}

	public static double microsecondsToWeeks(double mis) {
		return daysToWeeks(secondsToDays(microsecondsToSeconds(mis)));
	}

	public static double microsecondsToMonths(double mis) {
		return daysToMonths(secondsToDays(microsecondsToSeconds(mis)));
	}

	public static double microsecondsToYears(double mis) {
		return daysToYears(secondsToDays(microsecondsToSeconds(mis)));
	}

	public static double microsecondsToDecades(double mis) {
		return daysToDecades(secondsToDays(microsecondsToSeconds(mis)));
	}

	public static double microsecondsToCenturies(double mis) {
		return daysToCenturies(secondsToDays(microsecondsToSeconds(mis)));
	}

	public static double millisecondsToNanoseconds(double ms) {
		return secondsToNanoseconds(millisecondsToSeconds(ms));
	}

	public static double millisecondsToMicroseconds(double ms) {
		return secondsToMicroseconds(millisecondsToSeconds(ms));
	}

	public static double millisecondsToSeconds(double ms) {
		return ms * (1 / secondsToMillisecondsFactor);
	}

	public static double millisecondsToMinutes(double ms) {
		return secondsToMinutes(millisecondsToSeconds(ms));
	}

	public static double millisecondsToHours(double ms) {
		return secondsToHours(millisecondsToSeconds(ms));
	}

	public static double millisecondsToDays(double ms) {
		return secondsToDays(millisecondsToSeconds(ms));
	}

	public static double millisecondsToWeeks(double ms) {
		return daysToWeeks(secondsToDays(millisecondsToSeconds(ms)));
	}

	public static double millisecondsToMonths(double ms) {
		return daysToMonths(secondsToDays(millisecondsToSeconds(ms)));
	}

	public static double millisecondsToYears(double ms) {
		return daysToYears(secondsToDays(millisecondsToSeconds(ms)));
	}

	public static double millisecondsToDecades(double ms) {
		return daysToDecades(secondsToDays(millisecondsToSeconds(ms)));
	}

	public static double millisecondsToCenturies(double ms) {
		return daysToCenturies(secondsToDays(millisecondsToSeconds(ms)));
	}

	public static double secondsToNanoseconds(double s) {
		return s * secondsToNanosecondsFactor;
	}

	public static double secondsToMicroseconds(double s) {
		return s * secondsToMicrosecondsFactor;
	}

	public static double secondsToMilliseconds(double s) {
		return s * secondsToMillisecondsFactor;
	}

	public static double secondsToMinutes(double s) {
		return s * secondsToMinutesFactor;
	}

	public static double secondsToHours(double s) {
		return s * secondsToHoursFactor;
	}

	public static double secondsToDays(double s) {
		return s * secondsToDaysFactor;
	}

	public static double secondsToWeeks(double s) {
		return daysToWeeks(secondsToDays(s));
	}

	public static double secondsToMonths(double s) {
		return daysToMonths(secondsToDays(s));
	}

	public static double secondsToYears(double s) {
		return daysToYears(secondsToDays(s));
	}

	public static double secondsToDecades(double s) {
		return daysToDecades(secondsToDays(s));
	}

	public static double secondsToCenturies(double s) {
		return daysToCenturies(secondsToDays(s));
	}

	public static double minutesToNanoseconds(double min) {
		return secondsToNanoseconds(minutesToSeconds(min));
	}

	public static double minutesToMicroseconds(double min) {
		return secondsToMicroseconds(minutesToSeconds(min));
	}

	public static double minutesToMilliseconds(double min) {
		return secondsToMilliseconds(minutesToSeconds(min));
	}

	public static double minutesToSeconds(double min) {
		return min * timeFactor;
	}

	public static double minutesToHours(double min) {
		return min / timeFactor;
	}

	public static double minutesToDays(double min) {
		return secondsToDays(minutesToSeconds(min));
	}

	public static double minutesToWeeks(double min) {
		return daysToWeeks(minutesToDays(min));
	}

	public static double minutesToMonths(double min) {
		return daysToMonths(minutesToDays(min));
	}

	public static double minutesToYears(double min) {
		return daysToYears(minutesToDays(min));
	}

	public static double minutesToDecades(double min) {
		return daysToDecades(minutesToDays(min));
	}

	public static double minutesToCenturies(double min) {
		return daysToCenturies(minutesToDays(min));
	}

	public static double hoursToNanoseconds(double h) {
		return secondsToNanoseconds(hoursToSeconds(h));
	}

	public static double hoursToMicroseconds(double h) {
		return secondsToMicroseconds(hoursToSeconds(h));
	}

	public static double hoursToMilliseconds(double h) {
		return secondsToMilliseconds(hoursToSeconds(h));
	}

	public static double hoursToSeconds(double h) {
		return h * secondHourFactor;
	}

	public static double hoursToMinutes(double h) {
		return h * timeFactor;
	}

	public static double hoursToDays(double h) {
		return h / dayhourFactor;
	}

	public static double hoursToWeeks(double h) {
		return daysToWeeks(hoursToDays(h));
	}

	public static double hoursToMonths(double h) {
		return daysToMonths(hoursToDays(h));
	}

	public static double hoursToYears(double h) {
		return daysToYears(hoursToDays(h));
	}

	public static double hoursToDecades(double h) {
		return daysToDecades(hoursToDays(h));
	}

	public static double hoursToCenturies(double h) {
		return daysToCenturies(hoursToDays(h));
	}

	public static double daysToNanoseconds(double d) {
		return secondsToNanoseconds(daysToSeconds(d));
	}

	public static double daysToMicroseconds(double d) {
		return secondsToMicroseconds(daysToSeconds(d));
	}

	public static double daysToMilliseconds(double d) {
		return secondsToMilliseconds(daysToSeconds(d));
	}

	public static double daysToSeconds(double d) {
		return d / secondsToDaysFactor;
	}

	public static double daysToMinutes(double d) {
		return secondsToMinutes(daysToSeconds(d));
	}

	public static double daysToHours(double d) {
		return d * dayhourFactor;
	}

	public static double daysToWeeks(double d) {
		return d * daysToWeekFactor;
	}

	public static double daysToMonths(double d) {
		return d * daysToMonthsFactor;
	}

	public static double daysToYears(double d) {
		return d * daysToYearsFactor;
	}

	public static double daysToDecades(double d) {
		return d * daysToDecadesFactor;
	}

	public static double daysToCenturies(double d) {
		return d * daysToCenturiesFactor;
	}

	public static double weeksToNanoseconds(double w) {
		return daysToNanoseconds(weeksToDays(w));
	}

	public static double weeksToMicroseconds(double w) {
		return daysToMicroseconds(weeksToDays(w));
	}

	public static double weeksToMilliseconds(double w) {
		return daysToMilliseconds(weeksToDays(w));
	}

	public static double weeksToSeconds(double w) {
		return daysToSeconds(weeksToDays(w));
	}

	public static double weeksToMinutes(double w) {
		return daysToMinutes(weeksToDays(w));
	}

	public static double weeksToHours(double w) {
		return daysToHours(weeksToDays(w));
	}

	public static double weeksToDays(double w) {
		return w * daysInWeek;
	}

	public static double weeksToMonths(double w) {
		return daysToMonths(weeksToDays(w));
	}

	public static double weeksToYears(double w) {
		return daysToYears(weeksToDays(w));
	}

	public static double weeksToDecades(double w) {
		return daysToDecades(weeksToDays(w));
	}

	public static double weeksToCenturies(double w) {
		return daysToCenturies(weeksToDays(w));
	}

	public static double monthsToNanoseconds(double m) {
		return daysToNanoseconds(monthsToDays(m));
	}

	public static double monthsToMicroseconds(double m) {
		return daysToMicroseconds(monthsToDays(m));
	}

	public static double monthsToMilliseconds(double m) {
		return daysToMilliseconds(monthsToDays(m));
	}

	public static double monthsToSeconds(double m) {
		return daysToSeconds(monthsToDays(m));
	}

	public static double monthsToMinutes(double m) {
		return daysToMinutes(monthsToDays(m));
	}

	public static double monthsToHours(double m) {
		return daysToHours(monthsToDays(m));
	}

	public static double monthsToDays(double m) {
		return m * daysInMonths;
	}

	public static double monthsToWeeks(double m) {
		return daysToWeeks(monthsToDays(m));
	}

	public static double monthsToYears(double m) {
		return daysToYears(monthsToDays(m));
	}

	public static double monthsToDecades(double m) {
		return daysToDecades(monthsToDays(m));
	}

	public static double monthsToCenturies(double m) {
		return daysToCenturies(monthsToDays(m));
	}

	public static double yearsToNanoseconds(double y) {
		return daysToNanoseconds(yearsToDays(y));
	}

	public static double yearsToMicroseconds(double y) {
		return daysToMicroseconds(yearsToDays(y));
	}

	public static double yearsToMilliseconds(double y) {
		return daysToMilliseconds(yearsToDays(y));
	}

	public static double yearsToSeconds(double y) {
		return daysToSeconds(yearsToDays(y));
	}

	public static double yearsToMinutes(double y) {
		return daysToMinutes(yearsToDays(y));
	}

	public static double yearsToHours(double y) {
		return daysToHours(yearsToDays(y));
	}

	public static double yearsToDays(double y) {
		return y * daysInYears;
	}

	public static double yearsToWeeks(double y) {
		return daysToWeeks(yearsToDays(y));
	}

	public static double yearsToMonths(double y) {
		return daysToMonths(yearsToDays(y));
	}

	public static double yearsToDecades(double y) {
		return daysToDecades(yearsToDays(y));
	}

	public static double yearsToCenturies(double y) {
		return daysToCenturies(yearsToDays(y));
	}

	public static double decadesToNanoseconds(double d) {
		return daysToNanoseconds(decadesToDays(d));
	}

	public static double decadesToMicroseconds(double d) {
		return daysToMicroseconds(decadesToDays(d));
	}

	public static double decadesToMilliseconds(double d) {
		return daysToMilliseconds(decadesToDays(d));
	}

	public static double decadesToSeconds(double d) {
		return daysToSeconds(decadesToDays(d));
	}

	public static double decadesToMinutes(double d) {
		return daysToMinutes(decadesToDays(d));
	}

	public static double decadesToHours(double d) {
		return daysToHours(decadesToDays(d));
	}

	public static double decadesToDays(double d) {
		return d * daysInDecades;
	}

	public static double decadesToWeeks(double d) {
		return daysToWeeks(decadesToDays(d));
	}

	public static double decadesToMonths(double d) {
		return daysToMonths(decadesToDays(d));
	}

	public static double decadesToYears(double d) {
		return daysToYears(decadesToDays(d));
	}

	public static double decadesToCenturies(double d) {
		return daysToCenturies(decadesToDays(d));
	}

	public static double centuriesToNanoseconds(double c) {
		return daysToNanoseconds(centuriesToDays(c));
	}

	public static double centuriesToMicroseconds(double c) {
		return daysToMicroseconds(centuriesToDays(c));
	}

	public static double centuriesToMilliseconds(double c) {
		return daysToMilliseconds(centuriesToDays(c));
	}

	public static double centuriesToSeconds(double c) {
		return daysToSeconds(centuriesToDays(c));
	}

	public static double centuriesToMinutes(double c) {
		return daysToMinutes(centuriesToDays(c));
	}

	public static double centuriesToHours(double c) {
		return daysToHours(centuriesToDays(c));
	}

	public static double centuriesToDays(double c) {
		return c * daysInCenturies;
	}

	public static double centuriesToWeeks(double c) {
		return daysToWeeks(centuriesToDays(c));
	}

	public static double centuriesToMonths(double c) {
		return daysToMonths(centuriesToDays(c));
	}

	public static double centuriesToYears(double c) {
		return daysToYears(centuriesToDays(c));
	}

	public static double centuriesToDecades(double c) {
		return daysToDecades(centuriesToDays(c));
	}

	/*
	 * Digital Storage conversions!!!
	 */
	public static BigDecimal bitToByte(BigDecimal dg) {
		BigDecimal dgs = dg.divide(new BigDecimal(bitsInBytes));
		return dgs;
	}

	public static BigDecimal bitToKilobit(BigDecimal dg) {
		BigDecimal dgs = dg.divide(new BigDecimal(kiloFactor));
		return dgs;
	}

	public static BigDecimal bitToKilobyte(BigDecimal dg) {
		BigDecimal dgs = dg.divide(new BigDecimal(bitsInBytes).multiply(new BigDecimal(kiloFactor)));
		return dgs;
	}

	public static BigDecimal bitToKibibit(BigDecimal dg) {
		BigDecimal dgs = dg.divide(new BigDecimal(bitsInKibibits));
		return dgs;
	}

	public static BigDecimal bitToKibibyte(BigDecimal dg) {
		BigDecimal dgs = dg.divide(new BigDecimal(bitsInBytes).multiply(new BigDecimal(bitsInKibibits)));
		return dgs;
	}

	public static BigDecimal bitToMegabit(BigDecimal dg) {
		BigDecimal dgs = dg.divide(new BigDecimal(kiloFactor).multiply(new BigDecimal(kiloFactor)));
		return dgs;
	}

	public static BigDecimal bitToMegabyte(BigDecimal dg) {
		BigDecimal dgs = dg.divide(new BigDecimal(kiloFactor).multiply(new BigDecimal(kiloFactor)).multiply(new BigDecimal(bitsInBytes)));
		return dgs;
	}

	public static BigDecimal bitToMebibit(BigDecimal dg) {
		BigDecimal dgs = dg.divide(new BigDecimal(bitsInKibibits).multiply(new BigDecimal(bitsInKibibits)));
		return dgs;
	}

	public static BigDecimal bitToMebibyte(BigDecimal dg) {
		BigDecimal dgs = dg.divide(new BigDecimal(bitsInKibibits).multiply(new BigDecimal(bitsInKibibits)).multiply(new BigDecimal(bitsInBytes)));
		return dgs;
	}

	public static BigDecimal bitToGigabit(BigDecimal dg) {
		BigDecimal dgs = dg.divide(new BigDecimal(kiloFactor).multiply(new BigDecimal(kiloFactor)).multiply(new BigDecimal(kiloFactor)));
		return dgs;
	}

	public static BigDecimal bitToGigabyte(BigDecimal dg) {
		BigDecimal dgs = dg.divide(new BigDecimal(kiloFactor).multiply(new BigDecimal(kiloFactor)).multiply(new BigDecimal(kiloFactor))
				.multiply(new BigDecimal(bitsInBytes)));
		return dgs;
	}

	public static BigDecimal bitToGibibit(BigDecimal dg) {
		BigDecimal dgs = dg.divide(new BigDecimal(bitsInKibibits).multiply(new BigDecimal(bitsInKibibits)).multiply(new BigDecimal(bitsInKibibits)));
		return dgs;
	}

	public static BigDecimal bitToGibibyte(BigDecimal dg) {
		BigDecimal dgs = dg.divide(new BigDecimal(bitsInKibibits).multiply(new BigDecimal(bitsInKibibits)).multiply(new BigDecimal(bitsInKibibits))
				.multiply(new BigDecimal(bitsInBytes)));
		return dgs;
	}

	public static BigDecimal bitToTerabit(BigDecimal dg) {
		BigDecimal dgs = dg.divide(new BigDecimal(kiloFactor).multiply(new BigDecimal(kiloFactor)).multiply(new BigDecimal(kiloFactor))
				.multiply(new BigDecimal(kiloFactor)));
		return dgs;
	}

	public static BigDecimal bitToTerabyte(BigDecimal dg) {
		BigDecimal dgs = dg.divide(new BigDecimal(kiloFactor).multiply(new BigDecimal(kiloFactor)).multiply(new BigDecimal(kiloFactor))
				.multiply(new BigDecimal(kiloFactor)).multiply(new BigDecimal(bitsInBytes)));
		return dgs;
	}

	public static BigDecimal bitToTebibit(BigDecimal dg) {
		BigDecimal dgs = dg.divide(new BigDecimal(bitsInKibibits).multiply(new BigDecimal(bitsInKibibits)).multiply(new BigDecimal(bitsInKibibits))
				.multiply(new BigDecimal(bitsInKibibits)));
		return dgs;
	}

	public static BigDecimal bitToTebibyte(BigDecimal dg) {
		BigDecimal dgs = dg.divide(new BigDecimal(bitsInKibibits).multiply(new BigDecimal(bitsInKibibits)).multiply(new BigDecimal(bitsInKibibits))
				.multiply(new BigDecimal(bitsInKibibits)).multiply(new BigDecimal(bitsInBytes)));
		return dgs;
	}

	public static BigDecimal bitToPetabit(BigDecimal dg) {
		BigDecimal dgs = dg.divide(new BigDecimal(kiloFactor).multiply(new BigDecimal(kiloFactor)).multiply(new BigDecimal(kiloFactor))
				.multiply(new BigDecimal(kiloFactor)).multiply(new BigDecimal(kiloFactor)));
		return dgs;
	}

	public static BigDecimal bitToPetabyte(BigDecimal dg) {
		BigDecimal dgs = dg.divide(new BigDecimal(kiloFactor).multiply(new BigDecimal(kiloFactor)).multiply(new BigDecimal(kiloFactor))
				.multiply(new BigDecimal(kiloFactor)).multiply(new BigDecimal(kiloFactor)).multiply(new BigDecimal(bitsInBytes)));
		return dgs;
	}

	public static BigDecimal bitToPebibit(BigDecimal dg) {
		BigDecimal dgs = dg.divide(new BigDecimal(bitsInKibibits).multiply(new BigDecimal(bitsInKibibits)).multiply(new BigDecimal(bitsInKibibits))
				.multiply(new BigDecimal(bitsInKibibits)).multiply(new BigDecimal(bitsInKibibits)));
		return dgs;
	}

	public static BigDecimal bitToPebibyte(BigDecimal dg) {
		BigDecimal dgs = dg.divide(new BigDecimal(bitsInKibibits).multiply(new BigDecimal(bitsInKibibits)).multiply(new BigDecimal(bitsInKibibits))
				.multiply(new BigDecimal(bitsInKibibits)).multiply(new BigDecimal(bitsInKibibits)).multiply(new BigDecimal(bitsInBytes)));
		return dgs;
	}

	public static BigDecimal byteToBit(BigDecimal dg) {
		BigDecimal dgs = dg.multiply(new BigDecimal(bitsInBytes));
		return dgs;
	}

	public static BigDecimal byteToKilobit(BigDecimal dg) {
		return bitToKilobit(byteToBit(dg));
	}

	public static BigDecimal byteToKilobyte(BigDecimal dg) {
		return bitToKilobyte(byteToBit(dg));
	}

	public static BigDecimal byteToKibibit(BigDecimal dg) {
		return bitToKibibit(byteToBit(dg));
	}

	public static BigDecimal byteToKibibyte(BigDecimal dg) {
		return bitToKibibyte(byteToBit(dg));
	}

	public static BigDecimal byteToMegabit(BigDecimal dg) {
		return bitToMegabit(byteToBit(dg));
	}

	public static BigDecimal byteToMegabyte(BigDecimal dg) {
		return bitToMegabyte(byteToBit(dg));
	}

	public static BigDecimal byteToMebibit(BigDecimal dg) {
		return bitToMebibit(byteToBit(dg));
	}

	public static BigDecimal byteToMebibyte(BigDecimal dg) {
		return bitToMebibyte(byteToBit(dg));
	}

	public static BigDecimal byteToGigabit(BigDecimal dg) {
		return bitToGigabit(byteToBit(dg));
	}

	public static BigDecimal byteToGigabyte(BigDecimal dg) {
		return bitToGigabyte(byteToBit(dg));
	}

	public static BigDecimal byteToGibibit(BigDecimal dg) {
		return bitToGibibit(byteToBit(dg));
	}

	public static BigDecimal byteToGibibyte(BigDecimal dg) {
		return bitToGibibyte(byteToBit(dg));
	}

	public static BigDecimal byteToTerabit(BigDecimal dg) {
		return bitToTerabit(byteToBit(dg));
	}

	public static BigDecimal byteToTerabyte(BigDecimal dg) {
		return bitToTerabyte(byteToBit(dg));
	}

	public static BigDecimal byteToTebibit(BigDecimal dg) {
		return bitToTebibit(byteToBit(dg));
	}

	public static BigDecimal byteToTebibyte(BigDecimal dg) {
		return bitToTebibyte(byteToBit(dg));
	}

	public static BigDecimal byteToPetabit(BigDecimal dg) {
		return bitToPetabit(byteToBit(dg));
	}

	public static BigDecimal byteToPetabyte(BigDecimal dg) {
		return bitToPetabyte(byteToBit(dg));
	}

	public static BigDecimal byteToPebibit(BigDecimal dg) {
		return bitToPebibit(byteToBit(dg));
	}

	public static BigDecimal byteToPebibyte(BigDecimal dg) {
		return bitToPebibyte(byteToBit(dg));
	}

	public static BigDecimal kilobitToBit(BigDecimal dg) {
		BigDecimal dgs = dg.multiply(new BigDecimal(kiloFactor));
		return dgs;
	}

	public static BigDecimal kilobitToByte(BigDecimal dg) {
		return bitToByte(kilobitToBit(dg));
	}

	public static BigDecimal kilobitToKilobyte(BigDecimal dg) {
		return bitToKilobyte(kilobitToBit(dg));
	}

	public static BigDecimal kilobitToKibibit(BigDecimal dg) {
		return bitToKibibit(kilobitToBit(dg));
	}

	public static BigDecimal kilobitToKibibyte(BigDecimal dg) {
		return bitToKibibyte(kilobitToBit(dg));
	}

	public static BigDecimal kilobitToMegabit(BigDecimal dg) {
		return bitToMegabit(kilobitToBit(dg));
	}

	public static BigDecimal kilobitToMegabyte(BigDecimal dg) {
		return bitToMegabyte(kilobitToBit(dg));
	}

	public static BigDecimal kilobitToMebibit(BigDecimal dg) {
		return bitToMebibit(kilobitToBit(dg));
	}

	public static BigDecimal kilobitToMebibyte(BigDecimal dg) {
		return bitToMebibyte(kilobitToBit(dg));
	}

	public static BigDecimal kilobitToGigabit(BigDecimal dg) {
		return bitToGigabit(kilobitToBit(dg));
	}

	public static BigDecimal kilobitToGigabyte(BigDecimal dg) {
		return bitToGigabyte(kilobitToBit(dg));
	}

	public static BigDecimal kilobitToGibibit(BigDecimal dg) {
		return bitToGibibit(kilobitToBit(dg));
	}

	public static BigDecimal kilobitToGibibyte(BigDecimal dg) {
		return bitToGibibyte(kilobitToBit(dg));
	}

	public static BigDecimal kilobitToTerabit(BigDecimal dg) {
		return bitToTerabit(kilobitToBit(dg));
	}

	public static BigDecimal kilobitToTerabyte(BigDecimal dg) {
		return bitToTerabyte(kilobitToBit(dg));
	}

	public static BigDecimal kilobitToTebibit(BigDecimal dg) {
		return bitToTebibit(kilobitToBit(dg));
	}

	public static BigDecimal kilobitToTebibyte(BigDecimal dg) {
		return bitToTebibyte(kilobitToBit(dg));
	}

	public static BigDecimal kilobitToPetabit(BigDecimal dg) {
		return bitToPetabit(kilobitToBit(dg));
	}

	public static BigDecimal kilobitToPetabyte(BigDecimal dg) {
		return bitToPetabyte(kilobitToBit(dg));
	}

	public static BigDecimal kilobitToPebibit(BigDecimal dg) {
		return bitToPebibit(kilobitToBit(dg));
	}

	public static BigDecimal kilobitToPebibyte(BigDecimal dg) {
		return bitToPebibyte(kilobitToBit(dg));
	}

	public static BigDecimal kilobyteToBit(BigDecimal dg) {

		BigDecimal dgs = dg.multiply(new BigDecimal(kiloFactor).multiply(new BigDecimal(bitsInBytes)));
		return dgs;
	}

	public static BigDecimal kilobyteToByte(BigDecimal dg) {
		return bitToByte(kilobyteToBit(dg));
	}

	public static BigDecimal kilobyteToKilobit(BigDecimal dg) {
		return bitToKilobit(kilobyteToBit(dg));
	}

	public static BigDecimal kilobyteToKibibit(BigDecimal dg) {
		return bitToKibibit(kilobyteToBit(dg));
	}

	public static BigDecimal kilobyteToKibibyte(BigDecimal dg) {
		return bitToKibibyte(kilobyteToBit(dg));
	}

	public static BigDecimal kilobyteToMegabit(BigDecimal dg) {
		return bitToMegabit(kilobyteToBit(dg));
	}

	public static BigDecimal kilobyteToMegabyte(BigDecimal dg) {
		return bitToMegabyte(kilobyteToBit(dg));
	}

	public static BigDecimal kilobyteToMebibit(BigDecimal dg) {
		return bitToMebibit(kilobyteToBit(dg));
	}

	public static BigDecimal kilobyteToMebibyte(BigDecimal dg) {
		return bitToMebibyte(kilobyteToBit(dg));
	}

	public static BigDecimal kilobyteToGigabit(BigDecimal dg) {
		return bitToGigabit(kilobyteToBit(dg));
	}

	public static BigDecimal kilobyteToGigabyte(BigDecimal dg) {
		return bitToGigabyte(kilobyteToBit(dg));
	}

	public static BigDecimal kilobyteToGibibit(BigDecimal dg) {
		return bitToGibibit(kilobyteToBit(dg));
	}

	public static BigDecimal kilobyteToGibibyte(BigDecimal dg) {
		return bitToGibibyte(kilobyteToBit(dg));
	}

	public static BigDecimal kilobyteToTerabit(BigDecimal dg) {
		return bitToTerabit(kilobyteToBit(dg));
	}

	public static BigDecimal kilobyteToTerabyte(BigDecimal dg) {
		return bitToTerabyte(kilobyteToBit(dg));
	}

	public static BigDecimal kilobyteToTebibit(BigDecimal dg) {
		return bitToTebibit(kilobyteToBit(dg));
	}

	public static BigDecimal kilobyteToTebibyte(BigDecimal dg) {
		return bitToTebibyte(kilobyteToBit(dg));
	}

	public static BigDecimal kilobyteToPetabit(BigDecimal dg) {
		return bitToPetabit(kilobyteToBit(dg));
	}

	public static BigDecimal kilobyteToPetabyte(BigDecimal dg) {
		return bitToPetabyte(kilobyteToBit(dg));
	}

	public static BigDecimal kilobyteToPebibit(BigDecimal dg) {
		return bitToPebibit(kilobyteToBit(dg));
	}

	public static BigDecimal kilobyteToPebibyte(BigDecimal dg) {
		return bitToPebibyte(kilobyteToBit(dg));
	}

	public static BigDecimal kibibitToBit(BigDecimal dg) {
		BigDecimal dgs = dg.multiply(new BigDecimal(bitsInKibibits));
		return dgs;
	}

	public static BigDecimal kibibitToByte(BigDecimal dg) {
		return bitToByte(kibibitToBit(dg));
	}

	public static BigDecimal kibibitToKilobit(BigDecimal dg) {
		return bitToKilobit(kibibitToBit(dg));
	}

	public static BigDecimal kibibitToKilobyte(BigDecimal dg) {
		return bitToKilobyte(kibibitToBit(dg));
	}

	public static BigDecimal kibibitToKibibyte(BigDecimal dg) {
		return bitToKibibyte(kibibitToBit(dg));
	}

	public static BigDecimal kibibitToMegabit(BigDecimal dg) {
		return bitToMegabit(kibibitToBit(dg));
	}

	public static BigDecimal kibibitToMegabyte(BigDecimal dg) {
		return bitToMegabyte(kibibitToBit(dg));
	}

	public static BigDecimal kibibitToMebibit(BigDecimal dg) {
		return bitToMebibit(kibibitToBit(dg));
	}

	public static BigDecimal kibibitToMebibyte(BigDecimal dg) {
		return bitToMebibyte(kibibitToBit(dg));
	}

	public static BigDecimal kibibitToGigabit(BigDecimal dg) {
		return bitToGigabit(kibibitToBit(dg));
	}

	public static BigDecimal kibibitToGigabyte(BigDecimal dg) {
		return bitToGigabyte(kibibitToBit(dg));
	}

	public static BigDecimal kibibitToGibibit(BigDecimal dg) {
		return bitToGibibit(kibibitToBit(dg));
	}

	public static BigDecimal kibibitToGibibyte(BigDecimal dg) {
		return bitToGibibyte(kibibitToBit(dg));
	}

	public static BigDecimal kibibitToTerabit(BigDecimal dg) {
		return bitToTerabit(kibibitToBit(dg));
	}

	public static BigDecimal kibibitToTerabyte(BigDecimal dg) {
		return bitToTerabyte(kibibitToBit(dg));
	}

	public static BigDecimal kibibitToTebibit(BigDecimal dg) {
		return bitToTebibit(kibibitToBit(dg));
	}

	public static BigDecimal kibibitToTebibyte(BigDecimal dg) {
		return bitToTebibyte(kibibitToBit(dg));
	}

	public static BigDecimal kibibitToPetabit(BigDecimal dg) {
		return bitToPetabit(kibibitToBit(dg));
	}

	public static BigDecimal kibibitToPetabyte(BigDecimal dg) {
		return bitToPetabyte(kibibitToBit(dg));
	}

	public static BigDecimal kibibitToPebibit(BigDecimal dg) {
		return bitToPebibit(kibibitToBit(dg));
	}

	public static BigDecimal kibibitToPebibyte(BigDecimal dg) {
		return bitToPebibyte(kibibitToBit(dg));
	}

	public static BigDecimal kibibyteToBit(BigDecimal dg) {
		BigDecimal dgs = dg.multiply(new BigDecimal(bitsInKibibits).multiply(new BigDecimal(bitsInBytes)));
		return dgs;
	}

	public static BigDecimal kibibyteToByte(BigDecimal dg) {
		return bitToByte(kibibyteToBit(dg));
	}

	public static BigDecimal kibibyteToKilobit(BigDecimal dg) {
		return bitToKilobit(kibibyteToBit(dg));
	}

	public static BigDecimal kibibyteToKilobyte(BigDecimal dg) {
		return bitToKilobyte(kibibyteToBit(dg));
	}

	public static BigDecimal kibibyteToKibibit(BigDecimal dg) {
		return bitToKibibit(kibibyteToBit(dg));
	}

	public static BigDecimal kibibyteToMegabit(BigDecimal dg) {
		return bitToMegabit(kibibyteToBit(dg));
	}

	public static BigDecimal kibibyteToMegabyte(BigDecimal dg) {
		return bitToMegabyte(kibibyteToBit(dg));
	}

	public static BigDecimal kibibyteToMebibit(BigDecimal dg) {
		return bitToMebibit(kibibyteToBit(dg));
	}

	public static BigDecimal kibibyteToMebibyte(BigDecimal dg) {
		return bitToMebibyte(kibibyteToBit(dg));
	}

	public static BigDecimal kibibyteToGigabit(BigDecimal dg) {
		return bitToGigabit(kibibyteToBit(dg));
	}

	public static BigDecimal kibibyteToGigabyte(BigDecimal dg) {
		return bitToGigabyte(kibibyteToBit(dg));
	}

	public static BigDecimal kibibyteToGibibit(BigDecimal dg) {
		return bitToGibibit(kibibyteToBit(dg));
	}

	public static BigDecimal kibibyteToGibibyte(BigDecimal dg) {
		return bitToGibibyte(kibibyteToBit(dg));
	}

	public static BigDecimal kibibyteToTerabit(BigDecimal dg) {
		return bitToTerabit(kibibyteToBit(dg));
	}

	public static BigDecimal kibibyteToTerabyte(BigDecimal dg) {
		return bitToTerabyte(kibibyteToBit(dg));
	}

	public static BigDecimal kibibyteToTebibit(BigDecimal dg) {
		return bitToTebibit(kibibyteToBit(dg));
	}

	public static BigDecimal kibibyteToTebibyte(BigDecimal dg) {
		return bitToTebibyte(kibibyteToBit(dg));
	}

	public static BigDecimal kibibyteToPetabit(BigDecimal dg) {
		return bitToPetabit(kibibyteToBit(dg));
	}

	public static BigDecimal kibibyteToPetabyte(BigDecimal dg) {
		return bitToPetabyte(kibibyteToBit(dg));
	}

	public static BigDecimal kibibyteToPebibit(BigDecimal dg) {
		return bitToPebibit(kibibyteToBit(dg));
	}

	public static BigDecimal kibibyteToPebibyte(BigDecimal dg) {
		return bitToPebibyte(kibibyteToBit(dg));
	}

	public static BigDecimal megabitToBit(BigDecimal dg) {
		BigDecimal dgs = dg.multiply(new BigDecimal(kiloFactor).multiply(new BigDecimal(kiloFactor)));
		return dgs;
	}

	public static BigDecimal megabitToByte(BigDecimal dg) {
		return bitToByte(megabitToBit(dg));
	}

	public static BigDecimal megabitToKilobit(BigDecimal dg) {
		return bitToKilobit(megabitToBit(dg));
	}

	public static BigDecimal megabitToKilobyte(BigDecimal dg) {
		return bitToKilobyte(megabitToBit(dg));
	}

	public static BigDecimal megabitToKibibit(BigDecimal dg) {
		return bitToKibibit(megabitToBit(dg));
	}

	public static BigDecimal megabitToKibibyte(BigDecimal dg) {
		return bitToKibibyte(megabitToBit(dg));
	}

	public static BigDecimal megabitToMegabyte(BigDecimal dg) {
		return bitToMegabyte(megabitToBit(dg));
	}

	public static BigDecimal megabitToMebibit(BigDecimal dg) {
		return bitToMebibit(megabitToBit(dg));
	}

	public static BigDecimal megabitToMebibyte(BigDecimal dg) {
		return bitToMebibyte(megabitToBit(dg));
	}

	public static BigDecimal megabitToGigabit(BigDecimal dg) {
		return bitToGigabit(megabitToBit(dg));
	}

	public static BigDecimal megabitToGigabyte(BigDecimal dg) {
		return bitToGigabyte(megabitToBit(dg));
	}

	public static BigDecimal megabitToGibibit(BigDecimal dg) {
		return bitToGibibit(megabitToBit(dg));
	}

	public static BigDecimal megabitToGibibyte(BigDecimal dg) {
		return bitToGibibyte(megabitToBit(dg));
	}

	public static BigDecimal megabitToTerabit(BigDecimal dg) {
		return bitToTerabit(megabitToBit(dg));
	}

	public static BigDecimal megabitToTerabyte(BigDecimal dg) {
		return bitToTerabyte(megabitToBit(dg));
	}

	public static BigDecimal megabitToTebibit(BigDecimal dg) {
		return bitToTebibit(megabitToBit(dg));
	}

	public static BigDecimal megabitToTebibyte(BigDecimal dg) {
		return bitToTebibyte(megabitToBit(dg));
	}

	public static BigDecimal megabitToPetabit(BigDecimal dg) {
		return bitToPetabit(megabitToBit(dg));
	}

	public static BigDecimal megabitToPetabyte(BigDecimal dg) {
		return bitToPetabyte(megabitToBit(dg));
	}

	public static BigDecimal megabitToPebibit(BigDecimal dg) {
		return bitToPebibit(megabitToBit(dg));
	}

	public static BigDecimal megabitToPebibyte(BigDecimal dg) {
		return bitToPebibyte(megabitToBit(dg));
	}

	public static BigDecimal megabyteToBit(BigDecimal dg) {
		BigDecimal dgs = dg.multiply(new BigDecimal(kiloFactor).multiply(new BigDecimal(kiloFactor)).multiply(new BigDecimal(bitsInBytes)));
		return dgs;
	}

	public static BigDecimal megabyteToByte(BigDecimal dg) {
		return bitToByte(megabyteToBit(dg));
	}

	public static BigDecimal megabyteToKilobit(BigDecimal dg) {
		return bitToKilobit(megabyteToBit(dg));
	}

	public static BigDecimal megabyteToKilobyte(BigDecimal dg) {
		return bitToKilobyte(megabyteToBit(dg));
	}

	public static BigDecimal megabyteToKibibit(BigDecimal dg) {
		return bitToKibibit(megabyteToBit(dg));
	}

	public static BigDecimal megabyteToKibibyte(BigDecimal dg) {
		return bitToKibibyte(megabyteToBit(dg));
	}

	public static BigDecimal megabyteToMegabit(BigDecimal dg) {
		return bitToMegabit(megabyteToBit(dg));
	}

	public static BigDecimal megabyteToMebibit(BigDecimal dg) {
		return bitToMebibit(megabyteToBit(dg));
	}

	public static BigDecimal megabyteToMebibyte(BigDecimal dg) {
		return bitToMebibyte(megabyteToBit(dg));
	}

	public static BigDecimal megabyteToGigabit(BigDecimal dg) {
		return bitToGigabit(megabyteToBit(dg));
	}

	public static BigDecimal megabyteToGigabyte(BigDecimal dg) {
		return bitToGigabyte(megabyteToBit(dg));
	}

	public static BigDecimal megabyteToGibibit(BigDecimal dg) {
		return bitToGibibit(megabyteToBit(dg));
	}

	public static BigDecimal megabyteToGibibyte(BigDecimal dg) {
		return bitToGibibyte(megabyteToBit(dg));
	}

	public static BigDecimal megabyteToTerabit(BigDecimal dg) {
		return bitToTerabit(megabyteToBit(dg));
	}

	public static BigDecimal megabyteToTerabyte(BigDecimal dg) {
		return bitToTerabyte(megabyteToBit(dg));
	}

	public static BigDecimal megabyteToTebibit(BigDecimal dg) {
		return bitToTebibit(megabyteToBit(dg));
	}

	public static BigDecimal megabyteToTebibyte(BigDecimal dg) {
		return bitToTebibyte(megabyteToBit(dg));
	}

	public static BigDecimal megabyteToPetabit(BigDecimal dg) {
		return bitToPetabit(megabyteToBit(dg));
	}

	public static BigDecimal megabyteToPetabyte(BigDecimal dg) {
		return bitToPetabyte(megabyteToBit(dg));
	}

	public static BigDecimal megabyteToPebibit(BigDecimal dg) {
		return bitToPebibit(megabyteToBit(dg));
	}

	public static BigDecimal megabyteToPebibyte(BigDecimal dg) {
		return bitToPebibyte(megabyteToBit(dg));
	}

	public static BigDecimal mebibitToBit(BigDecimal dg) {
		BigDecimal dgs = dg.divide(new BigDecimal(bitsInKibibits).multiply(new BigDecimal(bitsInKibibits)));
		return dgs;
	}

	public static BigDecimal mebibitToByte(BigDecimal dg) {
		return bitToByte(mebibitToBit(dg));
	}

	public static BigDecimal mebibitToKilobit(BigDecimal dg) {
		return bitToKilobit(mebibitToBit(dg));
	}

	public static BigDecimal mebibitToKilobyte(BigDecimal dg) {
		return bitToKilobyte(mebibitToBit(dg));
	}

	public static BigDecimal mebibitToKibibit(BigDecimal dg) {
		return bitToKibibit(mebibitToBit(dg));
	}

	public static BigDecimal mebibitToKibibyte(BigDecimal dg) {
		return bitToKibibyte(mebibitToBit(dg));
	}

	public static BigDecimal mebibitToMegabit(BigDecimal dg) {
		return bitToMegabit(mebibitToBit(dg));
	}

	public static BigDecimal mebibitToMegabyte(BigDecimal dg) {
		return bitToMegabyte(mebibitToBit(dg));
	}

	public static BigDecimal mebibitToMebibyte(BigDecimal dg) {
		return bitToMebibyte(mebibitToBit(dg));
	}

	public static BigDecimal mebibitToGigabit(BigDecimal dg) {
		return bitToGigabit(mebibitToBit(dg));
	}

	public static BigDecimal mebibitToGigabyte(BigDecimal dg) {
		return bitToGigabyte(mebibitToBit(dg));
	}

	public static BigDecimal mebibitToGibibit(BigDecimal dg) {
		return bitToGibibit(mebibitToBit(dg));
	}

	public static BigDecimal mebibitToGibibyte(BigDecimal dg) {
		return bitToGibibyte(mebibitToBit(dg));
	}

	public static BigDecimal mebibitToTerabit(BigDecimal dg) {
		return bitToTerabit(mebibitToBit(dg));
	}

	public static BigDecimal mebibitToTerabyte(BigDecimal dg) {
		return bitToTerabyte(mebibitToBit(dg));
	}

	public static BigDecimal mebibitToTebibit(BigDecimal dg) {
		return bitToTebibit(mebibitToBit(dg));
	}

	public static BigDecimal mebibitToTebibyte(BigDecimal dg) {
		return bitToTebibyte(mebibitToBit(dg));
	}

	public static BigDecimal mebibitToPetabit(BigDecimal dg) {
		return bitToPetabit(mebibitToBit(dg));
	}

	public static BigDecimal mebibitToPetabyte(BigDecimal dg) {
		return bitToPetabyte(mebibitToBit(dg));
	}

	public static BigDecimal mebibitToPebibit(BigDecimal dg) {
		return bitToPebibit(mebibitToBit(dg));
	}

	public static BigDecimal mebibitToPebibyte(BigDecimal dg) {
		return bitToPebibyte(mebibitToBit(dg));
	}

	public static BigDecimal mebibyteToBit(BigDecimal dg) {
		BigDecimal dgs = dg.multiply(new BigDecimal(bitsInKibibits).multiply(new BigDecimal(bitsInKibibits)).multiply(new BigDecimal(bitsInBytes)));
		return dgs;
	}

	public static BigDecimal mebibyteToByte(BigDecimal dg) {
		return bitToByte(mebibyteToBit(dg));
	}

	public static BigDecimal mebibyteToKilobit(BigDecimal dg) {
		return bitToKilobit(mebibyteToBit(dg));
	}

	public static BigDecimal mebibyteToKilobyte(BigDecimal dg) {
		return bitToKilobyte(mebibyteToBit(dg));
	}

	public static BigDecimal mebibyteToKibibit(BigDecimal dg) {
		return bitToKibibit(mebibyteToBit(dg));
	}

	public static BigDecimal mebibyteToKibibyte(BigDecimal dg) {
		return bitToKibibyte(mebibyteToBit(dg));
	}

	public static BigDecimal mebibyteToMegabit(BigDecimal dg) {
		return bitToMegabit(mebibyteToBit(dg));
	}

	public static BigDecimal mebibyteToMegabyte(BigDecimal dg) {
		return bitToMegabyte(mebibyteToBit(dg));
	}

	public static BigDecimal mebibyteToMebibit(BigDecimal dg) {
		return bitToMebibit(mebibyteToBit(dg));
	}

	public static BigDecimal mebibyteToGigabit(BigDecimal dg) {
		return bitToGigabit(mebibyteToBit(dg));
	}

	public static BigDecimal mebibyteToGigabyte(BigDecimal dg) {
		return bitToGigabyte(mebibyteToBit(dg));
	}

	public static BigDecimal mebibyteToGibibit(BigDecimal dg) {
		return bitToGibibit(mebibyteToBit(dg));
	}

	public static BigDecimal mebibyteToGibibyte(BigDecimal dg) {
		return bitToGibibyte(mebibyteToBit(dg));
	}

	public static BigDecimal mebibyteToTerabit(BigDecimal dg) {
		return bitToTerabit(mebibyteToBit(dg));
	}

	public static BigDecimal mebibyteToTerabyte(BigDecimal dg) {
		return bitToTerabyte(mebibyteToBit(dg));
	}

	public static BigDecimal mebibyteToTebibit(BigDecimal dg) {
		return bitToTebibit(mebibyteToBit(dg));
	}

	public static BigDecimal mebibyteToTebibyte(BigDecimal dg) {
		return bitToTebibyte(mebibyteToBit(dg));
	}

	public static BigDecimal mebibyteToPetabit(BigDecimal dg) {
		return bitToPetabit(mebibyteToBit(dg));
	}

	public static BigDecimal mebibyteToPetabyte(BigDecimal dg) {
		return bitToPetabyte(mebibyteToBit(dg));
	}

	public static BigDecimal mebibyteToPebibit(BigDecimal dg) {
		return bitToPebibit(mebibyteToBit(dg));
	}

	public static BigDecimal mebibyteToPebibyte(BigDecimal dg) {
		return bitToPebibyte(mebibyteToBit(dg));
	}

	public static BigDecimal gigabitToBit(BigDecimal dg) {
		BigDecimal dgs = dg.multiply(new BigDecimal(kiloFactor).multiply(new BigDecimal(kiloFactor)).multiply(new BigDecimal(kiloFactor)));
		return dgs;
	}

	public static BigDecimal gigabitToByte(BigDecimal dg) {
		return bitToByte(gigabitToBit(dg));
	}

	public static BigDecimal gigabitToKilobit(BigDecimal dg) {
		return bitToKilobit(gigabitToBit(dg));
	}

	public static BigDecimal gigabitToKilobyte(BigDecimal dg) {
		return bitToKilobyte(gigabitToBit(dg));
	}

	public static BigDecimal gigabitToKibibit(BigDecimal dg) {
		return bitToKibibit(gigabitToBit(dg));
	}

	public static BigDecimal gigabitToKibibyte(BigDecimal dg) {
		return bitToKibibyte(gigabitToBit(dg));
	}

	public static BigDecimal gigabitToMegabit(BigDecimal dg) {
		return bitToMegabit(gigabitToBit(dg));
	}

	public static BigDecimal gigabitToMegabyte(BigDecimal dg) {
		return bitToMegabyte(gigabitToBit(dg));
	}

	public static BigDecimal gigabitToMebibit(BigDecimal dg) {
		return bitToMebibit(gigabitToBit(dg));
	}

	public static BigDecimal gigabitToMebibyte(BigDecimal dg) {
		return bitToMebibyte(gigabitToBit(dg));
	}

	public static BigDecimal gigabitToGigabyte(BigDecimal dg) {
		return bitToGigabyte(gigabitToBit(dg));
	}

	public static BigDecimal gigabitToGibibit(BigDecimal dg) {
		return bitToGibibit(gigabitToBit(dg));
	}

	public static BigDecimal gigabitToGibibyte(BigDecimal dg) {
		return bitToGibibyte(gigabitToBit(dg));
	}

	public static BigDecimal gigabitToTerabit(BigDecimal dg) {
		return bitToTerabit(gigabitToBit(dg));
	}

	public static BigDecimal gigabitToTerabyte(BigDecimal dg) {
		return bitToTerabyte(gigabitToBit(dg));
	}

	public static BigDecimal gigabitToTebibit(BigDecimal dg) {
		return bitToTebibit(gigabitToBit(dg));
	}

	public static BigDecimal gigabitToTebibyte(BigDecimal dg) {
		return bitToTebibyte(gigabitToBit(dg));
	}

	public static BigDecimal gigabitToPetabit(BigDecimal dg) {
		return bitToPetabit(gigabitToBit(dg));
	}

	public static BigDecimal gigabitToPetabyte(BigDecimal dg) {
		return bitToPetabyte(gigabitToBit(dg));
	}

	public static BigDecimal gigabitToPebibit(BigDecimal dg) {
		return bitToPebibit(gigabitToBit(dg));
	}

	public static BigDecimal gigabitToPebibyte(BigDecimal dg) {
		return bitToPebibyte(gigabitToBit(dg));
	}

	public static BigDecimal gigabyteToBit(BigDecimal dg) {
		BigDecimal dgs = dg.multiply(new BigDecimal(kiloFactor).multiply(new BigDecimal(kiloFactor)).multiply(new BigDecimal(kiloFactor))
				.multiply(new BigDecimal(bitsInBytes)));
		return dgs;
	}

	public static BigDecimal gigabyteToByte(BigDecimal dg) {
		return bitToByte(gigabyteToBit(dg));
	}

	public static BigDecimal gigabyteToKilobit(BigDecimal dg) {
		return bitToKilobit(gigabyteToBit(dg));
	}

	public static BigDecimal gigabyteToKilobyte(BigDecimal dg) {
		return bitToKilobyte(gigabyteToBit(dg));
	}

	public static BigDecimal gigabyteToKibibit(BigDecimal dg) {
		return bitToKibibit(gigabyteToBit(dg));
	}

	public static BigDecimal gigabyteToKibibyte(BigDecimal dg) {
		return bitToKibibyte(gigabyteToBit(dg));
	}

	public static BigDecimal gigabyteToMegabit(BigDecimal dg) {
		return bitToMegabit(gigabyteToBit(dg));
	}

	public static BigDecimal gigabyteToMegabyte(BigDecimal dg) {
		return bitToMegabyte(gigabyteToBit(dg));
	}

	public static BigDecimal gigabyteToMebibit(BigDecimal dg) {
		return bitToMebibit(gigabyteToBit(dg));
	}

	public static BigDecimal gigabyteToMebibyte(BigDecimal dg) {
		return bitToMebibyte(gigabyteToBit(dg));
	}

	public static BigDecimal gigabyteToGigabit(BigDecimal dg) {
		return bitToGigabit(gigabyteToBit(dg));
	}

	public static BigDecimal gigabyteToGibibit(BigDecimal dg) {
		return bitToGibibit(gigabyteToBit(dg));
	}

	public static BigDecimal gigabyteToGibibyte(BigDecimal dg) {
		return bitToGibibyte(gigabyteToBit(dg));
	}

	public static BigDecimal gigabyteToTerabit(BigDecimal dg) {
		return bitToTerabit(gigabyteToBit(dg));
	}

	public static BigDecimal gigabyteToTerabyte(BigDecimal dg) {
		return bitToTerabyte(gigabyteToBit(dg));
	}

	public static BigDecimal gigabyteToTebibit(BigDecimal dg) {
		return bitToTebibit(gigabyteToBit(dg));
	}

	public static BigDecimal gigabyteToTebibyte(BigDecimal dg) {
		return bitToTebibyte(gigabyteToBit(dg));
	}

	public static BigDecimal gigabyteToPetabit(BigDecimal dg) {
		return bitToPetabit(gigabyteToBit(dg));
	}

	public static BigDecimal gigabyteToPetabyte(BigDecimal dg) {
		return bitToPetabyte(gigabyteToBit(dg));
	}

	public static BigDecimal gigabyteToPebibit(BigDecimal dg) {
		return bitToPebibit(gigabyteToBit(dg));
	}

	public static BigDecimal gigabyteToPebibyte(BigDecimal dg) {
		return bitToPebibyte(gigabyteToBit(dg));
	}

	public static BigDecimal gibibitToBit(BigDecimal dg) {
		BigDecimal dgs = dg.multiply(new BigDecimal(bitsInKibibits).multiply(new BigDecimal(bitsInKibibits)).multiply(new BigDecimal(bitsInKibibits)));
		return dgs;
	}

	public static BigDecimal gibibitToByte(BigDecimal dg) {
		return bitToByte(gibibitToBit(dg));
	}

	public static BigDecimal gibibitToKilobit(BigDecimal dg) {
		return bitToKilobit(gibibitToBit(dg));
	}

	public static BigDecimal gibibitToKilobyte(BigDecimal dg) {
		return bitToKilobyte(gibibitToBit(dg));
	}

	public static BigDecimal gibibitToKibibit(BigDecimal dg) {
		return bitToKibibit(gibibitToBit(dg));
	}

	public static BigDecimal gibibitToKibibyte(BigDecimal dg) {
		return bitToKibibyte(gibibitToBit(dg));
	}

	public static BigDecimal gibibitToMegabit(BigDecimal dg) {
		return bitToMegabit(gibibitToBit(dg));
	}

	public static BigDecimal gibibitToMegabyte(BigDecimal dg) {
		return bitToMegabyte(gibibitToBit(dg));
	}

	public static BigDecimal gibibitToMebibit(BigDecimal dg) {
		return bitToMebibit(gibibitToBit(dg));
	}

	public static BigDecimal gibibitToMebibyte(BigDecimal dg) {
		return bitToMebibyte(gibibitToBit(dg));
	}

	public static BigDecimal gibibitToGigabit(BigDecimal dg) {
		return bitToGigabit(gibibitToBit(dg));
	}

	public static BigDecimal gibibitToGigabyte(BigDecimal dg) {
		return bitToGigabyte(gibibitToBit(dg));
	}

	public static BigDecimal gibibitToGibibyte(BigDecimal dg) {
		return bitToGibibyte(gibibitToBit(dg));
	}

	public static BigDecimal gibibitToTerabit(BigDecimal dg) {
		return bitToTerabit(gibibitToBit(dg));
	}

	public static BigDecimal gibibitToTerabyte(BigDecimal dg) {
		return bitToTerabyte(gibibitToBit(dg));
	}

	public static BigDecimal gibibitToTebibit(BigDecimal dg) {
		return bitToTebibit(gibibitToBit(dg));
	}

	public static BigDecimal gibibitToTebibyte(BigDecimal dg) {
		return bitToTebibyte(gibibitToBit(dg));
	}

	public static BigDecimal gibibitToPetabit(BigDecimal dg) {
		return bitToPetabit(gibibitToBit(dg));
	}

	public static BigDecimal gibibitToPetabyte(BigDecimal dg) {
		return bitToPetabyte(gibibitToBit(dg));
	}

	public static BigDecimal gibibitToPebibit(BigDecimal dg) {
		return bitToPebibit(gibibitToBit(dg));
	}

	public static BigDecimal gibibitToPebibyte(BigDecimal dg) {
		return bitToPebibyte(gibibitToBit(dg));
	}

	public static BigDecimal gibibyteToBit(BigDecimal dg) {
		BigDecimal dgs = dg.multiply(new BigDecimal(bitsInKibibits).multiply(new BigDecimal(bitsInKibibits)).multiply(new BigDecimal(bitsInKibibits))
				.multiply(new BigDecimal(bitsInBytes)));
		return dgs;
	}

	public static BigDecimal gibibyteToByte(BigDecimal dg) {
		return bitToByte(gibibyteToBit(dg));
	}

	public static BigDecimal gibibyteToKilobit(BigDecimal dg) {
		return bitToKilobit(gibibyteToBit(dg));
	}

	public static BigDecimal gibibyteToKilobyte(BigDecimal dg) {
		return bitToKilobyte(gibibyteToBit(dg));
	}

	public static BigDecimal gibibyteToKibibit(BigDecimal dg) {
		return bitToKibibit(gibibyteToBit(dg));
	}

	public static BigDecimal gibibyteToKibibyte(BigDecimal dg) {
		return bitToKibibyte(gibibyteToBit(dg));
	}

	public static BigDecimal gibibyteToMegabit(BigDecimal dg) {
		return bitToMegabit(gibibyteToBit(dg));
	}

	public static BigDecimal gibibyteToMegabyte(BigDecimal dg) {
		return bitToMegabyte(gibibyteToBit(dg));
	}

	public static BigDecimal gibibyteToMebibit(BigDecimal dg) {
		return bitToMebibit(gibibyteToBit(dg));
	}

	public static BigDecimal gibibyteToMebibyte(BigDecimal dg) {
		return bitToMebibyte(gibibyteToBit(dg));
	}

	public static BigDecimal gibibyteToGigabit(BigDecimal dg) {
		return bitToGigabit(gibibyteToBit(dg));
	}

	public static BigDecimal gibibyteToGigabyte(BigDecimal dg) {
		return bitToGigabyte(gibibyteToBit(dg));
	}

	public static BigDecimal gibibyteToGibibit(BigDecimal dg) {
		return bitToGibibit(gibibyteToBit(dg));
	}

	public static BigDecimal gibibyteToTerabit(BigDecimal dg) {
		return bitToTerabit(gibibyteToBit(dg));
	}

	public static BigDecimal gibibyteToTerabyte(BigDecimal dg) {
		return bitToTerabyte(gibibyteToBit(dg));
	}

	public static BigDecimal gibibyteToTebibit(BigDecimal dg) {
		return bitToTebibit(gibibyteToBit(dg));
	}

	public static BigDecimal gibibyteToTebibyte(BigDecimal dg) {
		return bitToTebibyte(gibibyteToBit(dg));
	}

	public static BigDecimal gibibyteToPetabit(BigDecimal dg) {
		return bitToPetabit(gibibyteToBit(dg));
	}

	public static BigDecimal gibibyteToPetabyte(BigDecimal dg) {
		return bitToPetabyte(gibibyteToBit(dg));
	}

	public static BigDecimal gibibyteToPebibit(BigDecimal dg) {
		return bitToPebibit(gibibyteToBit(dg));
	}

	public static BigDecimal gibibyteToPebibyte(BigDecimal dg) {
		return bitToPebibyte(gibibyteToBit(dg));
	}

	public static BigDecimal terabitToBit(BigDecimal dg) {
		BigDecimal dgs = dg.multiply(new BigDecimal(kiloFactor).multiply(new BigDecimal(kiloFactor)).multiply(new BigDecimal(kiloFactor))
				.multiply(new BigDecimal(kiloFactor)));
		return dgs;
	}

	public static BigDecimal terabitToByte(BigDecimal dg) {
		return bitToByte(terabitToBit(dg));
	}

	public static BigDecimal terabitToKilobit(BigDecimal dg) {
		return bitToKilobit(terabitToBit(dg));
	}

	public static BigDecimal terabitToKilobyte(BigDecimal dg) {
		return bitToKilobyte(terabitToBit(dg));
	}

	public static BigDecimal terabitToKibibit(BigDecimal dg) {
		return bitToKibibit(terabitToBit(dg));
	}

	public static BigDecimal terabitToKibibyte(BigDecimal dg) {
		return bitToKibibyte(terabitToBit(dg));
	}

	public static BigDecimal terabitToMegabit(BigDecimal dg) {
		return bitToMegabit(terabitToBit(dg));
	}

	public static BigDecimal terabitToMegabyte(BigDecimal dg) {
		return bitToMegabyte(terabitToBit(dg));
	}

	public static BigDecimal terabitToMebibit(BigDecimal dg) {
		return bitToMebibit(terabitToBit(dg));
	}

	public static BigDecimal terabitToMebibyte(BigDecimal dg) {
		return bitToMebibyte(terabitToBit(dg));
	}

	public static BigDecimal terabitToGigabit(BigDecimal dg) {
		return bitToGigabit(terabitToBit(dg));
	}

	public static BigDecimal terabitToGigabyte(BigDecimal dg) {
		return bitToGigabyte(terabitToBit(dg));
	}

	public static BigDecimal terabitToGibibit(BigDecimal dg) {
		return bitToGibibit(terabitToBit(dg));
	}

	public static BigDecimal terabitToGibibyte(BigDecimal dg) {
		return bitToGibibyte(terabitToBit(dg));
	}

	public static BigDecimal terabitToTerabyte(BigDecimal dg) {
		return bitToTerabyte(terabitToBit(dg));
	}

	public static BigDecimal terabitToTebibit(BigDecimal dg) {
		return bitToTebibit(terabitToBit(dg));
	}

	public static BigDecimal terabitToTebibyte(BigDecimal dg) {
		return bitToTebibyte(terabitToBit(dg));
	}

	public static BigDecimal terabitToPetabit(BigDecimal dg) {
		return bitToPetabit(terabitToBit(dg));
	}

	public static BigDecimal terabitToPetabyte(BigDecimal dg) {
		return bitToPetabyte(terabitToBit(dg));
	}

	public static BigDecimal terabitToPebibit(BigDecimal dg) {
		return bitToPebibit(terabitToBit(dg));
	}

	public static BigDecimal terabitToPebibyte(BigDecimal dg) {
		return bitToPebibyte(terabitToBit(dg));
	}

	public static BigDecimal terabyteToBit(BigDecimal dg) {
		BigDecimal dgs = dg.multiply(new BigDecimal(kiloFactor).multiply(new BigDecimal(kiloFactor)).multiply(new BigDecimal(kiloFactor))
				.multiply(new BigDecimal(kiloFactor)).multiply(new BigDecimal(bitsInBytes)));
		return dgs;
	}

	public static BigDecimal terabyteToByte(BigDecimal dg) {
		return bitToByte(terabyteToBit(dg));
	}

	public static BigDecimal terabyteToKilobit(BigDecimal dg) {
		return bitToKilobit(terabyteToBit(dg));
	}

	public static BigDecimal terabyteToKilobyte(BigDecimal dg) {
		return bitToKilobyte(terabyteToBit(dg));
	}

	public static BigDecimal terabyteToKibibit(BigDecimal dg) {
		return bitToKibibit(terabyteToBit(dg));
	}

	public static BigDecimal terabyteToKibibyte(BigDecimal dg) {
		return bitToKibibyte(terabyteToBit(dg));
	}

	public static BigDecimal terabyteToMegabit(BigDecimal dg) {
		return bitToMegabit(terabyteToBit(dg));
	}

	public static BigDecimal terabyteToMegabyte(BigDecimal dg) {
		return bitToMegabyte(terabyteToBit(dg));
	}

	public static BigDecimal terabyteToMebibit(BigDecimal dg) {
		return bitToMebibit(terabyteToBit(dg));
	}

	public static BigDecimal terabyteToMebibyte(BigDecimal dg) {
		return bitToMebibyte(terabyteToBit(dg));
	}

	public static BigDecimal terabyteToGigabit(BigDecimal dg) {
		return bitToGigabit(terabyteToBit(dg));
	}

	public static BigDecimal terabyteToGigabyte(BigDecimal dg) {
		return bitToGigabyte(terabyteToBit(dg));
	}

	public static BigDecimal terabyteToGibibit(BigDecimal dg) {
		return bitToGibibit(terabyteToBit(dg));
	}

	public static BigDecimal terabyteToGibibyte(BigDecimal dg) {
		return bitToGibibyte(terabyteToBit(dg));
	}

	public static BigDecimal terabyteToTerabit(BigDecimal dg) {
		return bitToTerabit(terabyteToBit(dg));
	}

	public static BigDecimal terabyteToTebibit(BigDecimal dg) {
		return bitToTebibit(terabyteToBit(dg));
	}

	public static BigDecimal terabyteToTebibyte(BigDecimal dg) {
		return bitToTebibyte(terabyteToBit(dg));
	}

	public static BigDecimal terabyteToPetabit(BigDecimal dg) {
		return bitToPetabit(terabyteToBit(dg));
	}

	public static BigDecimal terabyteToPetabyte(BigDecimal dg) {
		return bitToPetabyte(terabyteToBit(dg));
	}

	public static BigDecimal terabyteToPebibit(BigDecimal dg) {
		return bitToPebibit(terabyteToBit(dg));
	}

	public static BigDecimal terabyteToPebibyte(BigDecimal dg) {
		return bitToPebibyte(terabyteToBit(dg));
	}

	public static BigDecimal tebibitToBit(BigDecimal dg) {
		BigDecimal dgs = dg.multiply(new BigDecimal(bitsInKibibits).multiply(new BigDecimal(bitsInKibibits)).multiply(new BigDecimal(bitsInKibibits))
				.multiply(new BigDecimal(bitsInKibibits)));
		return dgs;
	}

	public static BigDecimal tebibitToByte(BigDecimal dg) {
		return bitToByte(tebibitToBit(dg));
	}

	public static BigDecimal tebibitToKilobit(BigDecimal dg) {
		return bitToKilobit(tebibitToBit(dg));
	}

	public static BigDecimal tebibitToKilobyte(BigDecimal dg) {
		return bitToKilobyte(tebibitToBit(dg));
	}

	public static BigDecimal tebibitToKibibit(BigDecimal dg) {
		return bitToKibibit(tebibitToBit(dg));
	}

	public static BigDecimal tebibitToKibibyte(BigDecimal dg) {
		return bitToKibibyte(tebibitToBit(dg));
	}

	public static BigDecimal tebibitToMegabit(BigDecimal dg) {
		return bitToMegabit(tebibitToBit(dg));
	}

	public static BigDecimal tebibitToMegabyte(BigDecimal dg) {
		return bitToMegabyte(tebibitToBit(dg));
	}

	public static BigDecimal tebibitToMebibit(BigDecimal dg) {
		return bitToMebibit(tebibitToBit(dg));
	}

	public static BigDecimal tebibitToMebibyte(BigDecimal dg) {
		return bitToMebibyte(tebibitToBit(dg));
	}

	public static BigDecimal tebibitToGigabit(BigDecimal dg) {
		return bitToGigabit(tebibitToBit(dg));
	}

	public static BigDecimal tebibitToGigabyte(BigDecimal dg) {
		return bitToGigabyte(tebibitToBit(dg));
	}

	public static BigDecimal tebibitToGibibit(BigDecimal dg) {
		return bitToGibibit(tebibitToBit(dg));
	}

	public static BigDecimal tebibitToGibibyte(BigDecimal dg) {
		return bitToGibibyte(tebibitToBit(dg));
	}

	public static BigDecimal tebibitToTerabit(BigDecimal dg) {
		return bitToTerabit(tebibitToBit(dg));
	}

	public static BigDecimal tebibitToTerabyte(BigDecimal dg) {
		return bitToTerabyte(tebibitToBit(dg));
	}

	public static BigDecimal tebibitToTebibyte(BigDecimal dg) {
		return bitToTebibyte(tebibitToBit(dg));
	}

	public static BigDecimal tebibitToPetabit(BigDecimal dg) {
		return bitToPetabit(tebibitToBit(dg));
	}

	public static BigDecimal tebibitToPetabyte(BigDecimal dg) {
		return bitToPetabyte(tebibitToBit(dg));
	}

	public static BigDecimal tebibitToPebibit(BigDecimal dg) {
		return bitToPebibit(tebibitToBit(dg));
	}

	public static BigDecimal tebibitToPebibyte(BigDecimal dg) {
		return bitToPebibyte(tebibitToBit(dg));
	}

	public static BigDecimal tebibyteToBit(BigDecimal dg) {
		BigDecimal dgs = dg.multiply(new BigDecimal(bitsInKibibits).multiply(new BigDecimal(bitsInKibibits)).multiply(new BigDecimal(bitsInKibibits))
				.multiply(new BigDecimal(bitsInKibibits)).multiply(new BigDecimal(bitsInBytes)));
		return dgs;
	}

	public static BigDecimal tebibyteToByte(BigDecimal dg) {
		return bitToByte(tebibyteToBit(dg));
	}

	public static BigDecimal tebibyteToKilobit(BigDecimal dg) {
		return bitToKilobit(tebibyteToBit(dg));
	}

	public static BigDecimal tebibyteToKilobyte(BigDecimal dg) {
		return bitToKilobyte(tebibyteToBit(dg));
	}

	public static BigDecimal tebibyteToKibibit(BigDecimal dg) {
		return bitToKibibit(tebibyteToBit(dg));
	}

	public static BigDecimal tebibyteToKibibyte(BigDecimal dg) {
		return bitToKibibyte(tebibyteToBit(dg));
	}

	public static BigDecimal tebibyteToMegabit(BigDecimal dg) {
		return bitToMegabit(tebibyteToBit(dg));
	}

	public static BigDecimal tebibyteToMegabyte(BigDecimal dg) {
		return bitToMegabyte(tebibyteToBit(dg));
	}

	public static BigDecimal tebibyteToMebibit(BigDecimal dg) {
		return bitToMebibit(tebibyteToBit(dg));
	}

	public static BigDecimal tebibyteToMebibyte(BigDecimal dg) {
		return bitToMebibyte(tebibyteToBit(dg));
	}

	public static BigDecimal tebibyteToGigabit(BigDecimal dg) {
		return bitToGigabit(tebibyteToBit(dg));
	}

	public static BigDecimal tebibyteToGigabyte(BigDecimal dg) {
		return bitToGigabyte(tebibyteToBit(dg));
	}

	public static BigDecimal tebibyteToGibibit(BigDecimal dg) {
		return bitToGibibit(tebibyteToBit(dg));
	}

	public static BigDecimal tebibyteToGibibyte(BigDecimal dg) {
		return bitToGibibyte(tebibyteToBit(dg));
	}

	public static BigDecimal tebibyteToTerabit(BigDecimal dg) {
		return bitToTerabit(tebibyteToBit(dg));
	}

	public static BigDecimal tebibyteToTerabyte(BigDecimal dg) {
		return bitToTerabyte(tebibyteToBit(dg));
	}

	public static BigDecimal tebibyteToTebibit(BigDecimal dg) {
		return bitToTebibit(tebibyteToBit(dg));
	}

	public static BigDecimal tebibyteToPetabit(BigDecimal dg) {
		return bitToPetabit(tebibyteToBit(dg));
	}

	public static BigDecimal tebibyteToPetabyte(BigDecimal dg) {
		return bitToPetabyte(tebibyteToBit(dg));
	}

	public static BigDecimal tebibyteToPebibit(BigDecimal dg) {
		return bitToPebibit(tebibyteToBit(dg));
	}

	public static BigDecimal tebibyteToPebibyte(BigDecimal dg) {
		return bitToPebibyte(tebibyteToBit(dg));
	}

	public static BigDecimal petabitToBit(BigDecimal dg) {
		BigDecimal dgs = dg.multiply(new BigDecimal(kiloFactor).multiply(new BigDecimal(kiloFactor)).multiply(new BigDecimal(kiloFactor))
				.multiply(new BigDecimal(kiloFactor)).multiply(new BigDecimal(kiloFactor)));
		return dgs;
	}

	public static BigDecimal petabitToByte(BigDecimal dg) {
		return bitToByte(petabitToBit(dg));
	}

	public static BigDecimal petabitToKilobit(BigDecimal dg) {
		return bitToKilobit(petabitToBit(dg));
	}

	public static BigDecimal petabitToKilobyte(BigDecimal dg) {
		return bitToKilobyte(petabitToBit(dg));
	}

	public static BigDecimal petabitToKibibit(BigDecimal dg) {
		return bitToKibibit(petabitToBit(dg));
	}

	public static BigDecimal petabitToKibibyte(BigDecimal dg) {
		return bitToKibibyte(petabitToBit(dg));
	}

	public static BigDecimal petabitToMegabit(BigDecimal dg) {
		return bitToMegabit(petabitToBit(dg));
	}

	public static BigDecimal petabitToMegabyte(BigDecimal dg) {
		return bitToMegabyte(petabitToBit(dg));
	}

	public static BigDecimal petabitToMebibit(BigDecimal dg) {
		return bitToMebibit(petabitToBit(dg));
	}

	public static BigDecimal petabitToMebibyte(BigDecimal dg) {
		return bitToMebibyte(petabitToBit(dg));
	}

	public static BigDecimal petabitToGigabit(BigDecimal dg) {
		return bitToGigabit(petabitToBit(dg));
	}

	public static BigDecimal petabitToGigabyte(BigDecimal dg) {
		return bitToGigabyte(petabitToBit(dg));
	}

	public static BigDecimal petabitToGibibit(BigDecimal dg) {
		return bitToGibibit(petabitToBit(dg));
	}

	public static BigDecimal petabitToGibibyte(BigDecimal dg) {
		return bitToGibibyte(petabitToBit(dg));
	}

	public static BigDecimal petabitToTerabit(BigDecimal dg) {
		return bitToTerabit(petabitToBit(dg));
	}

	public static BigDecimal petabitToTerabyte(BigDecimal dg) {
		return bitToTerabyte(petabitToBit(dg));
	}

	public static BigDecimal petabitToTebibit(BigDecimal dg) {
		return bitToTebibit(petabitToBit(dg));
	}

	public static BigDecimal petabitToTebibyte(BigDecimal dg) {
		return bitToTebibyte(petabitToBit(dg));
	}

	public static BigDecimal petabitToPetabyte(BigDecimal dg) {
		return bitToPetabyte(petabitToBit(dg));
	}

	public static BigDecimal petabitToPebibit(BigDecimal dg) {
		return bitToPebibit(petabitToBit(dg));
	}

	public static BigDecimal petabitToPebibyte(BigDecimal dg) {
		return bitToPebibyte(petabitToBit(dg));
	}

	public static BigDecimal petabyteToBit(BigDecimal dg) {
		BigDecimal dgs = dg.multiply(new BigDecimal(kiloFactor).multiply(new BigDecimal(kiloFactor)).multiply(new BigDecimal(kiloFactor))
				.multiply(new BigDecimal(kiloFactor)).multiply(new BigDecimal(kiloFactor)).multiply(new BigDecimal(bitsInBytes)));
		return dgs;
	}

	public static BigDecimal petabyteToByte(BigDecimal dg) {
		return bitToByte(petabyteToBit(dg));
	}

	public static BigDecimal petabyteToKilobit(BigDecimal dg) {
		return bitToKilobit(petabyteToBit(dg));
	}

	public static BigDecimal petabyteToKilobyte(BigDecimal dg) {
		return bitToKilobyte(petabyteToBit(dg));
	}

	public static BigDecimal petabyteToKibibit(BigDecimal dg) {
		return bitToKibibit(petabyteToBit(dg));
	}

	public static BigDecimal petabyteToKibibyte(BigDecimal dg) {
		return bitToKibibyte(petabyteToBit(dg));
	}

	public static BigDecimal petabyteToMegabit(BigDecimal dg) {
		return bitToMegabit(petabyteToBit(dg));
	}

	public static BigDecimal petabyteToMegabyte(BigDecimal dg) {
		return bitToMegabyte(petabyteToBit(dg));
	}

	public static BigDecimal petabyteToMebibit(BigDecimal dg) {
		return bitToMebibit(petabyteToBit(dg));
	}

	public static BigDecimal petabyteToMebibyte(BigDecimal dg) {
		return bitToMebibyte(petabyteToBit(dg));
	}

	public static BigDecimal petabyteToGigabit(BigDecimal dg) {
		return bitToGigabit(petabyteToBit(dg));
	}

	public static BigDecimal petabyteToGigabyte(BigDecimal dg) {
		return bitToGigabyte(petabyteToBit(dg));
	}

	public static BigDecimal petabyteToGibibit(BigDecimal dg) {
		return bitToGibibit(petabyteToBit(dg));
	}

	public static BigDecimal petabyteToGibibyte(BigDecimal dg) {
		return bitToGibibyte(petabyteToBit(dg));
	}

	public static BigDecimal petabyteToTerabit(BigDecimal dg) {
		return bitToTerabit(petabyteToBit(dg));
	}

	public static BigDecimal petabyteToTerabyte(BigDecimal dg) {
		return bitToTerabyte(petabyteToBit(dg));
	}

	public static BigDecimal petabyteToTebibit(BigDecimal dg) {
		return bitToTebibit(petabyteToBit(dg));
	}

	public static BigDecimal petabyteToTebibyte(BigDecimal dg) {
		return bitToTebibyte(petabyteToBit(dg));
	}

	public static BigDecimal petabyteToPetabit(BigDecimal dg) {
		return bitToPetabit(petabyteToBit(dg));
	}

	public static BigDecimal petabyteToPebibit(BigDecimal dg) {
		return bitToPebibit(petabyteToBit(dg));
	}

	public static BigDecimal petabyteToPebibyte(BigDecimal dg) {
		return bitToPebibyte(petabyteToBit(dg));
	}

	public static BigDecimal pebibitToBit(BigDecimal dg) {
		BigDecimal dgs = dg.multiply(new BigDecimal(bitsInKibibits).multiply(new BigDecimal(bitsInKibibits)).multiply(new BigDecimal(bitsInKibibits))
				.multiply(new BigDecimal(bitsInKibibits)).multiply(new BigDecimal(bitsInKibibits)));
		return dgs;
	}

	public static BigDecimal pebibitToByte(BigDecimal dg) {
		return bitToByte(pebibitToBit(dg));
	}

	public static BigDecimal pebibitToKilobit(BigDecimal dg) {
		return bitToKilobit(pebibitToBit(dg));
	}

	public static BigDecimal pebibitToKilobyte(BigDecimal dg) {
		return bitToKilobyte(pebibitToBit(dg));
	}

	public static BigDecimal pebibitToKibibit(BigDecimal dg) {
		return bitToKibibit(pebibitToBit(dg));
	}

	public static BigDecimal pebibitToKibibyte(BigDecimal dg) {
		return bitToKibibyte(pebibitToBit(dg));
	}

	public static BigDecimal pebibitToMegabit(BigDecimal dg) {
		return bitToMegabit(pebibitToBit(dg));
	}

	public static BigDecimal pebibitToMegabyte(BigDecimal dg) {
		return bitToMegabyte(pebibitToBit(dg));
	}

	public static BigDecimal pebibitToMebibit(BigDecimal dg) {
		return bitToMebibit(pebibitToBit(dg));
	}

	public static BigDecimal pebibitToMebibyte(BigDecimal dg) {
		return bitToMebibyte(pebibitToBit(dg));
	}

	public static BigDecimal pebibitToGigabit(BigDecimal dg) {
		return bitToGigabit(pebibitToBit(dg));
	}

	public static BigDecimal pebibitToGigabyte(BigDecimal dg) {
		return bitToGigabyte(pebibitToBit(dg));
	}

	public static BigDecimal pebibitToGibibit(BigDecimal dg) {
		return bitToGibibit(pebibitToBit(dg));
	}

	public static BigDecimal pebibitToGibibyte(BigDecimal dg) {
		return bitToGibibyte(pebibitToBit(dg));
	}

	public static BigDecimal pebibitToTerabit(BigDecimal dg) {
		return bitToTerabit(pebibitToBit(dg));
	}

	public static BigDecimal pebibitToTerabyte(BigDecimal dg) {
		return bitToTerabyte(pebibitToBit(dg));
	}

	public static BigDecimal pebibitToTebibit(BigDecimal dg) {
		return bitToTebibit(pebibitToBit(dg));
	}

	public static BigDecimal pebibitToTebibyte(BigDecimal dg) {
		return bitToTebibyte(pebibitToBit(dg));
	}

	public static BigDecimal pebibitToPetabit(BigDecimal dg) {
		return bitToPetabit(pebibitToBit(dg));
	}

	public static BigDecimal pebibitToPetabyte(BigDecimal dg) {
		return bitToPetabyte(pebibitToBit(dg));
	}

	public static BigDecimal pebibitToPebibyte(BigDecimal dg) {
		return bitToPebibyte(pebibitToBit(dg));
	}

	public static BigDecimal pebibyteToBit(BigDecimal dg) {
		BigDecimal dgs = dg.multiply(new BigDecimal(bitsInKibibits).multiply(new BigDecimal(bitsInKibibits)).multiply(new BigDecimal(bitsInKibibits))
				.multiply(new BigDecimal(bitsInKibibits)).multiply(new BigDecimal(bitsInKibibits)).multiply(new BigDecimal(bitsInBytes)));
		return dgs;
	}

	public static BigDecimal pebibyteToByte(BigDecimal dg) {
		return bitToByte(pebibyteToBit(dg));
	}

	public static BigDecimal pebibyteToKilobit(BigDecimal dg) {
		return bitToKilobit(pebibyteToBit(dg));
	}

	public static BigDecimal pebibyteToKilobyte(BigDecimal dg) {
		return bitToKilobyte(pebibyteToBit(dg));
	}

	public static BigDecimal pebibyteToKibibit(BigDecimal dg) {
		return bitToKibibit(pebibyteToBit(dg));
	}

	public static BigDecimal pebibyteToKibibyte(BigDecimal dg) {
		return bitToKibibyte(pebibyteToBit(dg));
	}

	public static BigDecimal pebibyteToMegabit(BigDecimal dg) {
		return bitToMegabit(pebibyteToBit(dg));
	}

	public static BigDecimal pebibyteToMegabyte(BigDecimal dg) {
		return bitToMegabyte(pebibyteToBit(dg));
	}

	public static BigDecimal pebibyteToMebibit(BigDecimal dg) {
		return bitToMebibit(pebibyteToBit(dg));
	}

	public static BigDecimal pebibyteToMebibyte(BigDecimal dg) {
		return bitToMebibyte(pebibyteToBit(dg));
	}

	public static BigDecimal pebibyteToGigabit(BigDecimal dg) {
		return bitToGigabit(pebibyteToBit(dg));
	}

	public static BigDecimal pebibyteToGigabyte(BigDecimal dg) {
		return bitToGigabyte(pebibyteToBit(dg));
	}

	public static BigDecimal pebibyteToGibibit(BigDecimal dg) {
		return bitToGibibit(pebibyteToBit(dg));
	}

	public static BigDecimal pebibyteToGibibyte(BigDecimal dg) {
		return bitToGibibyte(pebibyteToBit(dg));
	}

	public static BigDecimal pebibyteToTerabit(BigDecimal dg) {
		return bitToTerabit(pebibyteToBit(dg));
	}

	public static BigDecimal pebibyteToTerabyte(BigDecimal dg) {
		return bitToTerabyte(pebibyteToBit(dg));
	}

	public static BigDecimal pebibyteToTebibit(BigDecimal dg) {
		return bitToTebibit(pebibyteToBit(dg));
	}

	public static BigDecimal pebibyteToTebibyte(BigDecimal dg) {
		return bitToTebibyte(pebibyteToBit(dg));
	}

	public static BigDecimal pebibyteToPetabit(BigDecimal dg) {
		return bitToPetabit(pebibyteToBit(dg));
	}

	public static BigDecimal pebibyteToPetabyte(BigDecimal dg) {
		return bitToPetabyte(pebibyteToBit(dg));
	}

	public static BigDecimal pebibyteToPebibit(BigDecimal dg) {
		return bitToPebibit(pebibyteToBit(dg));
	}
}