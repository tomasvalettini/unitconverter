package com.example.unitconverter.fragments;

import java.util.Observable;

import com.example.unitconverter.conversions.Conversions;
import com.example.zzz.R;

public class VolumeFragment extends BaseFragment {
	public VolumeFragment(int id, String[] values_array) {
		super(id, R.color.color_volume, values_array);
	}

	@Override
	public void update(Observable arg0, Object response) {
		double volume = 0;

		if (!user_input.getText().toString().equals("") && !user_input.getText().toString().equals(".")) {
			volume = Double.parseDouble(user_input.getText().toString());

			if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[0])) {
				updateUI(new double[] { volume, Conversions.millilitersToLiters(volume), Conversions.millilitersToCubicMeters(volume),
						Conversions.millilitersToCubicInches(volume), Conversions.millilitersToCubicFeet(volume), Conversions.millilitersToUSTsp(volume),
						Conversions.millilitersToUSTbsp(volume), Conversions.millilitersToUSOunces(volume), Conversions.millilitersToUSCups(volume),
						Conversions.millilitersToUSPints(volume), Conversions.millilitersToUSQuarts(volume), Conversions.millilitersToUSGallons(volume),
						Conversions.millilitersToImperialTsp(volume), Conversions.millilitersToImperialTbsp(volume),
						Conversions.millilitersToImperialOunces(volume), Conversions.millilitersToImperialPints(volume),
						Conversions.millilitersToImperialQuarts(volume), Conversions.millilitersToImperialGallons(volume) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[1])) {
				updateUI(new double[] { Conversions.litersToMilliliters(volume), volume, Conversions.litersToCubicMeters(volume),
						Conversions.litersToCubicInches(volume), Conversions.litersToCubicFeet(volume), Conversions.litersToUSTsp(volume),
						Conversions.litersToUSTbsp(volume), Conversions.litersToUSOunces(volume), Conversions.litersToUSCups(volume),
						Conversions.litersToUSPints(volume), Conversions.litersToUSQuarts(volume), Conversions.litersToUSGallons(volume),
						Conversions.litersToImperialTsp(volume), Conversions.litersToImperialTbsp(volume), Conversions.litersToImperialOunces(volume),
						Conversions.litersToImperialPints(volume), Conversions.litersToImperialQuarts(volume), Conversions.litersToImperialGallons(volume) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[2])) {
				updateUI(new double[] { Conversions.cubicMetersToMilliliters(volume), Conversions.cubicMetersToLiters(volume), volume,
						Conversions.cubicMetersToCubicInches(volume), Conversions.cubicMetersToCubicFeet(volume), Conversions.cubicMetersToUSTsp(volume),
						Conversions.cubicMetersToUSTbsp(volume), Conversions.cubicMetersToUSOunces(volume), Conversions.cubicMetersToUSCups(volume),
						Conversions.cubicMetersToUSPints(volume), Conversions.cubicMetersToUSQuarts(volume), Conversions.cubicMetersToUSGallons(volume),
						Conversions.cubicMetersToImperialTsp(volume), Conversions.cubicMetersToImperialTbsp(volume),
						Conversions.cubicMetersToImperialOunces(volume), Conversions.cubicMetersToImperialPints(volume),
						Conversions.cubicMetersToImperialQuarts(volume), Conversions.cubicMetersToImperialGallons(volume) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[3])) {
				updateUI(new double[] { Conversions.cubicInchesToMilliliters(volume), Conversions.cubicInchesToLiters(volume),
						Conversions.cubicInchesToCubicMeters(volume), volume, Conversions.cubicInchesToCubicFeet(volume),
						Conversions.cubicInchesToUSTsp(volume), Conversions.cubicInchesToUSTbsp(volume), Conversions.cubicInchesToUSOunces(volume),
						Conversions.cubicInchesToUSCups(volume), Conversions.cubicInchesToUSPints(volume), Conversions.cubicInchesToUSQuarts(volume),
						Conversions.cubicInchesToUSGallons(volume), Conversions.cubicInchesToImperialTsp(volume),
						Conversions.cubicInchesToImperialTbsp(volume), Conversions.cubicInchesToImperialOunces(volume),
						Conversions.cubicInchesToImperialPints(volume), Conversions.cubicInchesToImperialQuarts(volume),
						Conversions.cubicInchesToImperialGallons(volume) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[4])) {
				updateUI(new double[] { Conversions.cubicFeetToMilliliters(volume), Conversions.cubicFeetToLiters(volume),
						Conversions.cubicFeetToCubicMeters(volume), Conversions.cubicFeetToCubicInches(volume), volume, Conversions.cubicFeetToUSTsp(volume),
						Conversions.cubicFeetToUSTbsp(volume), Conversions.cubicFeetToUSOunces(volume), Conversions.cubicFeetToUSCups(volume),
						Conversions.cubicFeetToUSPints(volume), Conversions.cubicFeetToUSQuarts(volume), Conversions.cubicFeetToUSGallons(volume),
						Conversions.cubicFeetToImperialTsp(volume), Conversions.cubicFeetToImperialTbsp(volume), Conversions.cubicFeetToImperialOunces(volume),
						Conversions.cubicFeetToImperialPints(volume), Conversions.cubicFeetToImperialQuarts(volume),
						Conversions.cubicFeetToImperialGallons(volume) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[5])) {
				updateUI(new double[] { Conversions.usTspToMilliliters(volume), Conversions.usTspToLiters(volume), Conversions.usTspToCubicMeters(volume),
						Conversions.usTspToCubicInches(volume), Conversions.usTspToCubicFeet(volume), volume, Conversions.usTspToUSTbsp(volume),
						Conversions.usTspToUSOunces(volume), Conversions.usTspToUSCups(volume), Conversions.usTspToUSPints(volume),
						Conversions.usTspToUSQuarts(volume), Conversions.usTspToUSGallons(volume), Conversions.usTspToImperialTsp(volume),
						Conversions.usTspToImperialTbsp(volume), Conversions.usTspToImperialOunces(volume), Conversions.usTspToImperialPints(volume),
						Conversions.usTspToImperialQuarts(volume), Conversions.usTspToImperialGallons(volume) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[6])) {
				updateUI(new double[] { Conversions.usTbspToMilliliters(volume), Conversions.usTbspToLiters(volume), Conversions.usTbspToCubicMeters(volume),
						Conversions.usTbspToCubicInches(volume), Conversions.usTbspToCubicFeet(volume), Conversions.usTbspToUSTsp(volume), volume,
						Conversions.usTbspToUSOunces(volume), Conversions.usTbspToUSCups(volume), Conversions.usTbspToUSPints(volume),
						Conversions.usTbspToUSQuarts(volume), Conversions.usTbspToUSGallons(volume), Conversions.usTbspToImperialTsp(volume),
						Conversions.usTbspToImperialTbsp(volume), Conversions.usTbspToImperialOunces(volume), Conversions.usTbspToImperialPints(volume),
						Conversions.usTbspToImperialQuarts(volume), Conversions.usTbspToImperialGallons(volume) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[7])) {
				updateUI(new double[] { Conversions.usOuncesToMilliliters(volume), Conversions.usOuncesToLiters(volume),
						Conversions.usOuncesToCubicMeters(volume), Conversions.usOuncesToCubicInches(volume), Conversions.usOuncesToCubicFeet(volume),
						Conversions.usOuncesToUSTsp(volume), Conversions.usOuncesToUSTbsp(volume), volume, Conversions.usOuncesToUSCups(volume),
						Conversions.usOuncesToUSPints(volume), Conversions.usOuncesToUSQuarts(volume), Conversions.usOuncesToUSGallons(volume),
						Conversions.usOuncesToImperialTsp(volume), Conversions.usOuncesToImperialTbsp(volume), Conversions.usOuncesToImperialOunces(volume),
						Conversions.usOuncesToImperialPints(volume), Conversions.usOuncesToImperialQuarts(volume),
						Conversions.usOuncesToImperialGallons(volume) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[8])) {
				updateUI(new double[] { Conversions.usCupsToMilliliters(volume), Conversions.usCupsToLiters(volume), Conversions.usCupsToCubicMeters(volume),
						Conversions.usCupsToCubicInches(volume), Conversions.usCupsToCubicFeet(volume), Conversions.usCupsToUSTsp(volume),
						Conversions.usCupsToUSTbsp(volume), Conversions.usCupsToUSOunces(volume), volume, Conversions.usCupsToUSPints(volume),
						Conversions.usCupsToUSQuarts(volume), Conversions.usCupsToUSGallons(volume), Conversions.usCupsToImperialTsp(volume),
						Conversions.usCupsToImperialTbsp(volume), Conversions.usCupsToImperialOunces(volume), Conversions.usCupsToImperialPints(volume),
						Conversions.usCupsToImperialQuarts(volume), Conversions.usCupsToImperialGallons(volume) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[9])) {
				updateUI(new double[] { Conversions.usPintsToMilliliters(volume), Conversions.usPintsToLiters(volume),
						Conversions.usPintsToCubicMeters(volume), Conversions.usPintsToCubicInches(volume), Conversions.usPintsToCubicFeet(volume),
						Conversions.usPintsToUSTsp(volume), Conversions.usPintsToUSTbsp(volume), Conversions.usPintsToUSOunces(volume),
						Conversions.usPintsToUSCups(volume), volume, Conversions.usPintsToUSQuarts(volume), Conversions.usPintsToUSGallons(volume),
						Conversions.usPintsToImperialTsp(volume), Conversions.usPintsToImperialTbsp(volume), Conversions.usPintsToImperialOunces(volume),
						Conversions.usPintsToImperialPints(volume), Conversions.usPintsToImperialQuarts(volume), Conversions.usPintsToImperialGallons(volume) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[10])) {
				updateUI(new double[] { Conversions.usQuartsToMilliliters(volume), Conversions.usQuartsToLiters(volume),
						Conversions.usQuartsToCubicMeters(volume), Conversions.usQuartsToCubicInches(volume), Conversions.usQuartsToCubicFeet(volume),
						Conversions.usQuartsToUSTsp(volume), Conversions.usQuartsToUSTbsp(volume), Conversions.usQuartsToUSOunces(volume),
						Conversions.usQuartsToUSCups(volume), Conversions.usQuartsToUSPints(volume), volume, Conversions.usQuartsToUSGallons(volume),
						Conversions.usQuartsToImperialTsp(volume), Conversions.usQuartsToImperialTbsp(volume), Conversions.usQuartsToImperialOunces(volume),
						Conversions.usQuartsToImperialPints(volume), Conversions.usQuartsToImperialQuarts(volume),
						Conversions.usQuartsToImperialGallons(volume) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[11])) {
				updateUI(new double[] { Conversions.usGallonsToMilliliters(volume), Conversions.usGallonsToLiters(volume),
						Conversions.usGallonsToCubicMeters(volume), Conversions.usGallonsToCubicInches(volume), Conversions.usGallonsToCubicFeet(volume),
						Conversions.usGallonsToUSTsp(volume), Conversions.usGallonsToUSTbsp(volume), Conversions.usGallonsToUSOunces(volume),
						Conversions.usGallonsToUSCups(volume), Conversions.usGallonsToUSPints(volume), Conversions.usGallonsToUSQuarts(volume), volume,
						Conversions.usGallonsToImperialTsp(volume), Conversions.usGallonsToImperialTbsp(volume), Conversions.usGallonsToImperialOunces(volume),
						Conversions.usGallonsToImperialPints(volume), Conversions.usGallonsToImperialQuarts(volume),
						Conversions.usGallonsToImperialGallons(volume) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[12])) {
				updateUI(new double[] { Conversions.imperialTspToMilliliters(volume), Conversions.imperialTspToLiters(volume),
						Conversions.imperialTspToCubicMeters(volume), Conversions.imperialTspToCubicInches(volume), Conversions.imperialTspToCubicFeet(volume),
						Conversions.imperialTspToUSTsp(volume), Conversions.imperialTspToUSTbsp(volume), Conversions.imperialTspToUSOunces(volume),
						Conversions.imperialTspToUSCups(volume), Conversions.imperialTspToUSPints(volume), Conversions.imperialTspToUSQuarts(volume),
						Conversions.imperialTspToUSGallons(volume), volume, Conversions.imperialTspToImperialTbsp(volume),
						Conversions.imperialTspToImperialOunces(volume), Conversions.imperialTspToImperialPints(volume),
						Conversions.imperialTspToImperialQuarts(volume), Conversions.imperialTspToImperialGallons(volume) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[13])) {
				updateUI(new double[] { Conversions.imperialTbspToMilliliters(volume), Conversions.imperialTbspToLiters(volume),
						Conversions.imperialTbspToCubicMeters(volume), Conversions.imperialTbspToCubicInches(volume),
						Conversions.imperialTbspToCubicFeet(volume), Conversions.imperialTbspToUSTsp(volume), Conversions.imperialTbspToUSTbsp(volume),
						Conversions.imperialTbspToUSOunces(volume), Conversions.imperialTbspToUSCups(volume), Conversions.imperialTbspToUSPints(volume),
						Conversions.imperialTbspToUSQuarts(volume), Conversions.imperialTbspToUSGallons(volume), Conversions.imperialTbspToImperialTsp(volume),
						volume, Conversions.imperialTbspToImperialOunces(volume), Conversions.imperialTbspToImperialPints(volume),
						Conversions.imperialTbspToImperialQuarts(volume), Conversions.imperialTbspToImperialGallons(volume) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[14])) {
				updateUI(new double[] { Conversions.imperialOuncesToMilliliters(volume), Conversions.imperialOuncesToLiters(volume),
						Conversions.imperialOuncesToCubicMeters(volume), Conversions.imperialOuncesToCubicInches(volume),
						Conversions.imperialOuncesToCubicFeet(volume), Conversions.imperialOuncesToUSTsp(volume), Conversions.imperialOuncesToUSTbsp(volume),
						Conversions.imperialOuncesToUSOunces(volume), Conversions.imperialOuncesToUSCups(volume), Conversions.imperialOuncesToUSPints(volume),
						Conversions.imperialOuncesToUSQuarts(volume), Conversions.imperialOuncesToUSGallons(volume),
						Conversions.imperialOuncesToImperialTsp(volume), Conversions.imperialOuncesToImperialTbsp(volume), volume,
						Conversions.imperialOuncesToImperialPints(volume), Conversions.imperialOuncesToImperialQuarts(volume),
						Conversions.imperialOuncesToImperialGallons(volume) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[15])) {
				updateUI(new double[] { Conversions.imperialPintsToMilliliters(volume), Conversions.imperialPintsToLiters(volume),
						Conversions.imperialPintsToCubicMeters(volume), Conversions.imperialPintsToCubicInches(volume),
						Conversions.imperialPintsToCubicFeet(volume), Conversions.imperialPintsToUSTsp(volume), Conversions.imperialPintsToUSTbsp(volume),
						Conversions.imperialPintsToUSOunces(volume), Conversions.imperialPintsToUSCups(volume), Conversions.imperialPintsToUSPints(volume),
						Conversions.imperialPintsToUSQuarts(volume), Conversions.imperialPintsToUSGallons(volume),
						Conversions.imperialPintsToImperialTsp(volume), Conversions.imperialPintsToImperialTbsp(volume),
						Conversions.imperialPintsToImperialOunces(volume), volume, Conversions.imperialPintsToImperialQuarts(volume),
						Conversions.imperialPintsToImperialGallons(volume) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[16])) {
				updateUI(new double[] { Conversions.imperialQuartsToMilliliters(volume), Conversions.imperialQuartsToLiters(volume),
						Conversions.imperialQuartsToCubicMeters(volume), Conversions.imperialQuartsToCubicInches(volume),
						Conversions.imperialQuartsToCubicFeet(volume), Conversions.imperialQuartsToUSTsp(volume), Conversions.imperialQuartsToUSTbsp(volume),
						Conversions.imperialQuartsToUSOunces(volume), Conversions.imperialQuartsToUSCups(volume), Conversions.imperialQuartsToUSPints(volume),
						Conversions.imperialQuartsToUSQuarts(volume), Conversions.imperialQuartsToUSGallons(volume),
						Conversions.imperialQuartsToImperialTsp(volume), Conversions.imperialQuartsToImperialTbsp(volume),
						Conversions.imperialQuartsToImperialOunces(volume), Conversions.imperialQuartsToImperialPints(volume), volume,
						Conversions.imperialQuartsToImperialGallons(volume) });
			} else {
				updateUI(new double[] { Conversions.imperialGallonsToMilliliters(volume), Conversions.imperialGallonsToLiters(volume),
						Conversions.imperialGallonsToCubicMeters(volume), Conversions.imperialGallonsToCubicInches(volume),
						Conversions.imperialGallonsToCubicFeet(volume), Conversions.imperialGallonsToUSTsp(volume),
						Conversions.imperialGallonsToUSTbsp(volume), Conversions.imperialGallonsToUSOunces(volume),
						Conversions.imperialGallonsToUSCups(volume), Conversions.imperialGallonsToUSPints(volume),
						Conversions.imperialGallonsToUSQuarts(volume), Conversions.imperialGallonsToUSGallons(volume),
						Conversions.imperialGallonsToImperialTsp(volume), Conversions.imperialGallonsToImperialTbsp(volume),
						Conversions.imperialGallonsToImperialOunces(volume), Conversions.imperialGallonsToImperialPints(volume),
						Conversions.imperialGallonsToImperialQuarts(volume), volume });
			}
		}
	}
}