package com.example.unitconverter.fragments;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Observable;
import java.util.Observer;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.example.unitconverter.MainActivity;
import com.example.unitconverter.edittext.listeners.EditTextChange;
import com.example.unitconverter.edittext.listeners.EditTextOnClick;
import com.example.unitconverter.edittext.listeners.EditTextVirtualKeyboard;
import com.example.unitconverter.numberpicker.changelistener.ValueChanged;
import com.example.zzz.R;

public class BaseFragment extends Fragment implements Observer {
	protected int id;
	protected int colors;
	protected String[] values_array;

	private final int MAX_DECIMAL = 15;

	// UI elements
	protected View layout;
	protected TextView label_txt;
	protected TextView conversions_txt;
	protected EditText user_input;
	protected NumberPicker picker;

	public BaseFragment(int id, int colors, String[] values_array) {
		this.id = id;
		this.colors = colors;
		this.values_array = values_array;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		layout = inflater.inflate(R.layout.fragment_layout, container, false);
		picker = (NumberPicker) layout.findViewById(R.id.picker_temperature);
		ValueChanged pickerListener = new ValueChanged();
		EditTextChange inputListener = new EditTextChange();

		((LinearLayout) layout.findViewById(R.id.top_part)).setBackgroundColor(getActivity().getResources().getColor(colors));

		pickerListener.addObserver(this);
		inputListener.addObserver(this);

		picker.setMinValue(0);
		picker.setMaxValue(values_array.length - 1);
		picker.setDisplayedValues(values_array);
		picker.setOnValueChangedListener(pickerListener);

		label_txt = (TextView) layout.findViewById(R.id.textview_label);
		conversions_txt = (TextView) layout.findViewById(R.id.textview_conversions);

		user_input = (EditText) layout.findViewById(R.id.user_input);
		user_input.addTextChangedListener(inputListener);
		user_input.setOnClickListener(new EditTextOnClick());
		user_input.setOnEditorActionListener(new EditTextVirtualKeyboard());

		updateUI(new double[] {});

		return layout;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		((MainActivity) activity).onSectionAttached(id);
	}

	protected void updateUI(double[] unit_conversions) {
		StringBuilder label = new StringBuilder();
		StringBuilder conversion = new StringBuilder();
		boolean first = true;

		// values_array and unit_conversions should be the same length, if not
		// big problem!!
		for (int i = 0; i < values_array.length; i++) {
			if (!first) {
				label.append("\n" + values_array[i] + ": ");
			} else {
				label.append(values_array[i] + ": ");
				first = false;
			}
		}

		first = true;

		if (unit_conversions.length > 0) {
			for (int i = 0; i < values_array.length; i++) {
				if (!first) {
					conversion.append("\n" + getFormattedDouble(unit_conversions[i]));
				} else {
					conversion.append(getFormattedDouble(unit_conversions[i]));
					first = false;
				}
			}
		}

		label_txt.setText(label.toString());
		conversions_txt.setText(conversion.toString());
	}

	private double getFormattedDouble(double x) {
		BigDecimal bd = new BigDecimal(x).setScale(MAX_DECIMAL, RoundingMode.HALF_EVEN);

		return bd.doubleValue();
	}

	protected void updateUIBigDecimal(BigDecimal[] unit_conversions) {
		StringBuilder label = new StringBuilder();
		StringBuilder conversion = new StringBuilder();
		boolean first = true;

		// values_array and unit_conversions should be the same length, if not
		// big problem!!
		for (int i = 0; i < values_array.length; i++) {
			if (!first) {
				label.append("\n" + values_array[i] + ": ");
			} else {
				label.append(values_array[i] + ": ");
				first = false;
			}
		}

		first = true;

		if (unit_conversions.length > 0) {
			for (int j = 0; j < unit_conversions.length; j++) {
				unit_conversions[j].setScale(MAX_DECIMAL, RoundingMode.HALF_EVEN);
			}

			for (int i = 0; i < values_array.length; i++) {
				if (!first) {
					conversion.append("\n" + unit_conversions[i].doubleValue());
				} else {
					conversion.append(unit_conversions[i].doubleValue());
					first = false;
				}
			}
		}

		label_txt.setText(label.toString());
		conversions_txt.setText(conversion.toString());
	}

	@Override
	public void update(Observable observable, Object data) {
		/*
		 * not implemented since we are going to override it in child classes!!
		 */
	}
}
