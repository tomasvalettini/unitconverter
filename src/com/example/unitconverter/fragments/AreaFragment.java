package com.example.unitconverter.fragments;

import java.util.Observable;

import com.example.unitconverter.conversions.Conversions;
import com.example.zzz.R;

public class AreaFragment extends BaseFragment {
	public AreaFragment(int id, String[] values_array) {
		super(id, R.color.color_area, values_array);
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		double area = 0;

		if (!user_input.getText().toString().equals("") && !user_input.getText().toString().equals(".")) {
			area = Double.parseDouble(user_input.getText().toString());

			if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[0])) {
				updateUI(new double[] { area, Conversions.millimetersToCentimeters(area), Conversions.millimetersToMeters(area),
						Conversions.millimetersToKilometers(area), Conversions.millimetersToInches(area), Conversions.millimetersToFeet(area),
						Conversions.millimetersToYards(area), Conversions.millimetersToMiles(area), Conversions.millimetersToNauticalMiles(area) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[1])) {
				updateUI(new double[] { Conversions.centimetersToMillimeters(area), area, Conversions.centimetersToMeters(area),
						Conversions.centimetersToKilometers(area), Conversions.centimetersToInches(area), Conversions.centimetersToFeet(area),
						Conversions.centimetersToYards(area), Conversions.centimetersToMiles(area), Conversions.centimetersToNauticalMiles(area) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[2])) {
				updateUI(new double[] { Conversions.metersToMillimeters(area), Conversions.metersToCentimeters(area), area,
						Conversions.metersToKilometers(area), Conversions.metersToInches(area), Conversions.metersToFeet(area),
						Conversions.metersToYards(area), Conversions.metersToMiles(area), Conversions.metersToNauticalMiles(area) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[3])) {
				updateUI(new double[] { Conversions.kilometersToMillimeters(area), Conversions.kilometersToCentimeters(area),
						Conversions.kilometersToMeters(area), area, Conversions.kilometersToInches(area), Conversions.kilometersToFeet(area),
						Conversions.kilometersToYards(area), Conversions.kilometersToMiles(area), Conversions.kilometersToNauticalMiles(area) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[4])) {
				updateUI(new double[] { Conversions.inchesToMillimeters(area), Conversions.inchesToCentimeters(area), Conversions.inchesToMeters(area),
						Conversions.inchesToKilometers(area), area, Conversions.inchesToFeet(area), Conversions.inchesToYards(area),
						Conversions.inchesToMiles(area), Conversions.inchesToNauticalMiles(area) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[5])) {
				updateUI(new double[] { Conversions.feetToMillimeters(area), Conversions.feetToCentimeters(area), Conversions.feetToMeters(area),
						Conversions.feetToKilometers(area), Conversions.feetToInches(area), area, Conversions.feetToYards(area), Conversions.feetToMiles(area),
						Conversions.feetToNauticalMiles(area) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[6])) {
				updateUI(new double[] { Conversions.yardsToMillimeters(area), Conversions.yardsToCentimeters(area), Conversions.yardsToMeters(area),
						Conversions.yardsToKilometers(area), Conversions.yardsToInches(area), Conversions.yardsToFeet(area), area,
						Conversions.yardsToMiles(area), Conversions.yardsToNauticalMiles(area) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[7])) {
				updateUI(new double[] { Conversions.milesToMillimeters(area), Conversions.milesToCentimeters(area), Conversions.milesToMeters(area),
						Conversions.milesToKilometers(area), Conversions.milesToInches(area), Conversions.milesToFeet(area), Conversions.milesToYards(area),
						area, Conversions.milesToNauticalMiles(area) });
			} else {
				updateUI(new double[] { Conversions.nauticalMilesToMillimeters(area), Conversions.nauticalMilesToCentimeters(area),
						Conversions.nauticalMilesToMeters(area), Conversions.nauticalMilesToKilometers(area), Conversions.nauticalMilesToInches(area),
						Conversions.nauticalMilesToFeet(area), Conversions.nauticalMilesToYards(area), Conversions.nauticalMilesToMiles(area), area });
			}
		}
	}
}