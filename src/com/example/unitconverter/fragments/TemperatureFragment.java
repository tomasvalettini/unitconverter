package com.example.unitconverter.fragments;

import java.util.Observable;

import android.widget.Toast;

import com.example.unitconverter.conversions.Conversions;
import com.example.zzz.R;

public class TemperatureFragment extends BaseFragment {
	public TemperatureFragment(int id, String[] values_array) {
		super(id, R.color.color_temperature, values_array);
	}

	@Override
	public void update(Observable arg0, Object response) {
		double deg = 0;

		if (!user_input.getText().toString().equals("-") && !user_input.getText().toString().equals("") && !user_input.getText().toString().equals(".")
				&& !user_input.getText().toString().equals("-.")) {
			deg = Double.parseDouble(user_input.getText().toString());

			if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[0])) {
				if (Conversions.celsiusToKelvin(deg) < 0) {
					deg = Conversions.kelvinToCelsius(0);
					user_input.setText(deg + "");
					Toast.makeText(getActivity(), "No temperature can go lower than " + deg + " Celsius", Toast.LENGTH_LONG).show();
				}

				updateUI(new double[] { deg, Conversions.celsiusToFahrenheit(deg), Conversions.celsiusToKelvin(deg) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[1])) {
				if (Conversions.fahrenheitToKelvin(deg) < 0) {
					deg = Conversions.kelvinToFehrenheit(0);
					user_input.setText(deg + "");
					Toast.makeText(getActivity(), "No temperature can go lower than " + deg + " Fahrenheit", Toast.LENGTH_LONG).show();
				}

				updateUI(new double[] { Conversions.fahrenheitToCelsius(deg), deg, Conversions.fahrenheitToKelvin(deg) });
			} else {
				if (deg < 0) {
					deg = 0;
					user_input.setText(deg + "");
					Toast.makeText(getActivity(), "No temperature can go lower than " + deg + " Kelvin", Toast.LENGTH_LONG).show();
				}

				updateUI(new double[] { Conversions.kelvinToCelsius(deg), Conversions.kelvinToFehrenheit(deg), deg });
			}
		}
	}
}