package com.example.unitconverter.fragments;

import java.util.Observable;

import com.example.unitconverter.conversions.Conversions;
import com.example.zzz.R;

public class SpeedFragment extends BaseFragment {
	public SpeedFragment(int id, String[] values_array) {
		super(id, R.color.color_speed, values_array);
	}

	@Override
	public void update(Observable arg0, Object response) {
		double speed = 0;

		if (!user_input.getText().toString().equals("") && !user_input.getText().toString().equals(".")) {
			speed = Double.parseDouble(user_input.getText().toString());

			if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[0])) {
				updateUI(new double[] { speed, Conversions.msToKmh(speed), Conversions.msToFs(speed), Conversions.msToMih(speed), Conversions.msToKnots(speed) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[1])) {
				updateUI(new double[] { Conversions.kmhToMs(speed), speed, Conversions.kmhToFs(speed), Conversions.kmhToMih(speed),
						Conversions.kmhToKnots(speed) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[2])) {
				updateUI(new double[] { Conversions.fsToMs(speed), Conversions.fsToKmh(speed), speed, Conversions.fsToMih(speed), Conversions.fsToKnots(speed) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[3])) {
				updateUI(new double[] { Conversions.mihToMs(speed), Conversions.mihToKmh(speed), Conversions.mihToFs(speed), speed,
						Conversions.mihToKnots(speed) });
			} else {
				updateUI(new double[] { Conversions.knotsToMs(speed), Conversions.knotsToKmh(speed), Conversions.knotsToFs(speed),
						Conversions.knotsToMih(speed), speed });
			}
		}
	}
}