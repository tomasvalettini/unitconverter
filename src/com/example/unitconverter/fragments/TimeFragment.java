package com.example.unitconverter.fragments;

import java.util.Observable;

import com.example.unitconverter.conversions.Conversions;
import com.example.zzz.R;

public class TimeFragment extends BaseFragment {
	public TimeFragment(int id, String[] values_array) {
		super(id, R.color.color_time, values_array);
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		double time = 0;

		if (!user_input.getText().toString().equals("") && !user_input.getText().toString().equals(".")) {
			time = Double.parseDouble(user_input.getText().toString());

			if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[0])) {
				updateUI(new double[] { time, Conversions.nanosecondsToMicroseconds(time), Conversions.nanosecondsToMilliseconds(time),
						Conversions.nanosecondsToSeconds(time), Conversions.nanosecondsToMinutes(time), Conversions.nanosecondsToHours(time),
						Conversions.nanosecondsToDays(time), Conversions.nanosecondsToWeeks(time), Conversions.nanosecondsToMonths(time),
						Conversions.nanosecondsToYears(time), Conversions.nanosecondsToDecades(time), Conversions.nanosecondsToCenturies(time) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[1])) {
				updateUI(new double[] { Conversions.microsecondsToNanoseconds(time), time, Conversions.microsecondsToMilliseconds(time),
						Conversions.microsecondsToSeconds(time), Conversions.microsecondsToMinutes(time), Conversions.microsecondsToHours(time),
						Conversions.microsecondsToDays(time), Conversions.microsecondsToWeeks(time), Conversions.microsecondsToMonths(time),
						Conversions.microsecondsToYears(time), Conversions.microsecondsToDecades(time), Conversions.microsecondsToCenturies(time) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[2])) {
				updateUI(new double[] { Conversions.millisecondsToNanoseconds(time), Conversions.millisecondsToMicroseconds(time), time,
						Conversions.millisecondsToSeconds(time), Conversions.millisecondsToMinutes(time), Conversions.millisecondsToHours(time),
						Conversions.millisecondsToDays(time), Conversions.millisecondsToWeeks(time), Conversions.millisecondsToMonths(time),
						Conversions.millisecondsToYears(time), Conversions.millisecondsToDecades(time), Conversions.millisecondsToCenturies(time) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[3])) {
				updateUI(new double[] { Conversions.secondsToNanoseconds(time), Conversions.secondsToMicroseconds(time),
						Conversions.secondsToMilliseconds(time), time, Conversions.secondsToMinutes(time), Conversions.secondsToHours(time),
						Conversions.secondsToDays(time), Conversions.secondsToWeeks(time), Conversions.secondsToMonths(time), Conversions.secondsToYears(time),
						Conversions.secondsToDecades(time), Conversions.secondsToCenturies(time) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[4])) {
				updateUI(new double[] { Conversions.minutesToNanoseconds(time), Conversions.minutesToMicroseconds(time),
						Conversions.minutesToMilliseconds(time), Conversions.minutesToSeconds(time), time, Conversions.minutesToHours(time),
						Conversions.minutesToDays(time), Conversions.minutesToWeeks(time), Conversions.minutesToMonths(time), Conversions.minutesToYears(time),
						Conversions.minutesToDecades(time), Conversions.minutesToCenturies(time) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[5])) {
				updateUI(new double[] { Conversions.hoursToNanoseconds(time), Conversions.hoursToMicroseconds(time), Conversions.hoursToMilliseconds(time),
						Conversions.hoursToSeconds(time), Conversions.hoursToMinutes(time), time, Conversions.hoursToDays(time),
						Conversions.hoursToWeeks(time), Conversions.hoursToMonths(time), Conversions.hoursToYears(time), Conversions.hoursToDecades(time),
						Conversions.hoursToCenturies(time) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[6])) {
				updateUI(new double[] { Conversions.daysToNanoseconds(time), Conversions.daysToMicroseconds(time), Conversions.daysToMilliseconds(time),
						Conversions.daysToSeconds(time), Conversions.daysToMinutes(time), Conversions.daysToHours(time), time, Conversions.daysToWeeks(time),
						Conversions.daysToMonths(time), Conversions.daysToYears(time), Conversions.daysToDecades(time), Conversions.daysToCenturies(time) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[7])) {
				updateUI(new double[] { Conversions.weeksToNanoseconds(time), Conversions.weeksToMicroseconds(time), Conversions.weeksToMilliseconds(time),
						Conversions.weeksToSeconds(time), Conversions.weeksToMinutes(time), Conversions.weeksToHours(time), Conversions.weeksToDays(time),
						time, Conversions.weeksToMonths(time), Conversions.weeksToYears(time), Conversions.weeksToDecades(time),
						Conversions.weeksToCenturies(time) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[8])) {
				updateUI(new double[] { Conversions.monthsToNanoseconds(time), Conversions.monthsToMicroseconds(time), Conversions.monthsToMilliseconds(time),
						Conversions.monthsToSeconds(time), Conversions.monthsToMinutes(time), Conversions.monthsToHours(time), Conversions.monthsToDays(time),
						Conversions.monthsToWeeks(time), time, Conversions.monthsToYears(time), Conversions.monthsToDecades(time),
						Conversions.monthsToCenturies(time) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[9])) {
				updateUI(new double[] { Conversions.yearsToNanoseconds(time), Conversions.yearsToMicroseconds(time), Conversions.yearsToMilliseconds(time),
						Conversions.yearsToSeconds(time), Conversions.yearsToMinutes(time), Conversions.yearsToHours(time), Conversions.yearsToDays(time),
						Conversions.yearsToWeeks(time), Conversions.yearsToMonths(time), time, Conversions.yearsToDecades(time),
						Conversions.yearsToCenturies(time) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[10])) {
				updateUI(new double[] { Conversions.decadesToNanoseconds(time), Conversions.decadesToMicroseconds(time),
						Conversions.decadesToMilliseconds(time), Conversions.decadesToSeconds(time), Conversions.decadesToMinutes(time),
						Conversions.decadesToHours(time), Conversions.decadesToDays(time), Conversions.decadesToWeeks(time), Conversions.decadesToMonths(time),
						Conversions.decadesToYears(time), time, Conversions.decadesToCenturies(time) });
			} else {
				updateUI(new double[] { Conversions.centuriesToNanoseconds(time), Conversions.centuriesToMicroseconds(time),
						Conversions.centuriesToMilliseconds(time), Conversions.centuriesToSeconds(time), Conversions.centuriesToMinutes(time),
						Conversions.centuriesToHours(time), Conversions.centuriesToDays(time), Conversions.centuriesToWeeks(time),
						Conversions.centuriesToMonths(time), Conversions.centuriesToYears(time), Conversions.centuriesToDecades(time), time });
			}
		}
	}
}