package com.example.unitconverter.fragments;

import java.math.BigDecimal;
import java.util.Observable;

import com.example.unitconverter.conversions.Conversions;
import com.example.zzz.R;

public class DigitalStorageFragment extends BaseFragment {
	public DigitalStorageFragment(int id, String[] values_array) {
		super(id, R.color.color_digital_storage, values_array);
	}

	@Override
	public void update(Observable arg0, Object response) {
		double dgs = 0;

		if (!user_input.getText().toString().equals("") && !user_input.getText().toString().equals(".")) {
			dgs = Double.parseDouble(user_input.getText().toString());
			BigDecimal dg = new BigDecimal(dgs);

			if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[0])) {
				updateUIBigDecimal(new BigDecimal[] { dg, Conversions.bitToByte(dg), Conversions.bitToKilobit(dg), Conversions.bitToKilobyte(dg),
						Conversions.bitToKibibit(dg), Conversions.bitToKibibyte(dg), Conversions.bitToMegabit(dg), Conversions.bitToMegabyte(dg),
						Conversions.bitToMebibit(dg), Conversions.bitToMebibyte(dg), Conversions.bitToGigabit(dg), Conversions.bitToGigabyte(dg),
						Conversions.bitToGibibit(dg), Conversions.bitToGibibyte(dg), Conversions.bitToTerabit(dg), Conversions.bitToTerabyte(dg),
						Conversions.bitToTebibit(dg), Conversions.bitToTebibyte(dg), Conversions.bitToPetabit(dg), Conversions.bitToPetabyte(dg),
						Conversions.bitToPebibit(dg), Conversions.bitToPebibyte(dg) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[1])) {
				updateUIBigDecimal(new BigDecimal[] { Conversions.byteToBit(dg), dg, Conversions.byteToKilobit(dg), Conversions.byteToKilobyte(dg),
						Conversions.byteToKibibit(dg), Conversions.byteToKibibyte(dg), Conversions.byteToMegabit(dg), Conversions.byteToMegabyte(dg),
						Conversions.byteToMebibit(dg), Conversions.byteToMebibyte(dg), Conversions.byteToGigabit(dg), Conversions.byteToGigabyte(dg),
						Conversions.byteToGibibit(dg), Conversions.byteToGibibyte(dg), Conversions.byteToTerabit(dg), Conversions.byteToTerabyte(dg),
						Conversions.byteToTebibit(dg), Conversions.byteToTebibyte(dg), Conversions.byteToPetabit(dg), Conversions.byteToPetabyte(dg),
						Conversions.byteToPebibit(dg), Conversions.byteToPebibyte(dg) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[2])) {
				updateUIBigDecimal(new BigDecimal[] { Conversions.kilobitToBit(dg), Conversions.kilobitToByte(dg), dg, Conversions.kilobitToKilobyte(dg),
						Conversions.kilobitToKibibit(dg), Conversions.kilobitToKibibyte(dg), Conversions.kilobitToMegabit(dg),
						Conversions.kilobitToMegabyte(dg), Conversions.kilobitToMebibit(dg), Conversions.kilobitToMebibyte(dg),
						Conversions.kilobitToGigabit(dg), Conversions.kilobitToGigabyte(dg), Conversions.kilobitToGibibit(dg),
						Conversions.kilobitToGibibyte(dg), Conversions.kilobitToTerabit(dg), Conversions.kilobitToTerabyte(dg),
						Conversions.kilobitToTebibit(dg), Conversions.kilobitToTebibyte(dg), Conversions.kilobitToPetabit(dg),
						Conversions.kilobitToPetabyte(dg), Conversions.kilobitToPebibit(dg), Conversions.kilobitToPebibyte(dg) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[3])) {
				updateUIBigDecimal(new BigDecimal[] { Conversions.kilobyteToBit(dg), Conversions.kilobyteToByte(dg), Conversions.kilobyteToKilobit(dg), dg,
						Conversions.kilobyteToKibibit(dg), Conversions.kilobyteToKibibyte(dg), Conversions.kilobyteToMegabit(dg),
						Conversions.kilobyteToMegabyte(dg), Conversions.kilobyteToMebibit(dg), Conversions.kilobyteToMebibyte(dg),
						Conversions.kilobyteToGigabit(dg), Conversions.kilobyteToGigabyte(dg), Conversions.kilobyteToGibibit(dg),
						Conversions.kilobyteToGibibyte(dg), Conversions.kilobyteToTerabit(dg), Conversions.kilobyteToTerabyte(dg),
						Conversions.kilobyteToTebibit(dg), Conversions.kilobyteToTebibyte(dg), Conversions.kilobyteToPetabit(dg),
						Conversions.kilobyteToPetabyte(dg), Conversions.kilobyteToPebibit(dg), Conversions.kilobyteToPebibyte(dg) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[4])) {
				updateUIBigDecimal(new BigDecimal[] { Conversions.kibibitToBit(dg), Conversions.kibibitToByte(dg), Conversions.kibibitToKilobit(dg),
						Conversions.kibibitToKilobyte(dg), dg, Conversions.kibibitToKibibyte(dg), Conversions.kibibitToMegabit(dg),
						Conversions.kibibitToMegabyte(dg), Conversions.kibibitToMebibit(dg), Conversions.kibibitToMebibyte(dg),
						Conversions.kibibitToGigabit(dg), Conversions.kibibitToGigabyte(dg), Conversions.kibibitToGibibit(dg),
						Conversions.kibibitToGibibyte(dg), Conversions.kibibitToTerabit(dg), Conversions.kibibitToTerabyte(dg),
						Conversions.kibibitToTebibit(dg), Conversions.kibibitToTebibyte(dg), Conversions.kibibitToPetabit(dg),
						Conversions.kibibitToPetabyte(dg), Conversions.kibibitToPebibit(dg), Conversions.kibibitToPebibyte(dg) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[5])) {
				updateUIBigDecimal(new BigDecimal[] { Conversions.kibibyteToBit(dg), Conversions.kibibyteToByte(dg), Conversions.kibibyteToKilobit(dg),
						Conversions.kibibyteToKilobyte(dg), Conversions.kibibyteToKibibit(dg), dg, Conversions.kibibyteToMegabit(dg),
						Conversions.kibibyteToMegabyte(dg), Conversions.kibibyteToMebibit(dg), Conversions.kibibyteToMebibyte(dg),
						Conversions.kibibyteToGigabit(dg), Conversions.kibibyteToGigabyte(dg), Conversions.kibibyteToGibibit(dg),
						Conversions.kibibyteToGibibyte(dg), Conversions.kibibyteToTerabit(dg), Conversions.kibibyteToTerabyte(dg),
						Conversions.kibibyteToTebibit(dg), Conversions.kibibyteToTebibyte(dg), Conversions.kibibyteToPetabit(dg),
						Conversions.kibibyteToPetabyte(dg), Conversions.kibibyteToPebibit(dg), Conversions.kibibyteToPebibyte(dg) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[6])) {
				updateUIBigDecimal(new BigDecimal[] { Conversions.megabitToBit(dg), Conversions.megabitToByte(dg), Conversions.megabitToKilobit(dg),
						Conversions.megabitToKilobyte(dg), Conversions.megabitToKibibit(dg), Conversions.megabitToKibibyte(dg), dg,
						Conversions.megabitToMegabyte(dg), Conversions.megabitToMebibit(dg), Conversions.megabitToMebibyte(dg),
						Conversions.megabitToGigabit(dg), Conversions.megabitToGigabyte(dg), Conversions.megabitToGibibit(dg),
						Conversions.megabitToGibibyte(dg), Conversions.megabitToTerabit(dg), Conversions.megabitToTerabyte(dg),
						Conversions.megabitToTebibit(dg), Conversions.megabitToTebibyte(dg), Conversions.megabitToPetabit(dg),
						Conversions.megabitToPetabyte(dg), Conversions.megabitToPebibit(dg), Conversions.megabitToPebibyte(dg) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[7])) {
				updateUIBigDecimal(new BigDecimal[] { Conversions.megabyteToBit(dg), Conversions.megabyteToByte(dg), Conversions.megabyteToKilobit(dg),
						Conversions.megabyteToKilobyte(dg), Conversions.megabyteToKibibit(dg), Conversions.megabyteToKibibyte(dg),
						Conversions.megabyteToMegabit(dg), dg, Conversions.megabyteToMebibit(dg), Conversions.megabyteToMebibyte(dg),
						Conversions.megabyteToGigabit(dg), Conversions.megabyteToGigabyte(dg), Conversions.megabyteToGibibit(dg),
						Conversions.megabyteToGibibyte(dg), Conversions.megabyteToTerabit(dg), Conversions.megabyteToTerabyte(dg),
						Conversions.megabyteToTebibit(dg), Conversions.megabyteToTebibyte(dg), Conversions.megabyteToPetabit(dg),
						Conversions.megabyteToPetabyte(dg), Conversions.megabyteToPebibit(dg), Conversions.megabyteToPebibyte(dg) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[8])) {
				updateUIBigDecimal(new BigDecimal[] { Conversions.mebibitToBit(dg), Conversions.mebibitToByte(dg), Conversions.mebibitToKilobit(dg),
						Conversions.mebibitToKilobyte(dg), Conversions.mebibitToKibibit(dg), Conversions.mebibitToKibibyte(dg),
						Conversions.mebibitToMegabit(dg), Conversions.mebibitToMegabyte(dg), dg, Conversions.mebibitToMebibyte(dg),
						Conversions.mebibitToGigabit(dg), Conversions.mebibitToGigabyte(dg), Conversions.mebibitToGibibit(dg),
						Conversions.mebibitToGibibyte(dg), Conversions.mebibitToTerabit(dg), Conversions.mebibitToTerabyte(dg),
						Conversions.mebibitToTebibit(dg), Conversions.mebibitToTebibyte(dg), Conversions.mebibitToPetabit(dg),
						Conversions.mebibitToPetabyte(dg), Conversions.mebibitToPebibit(dg), Conversions.mebibitToPebibyte(dg) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[9])) {
				updateUIBigDecimal(new BigDecimal[] { Conversions.mebibyteToBit(dg), Conversions.mebibyteToByte(dg), Conversions.mebibyteToKilobit(dg),
						Conversions.mebibyteToKilobyte(dg), Conversions.mebibyteToKibibit(dg), Conversions.mebibyteToKibibyte(dg),
						Conversions.mebibyteToMegabit(dg), Conversions.mebibyteToMegabyte(dg), Conversions.mebibyteToMebibit(dg), dg,
						Conversions.mebibyteToGigabit(dg), Conversions.mebibyteToGigabyte(dg), Conversions.mebibyteToGibibit(dg),
						Conversions.mebibyteToGibibyte(dg), Conversions.mebibyteToTerabit(dg), Conversions.mebibyteToTerabyte(dg),
						Conversions.mebibyteToTebibit(dg), Conversions.mebibyteToTebibyte(dg), Conversions.mebibyteToPetabit(dg),
						Conversions.mebibyteToPetabyte(dg), Conversions.mebibyteToPebibit(dg), Conversions.mebibyteToPebibyte(dg) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[10])) {
				updateUIBigDecimal(new BigDecimal[] { Conversions.gigabitToBit(dg), Conversions.gigabitToByte(dg), Conversions.gigabitToKilobit(dg),
						Conversions.gigabitToKilobyte(dg), Conversions.gigabitToKibibit(dg), Conversions.gigabitToKibibyte(dg),
						Conversions.gigabitToMegabit(dg), Conversions.gigabitToMegabyte(dg), Conversions.gigabitToMebibit(dg),
						Conversions.gigabitToMebibyte(dg), dg, Conversions.gigabitToGigabyte(dg), Conversions.gigabitToGibibit(dg),
						Conversions.gigabitToGibibyte(dg), Conversions.gigabitToTerabit(dg), Conversions.gigabitToTerabyte(dg),
						Conversions.gigabitToTebibit(dg), Conversions.gigabitToTebibyte(dg), Conversions.gigabitToPetabit(dg),
						Conversions.gigabitToPetabyte(dg), Conversions.gigabitToPebibit(dg), Conversions.gigabitToPebibyte(dg) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[11])) {
				updateUIBigDecimal(new BigDecimal[] { Conversions.gigabyteToBit(dg), Conversions.gigabyteToByte(dg), Conversions.gigabyteToKilobit(dg),
						Conversions.gigabyteToKilobyte(dg), Conversions.gigabyteToKibibit(dg), Conversions.gigabyteToKibibyte(dg),
						Conversions.gigabyteToMegabit(dg), Conversions.gigabyteToMegabyte(dg), Conversions.gigabyteToMebibit(dg),
						Conversions.gigabyteToMebibyte(dg), Conversions.gigabyteToGigabit(dg), dg, Conversions.gigabyteToGibibit(dg),
						Conversions.gigabyteToGibibyte(dg), Conversions.gigabyteToTerabit(dg), Conversions.gigabyteToTerabyte(dg),
						Conversions.gigabyteToTebibit(dg), Conversions.gigabyteToTebibyte(dg), Conversions.gigabyteToPetabit(dg),
						Conversions.gigabyteToPetabyte(dg), Conversions.gigabyteToPebibit(dg), Conversions.gigabyteToPebibyte(dg) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[12])) {
				updateUIBigDecimal(new BigDecimal[] { Conversions.gibibitToBit(dg), Conversions.gibibitToByte(dg), Conversions.gibibitToKilobit(dg),
						Conversions.gibibitToKilobyte(dg), Conversions.gibibitToKibibit(dg), Conversions.gibibitToKibibyte(dg),
						Conversions.gibibitToMegabit(dg), Conversions.gibibitToMegabyte(dg), Conversions.gibibitToMebibit(dg),
						Conversions.gibibitToMebibyte(dg), Conversions.gibibitToGigabit(dg), Conversions.gibibitToGigabyte(dg), dg,
						Conversions.gibibitToGibibyte(dg), Conversions.gibibitToTerabit(dg), Conversions.gibibitToTerabyte(dg),
						Conversions.gibibitToTebibit(dg), Conversions.gibibitToTebibyte(dg), Conversions.gibibitToPetabit(dg),
						Conversions.gibibitToPetabyte(dg), Conversions.gibibitToPebibit(dg), Conversions.gibibitToPebibyte(dg) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[13])) {
				updateUIBigDecimal(new BigDecimal[] { Conversions.gibibyteToBit(dg), Conversions.gibibyteToByte(dg), Conversions.gibibyteToKilobit(dg),
						Conversions.gibibyteToKilobyte(dg), Conversions.gibibyteToKibibit(dg), Conversions.gibibyteToKibibyte(dg),
						Conversions.gibibyteToMegabit(dg), Conversions.gibibyteToMegabyte(dg), Conversions.gibibyteToMebibit(dg),
						Conversions.gibibyteToMebibyte(dg), Conversions.gibibyteToGigabit(dg), Conversions.gibibyteToGigabyte(dg),
						Conversions.gibibyteToGibibit(dg), dg, Conversions.gibibyteToTerabit(dg), Conversions.gibibyteToTerabyte(dg),
						Conversions.gibibyteToTebibit(dg), Conversions.gibibyteToTebibyte(dg), Conversions.gibibyteToPetabit(dg),
						Conversions.gibibyteToPetabyte(dg), Conversions.gibibyteToPebibit(dg), Conversions.gibibyteToPebibyte(dg) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[14])) {
				updateUIBigDecimal(new BigDecimal[] { Conversions.terabitToBit(dg), Conversions.terabitToByte(dg), Conversions.terabitToKilobit(dg),
						Conversions.terabitToKilobyte(dg), Conversions.terabitToKibibit(dg), Conversions.terabitToKibibyte(dg),
						Conversions.terabitToMegabit(dg), Conversions.terabitToMegabyte(dg), Conversions.terabitToMebibit(dg),
						Conversions.terabitToMebibyte(dg), Conversions.terabitToGigabit(dg), Conversions.terabitToGigabyte(dg),
						Conversions.terabitToGibibit(dg), Conversions.terabitToGibibyte(dg), dg, Conversions.terabitToTerabyte(dg),
						Conversions.terabitToTebibit(dg), Conversions.terabitToTebibyte(dg), Conversions.terabitToPetabit(dg),
						Conversions.terabitToPetabyte(dg), Conversions.terabitToPebibit(dg), Conversions.terabitToPebibyte(dg) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[15])) {
				updateUIBigDecimal(new BigDecimal[] { Conversions.terabyteToBit(dg), Conversions.terabyteToByte(dg), Conversions.terabyteToKilobit(dg),
						Conversions.terabyteToKilobyte(dg), Conversions.terabyteToKibibit(dg), Conversions.terabyteToKibibyte(dg),
						Conversions.terabyteToMegabit(dg), Conversions.terabyteToMegabyte(dg), Conversions.terabyteToMebibit(dg),
						Conversions.terabyteToMebibyte(dg), Conversions.terabyteToGigabit(dg), Conversions.terabyteToGigabyte(dg),
						Conversions.terabyteToGibibit(dg), Conversions.terabyteToGibibyte(dg), Conversions.terabyteToTerabit(dg), dg,
						Conversions.terabyteToTebibit(dg), Conversions.terabyteToTebibyte(dg), Conversions.terabyteToPetabit(dg),
						Conversions.terabyteToPetabyte(dg), Conversions.terabyteToPebibit(dg), Conversions.terabyteToPebibyte(dg) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[16])) {
				updateUIBigDecimal(new BigDecimal[] { Conversions.tebibitToBit(dg), Conversions.tebibitToByte(dg), Conversions.tebibitToKilobit(dg),
						Conversions.tebibitToKilobyte(dg), Conversions.tebibitToKibibit(dg), Conversions.tebibitToKibibyte(dg),
						Conversions.tebibitToMegabit(dg), Conversions.tebibitToMegabyte(dg), Conversions.tebibitToMebibit(dg),
						Conversions.tebibitToMebibyte(dg), Conversions.tebibitToGigabit(dg), Conversions.tebibitToGigabyte(dg),
						Conversions.tebibitToGibibit(dg), Conversions.tebibitToGibibyte(dg), Conversions.tebibitToTerabit(dg),
						Conversions.tebibitToTerabyte(dg), dg, Conversions.tebibitToTebibyte(dg), Conversions.tebibitToPetabit(dg),
						Conversions.tebibitToPetabyte(dg), Conversions.tebibitToPebibit(dg), Conversions.tebibitToPebibyte(dg) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[17])) {
				updateUIBigDecimal(new BigDecimal[] { Conversions.tebibyteToBit(dg), Conversions.tebibyteToByte(dg), Conversions.tebibyteToKilobit(dg),
						Conversions.tebibyteToKilobyte(dg), Conversions.tebibyteToKibibit(dg), Conversions.tebibyteToKibibyte(dg),
						Conversions.tebibyteToMegabit(dg), Conversions.tebibyteToMegabyte(dg), Conversions.tebibyteToMebibit(dg),
						Conversions.tebibyteToMebibyte(dg), Conversions.tebibyteToGigabit(dg), Conversions.tebibyteToGigabyte(dg),
						Conversions.tebibyteToGibibit(dg), Conversions.tebibyteToGibibyte(dg), Conversions.tebibyteToTerabit(dg),
						Conversions.tebibyteToTerabyte(dg), Conversions.tebibyteToTebibit(dg), dg, Conversions.tebibyteToPetabit(dg),
						Conversions.tebibyteToPetabyte(dg), Conversions.tebibyteToPebibit(dg), Conversions.tebibyteToPebibyte(dg) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[18])) {
				updateUIBigDecimal(new BigDecimal[] { Conversions.petabitToBit(dg), Conversions.petabitToByte(dg), Conversions.petabitToKilobit(dg),
						Conversions.petabitToKilobyte(dg), Conversions.petabitToKibibit(dg), Conversions.petabitToKibibyte(dg),
						Conversions.petabitToMegabit(dg), Conversions.petabitToMegabyte(dg), Conversions.petabitToMebibit(dg),
						Conversions.petabitToMebibyte(dg), Conversions.petabitToGigabit(dg), Conversions.petabitToGigabyte(dg),
						Conversions.petabitToGibibit(dg), Conversions.petabitToGibibyte(dg), Conversions.petabitToTerabit(dg),
						Conversions.petabitToTerabyte(dg), Conversions.petabitToTebibit(dg), Conversions.petabitToTebibyte(dg), dg,
						Conversions.petabitToPetabyte(dg), Conversions.petabitToPebibit(dg), Conversions.petabitToPebibyte(dg) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[19])) {
				updateUIBigDecimal(new BigDecimal[] { Conversions.petabyteToBit(dg), Conversions.petabyteToByte(dg), Conversions.petabyteToKilobit(dg),
						Conversions.petabyteToKilobyte(dg), Conversions.petabyteToKibibit(dg), Conversions.petabyteToKibibyte(dg),
						Conversions.petabyteToMegabit(dg), Conversions.petabyteToMegabyte(dg), Conversions.petabyteToMebibit(dg),
						Conversions.petabyteToMebibyte(dg), Conversions.petabyteToGigabit(dg), Conversions.petabyteToGigabyte(dg),
						Conversions.petabyteToGibibit(dg), Conversions.petabyteToGibibyte(dg), Conversions.petabyteToTerabit(dg),
						Conversions.petabyteToTerabyte(dg), Conversions.petabyteToTebibit(dg), Conversions.petabyteToTebibyte(dg),
						Conversions.petabyteToPetabit(dg), dg, Conversions.petabyteToPebibit(dg), Conversions.petabyteToPebibyte(dg) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[20])) {
				updateUIBigDecimal(new BigDecimal[] { Conversions.pebibitToBit(dg), Conversions.pebibitToByte(dg), Conversions.pebibitToKilobit(dg),
						Conversions.pebibitToKilobyte(dg), Conversions.pebibitToKibibit(dg), Conversions.pebibitToKibibyte(dg),
						Conversions.pebibitToMegabit(dg), Conversions.pebibitToMegabyte(dg), Conversions.pebibitToMebibit(dg),
						Conversions.pebibitToMebibyte(dg), Conversions.pebibitToGigabit(dg), Conversions.pebibitToGigabyte(dg),
						Conversions.pebibitToGibibit(dg), Conversions.pebibitToGibibyte(dg), Conversions.pebibitToTerabit(dg),
						Conversions.pebibitToTerabyte(dg), Conversions.pebibitToTebibit(dg), Conversions.pebibitToTebibyte(dg),
						Conversions.pebibitToPetabit(dg), Conversions.pebibitToPetabyte(dg), dg, Conversions.pebibitToPebibyte(dg) });
			} else {
				updateUIBigDecimal(new BigDecimal[] { Conversions.pebibyteToBit(dg), Conversions.pebibyteToByte(dg), Conversions.pebibyteToKilobit(dg),
						Conversions.pebibyteToKilobyte(dg), Conversions.pebibyteToKibibit(dg), Conversions.pebibyteToKibibyte(dg),
						Conversions.pebibyteToMegabit(dg), Conversions.pebibyteToMegabyte(dg), Conversions.pebibyteToMebibit(dg),
						Conversions.pebibyteToMebibyte(dg), Conversions.pebibyteToGigabit(dg), Conversions.pebibyteToGigabyte(dg),
						Conversions.pebibyteToGibibit(dg), Conversions.pebibyteToGibibyte(dg), Conversions.pebibyteToTerabit(dg),
						Conversions.pebibyteToTerabyte(dg), Conversions.pebibyteToTebibit(dg), Conversions.pebibyteToTebibyte(dg),
						Conversions.pebibyteToPetabit(dg), Conversions.pebibyteToPetabyte(dg), Conversions.pebibyteToPebibit(dg), dg });
			}
		}
	}
}