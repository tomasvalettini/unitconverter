package com.example.unitconverter.fragments;

import java.util.Observable;

import com.example.unitconverter.conversions.Conversions;
import com.example.zzz.R;

public class LengthFragment extends BaseFragment {
	public LengthFragment(int id, String[] values_array) {
		super(id, R.color.color_length, values_array);
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		double length = 0;

		if (!user_input.getText().toString().equals("") && !user_input.getText().toString().equals(".")) {
			length = Double.parseDouble(user_input.getText().toString());

			if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[0])) {
				updateUI(new double[] { length, Conversions.millimetersToCentimeters(length), Conversions.millimetersToMeters(length),
						Conversions.millimetersToKilometers(length), Conversions.millimetersToInches(length), Conversions.millimetersToFeet(length),
						Conversions.millimetersToYards(length), Conversions.millimetersToMiles(length), Conversions.millimetersToNauticalMiles(length) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[1])) {
				updateUI(new double[] { Conversions.centimetersToMillimeters(length), length, Conversions.centimetersToMeters(length),
						Conversions.centimetersToKilometers(length), Conversions.centimetersToInches(length), Conversions.centimetersToFeet(length),
						Conversions.centimetersToYards(length), Conversions.centimetersToMiles(length), Conversions.centimetersToNauticalMiles(length) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[2])) {
				updateUI(new double[] { Conversions.metersToMillimeters(length), Conversions.metersToCentimeters(length), length,
						Conversions.metersToKilometers(length), Conversions.metersToInches(length), Conversions.metersToFeet(length),
						Conversions.metersToYards(length), Conversions.metersToMiles(length), Conversions.metersToNauticalMiles(length) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[3])) {
				updateUI(new double[] { Conversions.kilometersToMillimeters(length), Conversions.kilometersToCentimeters(length),
						Conversions.kilometersToMeters(length), length, Conversions.kilometersToInches(length), Conversions.kilometersToFeet(length),
						Conversions.kilometersToYards(length), Conversions.kilometersToMiles(length), Conversions.kilometersToNauticalMiles(length) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[4])) {
				updateUI(new double[] { Conversions.inchesToMillimeters(length), Conversions.inchesToCentimeters(length), Conversions.inchesToMeters(length),
						Conversions.inchesToKilometers(length), length, Conversions.inchesToFeet(length), Conversions.inchesToYards(length),
						Conversions.inchesToMiles(length), Conversions.inchesToNauticalMiles(length) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[5])) {
				updateUI(new double[] { Conversions.feetToMillimeters(length), Conversions.feetToCentimeters(length), Conversions.feetToMeters(length),
						Conversions.feetToKilometers(length), Conversions.feetToInches(length), length, Conversions.feetToYards(length),
						Conversions.feetToMiles(length), Conversions.feetToNauticalMiles(length) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[6])) {
				updateUI(new double[] { Conversions.yardsToMillimeters(length), Conversions.yardsToCentimeters(length), Conversions.yardsToMeters(length),
						Conversions.yardsToKilometers(length), Conversions.yardsToInches(length), Conversions.yardsToFeet(length), length,
						Conversions.yardsToMiles(length), Conversions.yardsToNauticalMiles(length) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[7])) {
				updateUI(new double[] { Conversions.milesToMillimeters(length), Conversions.milesToCentimeters(length), Conversions.milesToMeters(length),
						Conversions.milesToKilometers(length), Conversions.milesToInches(length), Conversions.milesToFeet(length),
						Conversions.milesToYards(length), length, Conversions.milesToNauticalMiles(length) });
			} else {
				updateUI(new double[] { Conversions.nauticalMilesToMillimeters(length), Conversions.nauticalMilesToCentimeters(length),
						Conversions.nauticalMilesToMeters(length), Conversions.nauticalMilesToKilometers(length), Conversions.nauticalMilesToInches(length),
						Conversions.nauticalMilesToFeet(length), Conversions.nauticalMilesToYards(length), Conversions.nauticalMilesToMiles(length), length });
			}
		}
	}
}