package com.example.unitconverter.fragments;

import java.util.Observable;

import com.example.unitconverter.conversions.Conversions;
import com.example.zzz.R;

public class MassFragment extends BaseFragment {

	public MassFragment(int id, String[] values_array) {
		super(id, R.color.color_mass, values_array);
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		double mass = 0;

		if (!user_input.getText().toString().equals("") && !user_input.getText().toString().equals(".")) {
			mass = Double.parseDouble(user_input.getText().toString());

			if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[0])) {
				updateUI(new double[] { mass, Conversions.microgramsToMilligrams(mass), Conversions.microgramsToGrams(mass),
						Conversions.microgramsToKilograms(mass), Conversions.microgramsToMetrictons(mass), Conversions.microgramsToOunces(mass),
						Conversions.microgramsToPounds(mass), Conversions.microgramsToStones(mass), Conversions.microgramsToShorttons(mass),
						Conversions.microgramsToLongtons(mass) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[1])) {
				updateUI(new double[] { Conversions.milligramsToMicrograms(mass), mass, Conversions.milligramsToGrams(mass),
						Conversions.milligramsToKilograms(mass), Conversions.milligramsToMetrictons(mass), Conversions.milligramsToOunces(mass),
						Conversions.milligramsToPounds(mass), Conversions.milligramsToStones(mass), Conversions.milligramsToShorttons(mass),
						Conversions.milligramsToLongtons(mass) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[2])) {
				updateUI(new double[] { Conversions.gramsToMicrograms(mass), Conversions.gramsToMilligrams(mass), mass, Conversions.gramsToKilograms(mass),
						Conversions.gramsToMetrictons(mass), Conversions.gramsToOunces(mass), Conversions.gramsToPounds(mass), Conversions.gramsToStones(mass),
						Conversions.gramsToShorttons(mass), Conversions.gramsToLongtons(mass) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[3])) {
				updateUI(new double[] { Conversions.kilogramsToMicrograms(mass), Conversions.kilogramsToMilligrams(mass), Conversions.kilogramsToGrams(mass),
						mass, Conversions.kilogramsToMetrictons(mass), Conversions.kilogramsToOunces(mass), Conversions.kilogramsToPounds(mass),
						Conversions.kilogramsToStones(mass), Conversions.kilogramsToShorttons(mass), Conversions.kilogramsToLongtons(mass) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[4])) {
				updateUI(new double[] { Conversions.metrictonsToMicrograms(mass), Conversions.metrictonsToMilligrams(mass),
						Conversions.metrictonsToGrams(mass), Conversions.metrictonsToKilograms(mass), mass, Conversions.metrictonsToOunces(mass),
						Conversions.metrictonsToPounds(mass), Conversions.metrictonsToStones(mass), Conversions.metrictonsToShorttons(mass),
						Conversions.metrictonsToLongtons(mass) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[5])) {
				updateUI(new double[] { Conversions.ouncesToMicrograms(mass), Conversions.ouncesToMilligrams(mass), Conversions.ouncesToGrams(mass),
						Conversions.ouncesToKilograms(mass), Conversions.ouncesToMetrictons(mass), mass, Conversions.ouncesToPounds(mass),
						Conversions.ouncesToStones(mass), Conversions.ouncesToShorttons(mass), Conversions.ouncesToLongtons(mass) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[6])) {
				updateUI(new double[] { Conversions.poundsToMicrograms(mass), Conversions.poundsToMilligrams(mass), Conversions.poundsToGrams(mass),
						Conversions.poundsToKilograms(mass), Conversions.poundsToMetrictons(mass), Conversions.poundsToOunces(mass), mass,
						Conversions.poundsToStones(mass), Conversions.poundsToShorttons(mass), Conversions.poundsToLongtons(mass) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[7])) {
				updateUI(new double[] { Conversions.stonesToMicrograms(mass), Conversions.stonesToMilligrams(mass), Conversions.stonesToGrams(mass),
						Conversions.stonesToKilograms(mass), Conversions.stonesToMetrictons(mass), Conversions.stonesToOunces(mass),
						Conversions.stonesToPounds(mass), mass, Conversions.stonesToShorttons(mass), Conversions.stonesToLongtons(mass) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[8])) {
				updateUI(new double[] { Conversions.shorttonsToMicrograms(mass), Conversions.shorttonsToMilligrams(mass), Conversions.shorttonsToGrams(mass),
						Conversions.shorttonsToKilograms(mass), Conversions.shorttonsToMetrictons(mass), Conversions.shorttonsToOunces(mass),
						Conversions.shorttonsToPounds(mass), Conversions.shorttonsToStones(mass), mass, Conversions.shorttonsToLongtons(mass) });
			} else {
				updateUI(new double[] { Conversions.longtonsToMicrograms(mass), Conversions.longtonsToMilligrams(mass), Conversions.longtonsToGrams(mass),
						Conversions.longtonsToKilograms(mass), Conversions.longtonsToMetrictons(mass), Conversions.longtonsToOunces(mass),
						Conversions.longtonsToPounds(mass), Conversions.longtonsToStones(mass), Conversions.longtonsToShorttons(mass), mass });
			}
		}
	}
}