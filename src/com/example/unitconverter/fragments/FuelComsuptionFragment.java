package com.example.unitconverter.fragments;

import java.util.Observable;

import com.example.unitconverter.conversions.Conversions;
import com.example.zzz.R;

public class FuelComsuptionFragment extends BaseFragment {
	public FuelComsuptionFragment(int id, String[] values_array) {
		super(id, R.color.color_fuel_consuption, values_array);
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		double fuelConsumption = 0;

		if (!user_input.getText().toString().equals("") && !user_input.getText().toString().equals(".")) {
			fuelConsumption = Double.parseDouble(user_input.getText().toString());

			if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[0])) {
				updateUI(new double[] { fuelConsumption, Conversions.litersPer100KmToKmPerLiters(fuelConsumption),
						Conversions.litersPer100KmToMilesPerUSGallons(fuelConsumption), Conversions.litersPer100KmToMilesPerImperialGallons(fuelConsumption) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[1])) {
				updateUI(new double[] { Conversions.kmPerLitersToLitersPer100Km(fuelConsumption), fuelConsumption,
						Conversions.kmPerLitersToMilesPerUSGallons(fuelConsumption), Conversions.kmPerLitersToMilesPerImperialGallons(fuelConsumption) });
			} else if (picker.getDisplayedValues()[picker.getValue()].equals(values_array[2])) {
				updateUI(new double[] { Conversions.milesPerUSGallonsToLitersPer100Km(fuelConsumption),
						Conversions.milesPerUSGallonsToKmPerLiters(fuelConsumption), fuelConsumption,
						Conversions.milesPerUSGallonsToMilesPerImperialGallons(fuelConsumption) });
			} else {
				updateUI(new double[] { Conversions.milesPerImperialGallonsToLitersPer100Km(fuelConsumption),
						Conversions.milesPerImperialGallonsToKmPerLiters(fuelConsumption),
						Conversions.milesPerImperialGallonsToMilesPerUSGallons(fuelConsumption), fuelConsumption });
			}
		}
	}
}