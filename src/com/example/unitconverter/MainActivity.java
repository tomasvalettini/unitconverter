package com.example.unitconverter;

import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;

import com.example.unitconverter.fragments.AreaFragment;
import com.example.unitconverter.fragments.DigitalStorageFragment;
import com.example.unitconverter.fragments.FuelComsuptionFragment;
import com.example.unitconverter.fragments.LengthFragment;
import com.example.unitconverter.fragments.MassFragment;
import com.example.unitconverter.fragments.SpeedFragment;
import com.example.unitconverter.fragments.TemperatureFragment;
import com.example.unitconverter.fragments.TimeFragment;
import com.example.unitconverter.fragments.VolumeFragment;
import com.example.zzz.R;

public class MainActivity extends Activity implements NavigationDrawerFragment.NavigationDrawerCallbacks {

	/**
	 * Fragment managing the behaviors, interactions and presentation of the
	 * navigation drawer.
	 */
	private NavigationDrawerFragment mNavigationDrawerFragment;

	/**
	 * Used to store the last screen title. For use in
	 * {@link #restoreActionBar()}.
	 */
	private CharSequence mTitle;
	private String[] nav_items_array;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		nav_items_array = getResources().getStringArray(R.array.nav_items_array);

		mNavigationDrawerFragment = (NavigationDrawerFragment) getFragmentManager().findFragmentById(R.id.navigation_drawer);
		mTitle = getTitle();

		// Set up the drawer.
		mNavigationDrawerFragment.setUp(R.id.navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout));
	}

	@Override
	public void onNavigationDrawerItemSelected(int position) {
		// update the main content by replacing fragments
		FragmentManager fragmentManager = getFragmentManager();
		switch (position) {
		case 0:
			fragmentManager.beginTransaction()
					.replace(R.id.container, new TemperatureFragment(position, getResources().getStringArray(R.array.temperature_array))).commit();
			break;
		case 1:
			fragmentManager.beginTransaction().replace(R.id.container, new LengthFragment(position, getResources().getStringArray(R.array.length_array)))
					.commit();
			break;
		case 2:
			fragmentManager.beginTransaction().replace(R.id.container, new MassFragment(position, getResources().getStringArray(R.array.mass_array))).commit();
			break;
		case 3:
			fragmentManager.beginTransaction().replace(R.id.container, new SpeedFragment(position, getResources().getStringArray(R.array.speed_array)))
					.commit();
			break;
		case 4:
			fragmentManager.beginTransaction().replace(R.id.container, new VolumeFragment(position, getResources().getStringArray(R.array.volume_array)))
					.commit();
			break;
		case 5:
			fragmentManager.beginTransaction().replace(R.id.container, new AreaFragment(position, getResources().getStringArray(R.array.area_array))).commit();
			break;
		case 6:
			fragmentManager.beginTransaction()
					.replace(R.id.container, new FuelComsuptionFragment(position, getResources().getStringArray(R.array.fuel_consuption_array))).commit();
			break;
		case 7:
			fragmentManager.beginTransaction().replace(R.id.container, new TimeFragment(position, getResources().getStringArray(R.array.time_array))).commit();
			break;
		case 8:
			fragmentManager.beginTransaction()
					.replace(R.id.container, new DigitalStorageFragment(position, getResources().getStringArray(R.array.digital_storage_array))).commit();
			break;
		default:
			break;
		}

	}

	public void onSectionAttached(int number) {
		mTitle = nav_items_array[number];
	}

	public void restoreActionBar() {
		ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setDisplayShowHomeEnabled(false);
		actionBar.setTitle(mTitle);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (!mNavigationDrawerFragment.isDrawerOpen()) {
			// Only show items in the action bar relevant to this screen
			// if the drawer is not showing. Otherwise, let the drawer
			// decide what to show in the action bar.
			// getMenuInflater().inflate(R.menu.main, menu);
			restoreActionBar();
			return true;
		}

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}

		return super.onOptionsItemSelected(item);
	}
}